package com.scit.tabibak.Models.Medical_info_models;

import com.google.gson.annotations.SerializedName;
import com.scit.tabibak.Models.UserObject;

import java.io.Serializable;

public class MedicalInfoObject implements Serializable {
    @SerializedName("id") private int id = 0;
    @SerializedName("name") private String name = "";
    @SerializedName("image") private String image = "";
    @SerializedName("details") private String details = "";
    @SerializedName("user_id") private int user_id = 0;
    @SerializedName("sub_details") private String sub_details = "";
    @SerializedName("owner") private UserObject owner = new UserObject();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public UserObject getOwner() {
        return owner;
    }

    public void setOwner(UserObject owner) {
        this.owner = owner;
    }

    public String getSub_details() {
        return sub_details;
    }

    public void setSub_details(String sub_details) {
        this.sub_details = sub_details;
    }
}
