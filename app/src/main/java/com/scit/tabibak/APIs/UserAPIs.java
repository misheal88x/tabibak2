package com.scit.tabibak.APIs;

import com.scit.tabibak.Models.BaseResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface UserAPIs {
    @Multipart
    @POST("register")
    Call<BaseResponse> register(
            @Header("Accept") String accept,
            @Part("name") RequestBody name,
            @Part("phone") RequestBody phone,
            @Part("password") RequestBody password,
            @Part("role_id") RequestBody role_id,
            @Part("city_id") RequestBody city_id,
            @Part("specialization_id") RequestBody specialization_id,
            @Part("address") RequestBody address,
            @Part("identification_file") RequestBody identification_file,
            @Part("email") RequestBody email,
            @Part("facebook") RequestBody facebook,
            @Part("facility_id") RequestBody facility_id,
            @Part MultipartBody.Part image
    );

    @FormUrlEncoded
    @POST("login")
    Call<BaseResponse> login(
            @Header("Accept") String accept,
            @Field("phone") String phone,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("fcm-update")
    Call<BaseResponse> update_fcm(
            @Header("Accept") String accept,
            @Field("fcm_token") String token
    );
}
