package com.scit.tabibak.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scit.tabibak.APIsClasses.MedicinesAPIsClass;
import com.scit.tabibak.Activities.SplashActivity;
import com.scit.tabibak.Bases.BaseFragment;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Dialogs.ContactUsDialog;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.ILoadImage;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Models.Medicine_models.MedicineObject;
import com.scit.tabibak.Models.UserObject;
import com.scit.tabibak.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import im.delight.android.webview.AdvancedWebView;

public class MedicineDetailsFragment extends BaseFragment {
    private RelativeLayout toolbar;
    private TextView title;
    private RelativeLayout call,help,logout;

    private ImageView image;
    private ProgressBar loading;
    private AdvancedWebView web_view;

    private LinearLayout layout,no_internet,error;
    private ProgressBar loading_data;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_medicine_details,container,false);
    }

    @Override
    public void init_views() {
        toolbar = base.findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        call = toolbar.findViewById(R.id.contact_us);
        help = toolbar.findViewById(R.id.help);
        logout = toolbar.findViewById(R.id.logout);
        if (!SharedPrefManager.getInstance(base).getLogged()){
            logout.setVisibility(View.GONE);
        }

        image = base.findViewById(R.id.image);
        loading = base.findViewById(R.id.loading);
        loading_data = base.findViewById(R.id.loading_data);
        web_view = base.findViewById(R.id.web_view);
        layout = base.findViewById(R.id.layout);
        no_internet = base.findViewById(R.id.no_internet_layout);
        error = base.findViewById(R.id.error_layout);
    }

    @Override
    public void init_events() {

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(base);
                builder.setTitle(getResources().getString(R.string.logout_title));
                builder.setMessage(getResources().getString(R.string.logout_message));
                builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPrefManager.getInstance(base).setLogged(false);
                        SharedPrefManager.getInstance(base).setUser(new UserObject());
                        startActivity(new Intent(base, SplashActivity.class));
                        base.finish();
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactUsDialog dialog = new ContactUsDialog(base);
                dialog.show();
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base.open_fragment(new HelpFragment());
            }
        });
        error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callDetailsAPI(getArguments().getInt("id"));
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        title.setText("");
        callDetailsAPI(getArguments().getInt("id"));
    }

    private void callDetailsAPI(final int id){
        loading_data.setVisibility(View.VISIBLE);
        layout.setVisibility(View.GONE);
        error.setVisibility(View.GONE);
        no_internet.setVisibility(View.GONE);
        MedicinesAPIsClass.get_med(base, id,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        loading_data.setVisibility(View.GONE);
                        error.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        loading_data.setVisibility(View.GONE);
                        String j = new Gson().toJson(json);
                        try{
                            MedicineObject success = new Gson().fromJson(j,MedicineObject.class);
                            title.setText(success.getName());
                            web_view.loadHtml(success.getDetails());
                            layout.setVisibility(View.VISIBLE);
                        }catch (Exception e){
                            callDetailsAPI(id);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        loading_data.setVisibility(View.GONE);
                        no_internet.setVisibility(View.VISIBLE);
                    }
                });
    }
}
