package com.scit.tabibak.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Dialogs.AddDiagnosisDialog;
import com.scit.tabibak.Interfaces.OnItemClickListener;
import com.scit.tabibak.Models.Medical_guide_models.SearchResultObject;
import com.scit.tabibak.Models.Medicine_models.MedicineObject;
import com.scit.tabibak.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class PatientsRecordsAdapter extends RecyclerView.Adapter<PatientsRecordsAdapter.ViewHolder> {
    private Context context;
    private List<SearchResultObject> list;


    private OnItemClickListener onItemClickListener;

    public PatientsRecordsAdapter(Context context,List<SearchResultObject> list,OnItemClickListener onItemClickListener) {
        this.context = context;
        this.onItemClickListener = onItemClickListener;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private CircleImageView icon;
        private TextView add_diagnosis;
        private ProgressBar loading;
        private CardView layout;
        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            icon = view.findViewById(R.id.icon);
            add_diagnosis = view.findViewById(R.id.add_diagnosis);
            loading = view.findViewById(R.id.loading);
            layout = view.findViewById(R.id.layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_patient_record, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final SearchResultObject o = list.get(position);

        holder.name.setText(o.getName());
        BaseFunctions.setGlideImage(context,holder.icon,o.getAvatar());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(position);
            }
        });
        holder.add_diagnosis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddDiagnosisDialog dialog = new AddDiagnosisDialog(context,o.getId());
                dialog.show();
            }
        });
    }
}
