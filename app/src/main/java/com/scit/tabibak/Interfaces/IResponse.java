package com.scit.tabibak.Interfaces;

public interface IResponse {
    void onResponse();
    void onResponse(Object json);
}
