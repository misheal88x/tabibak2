package com.scit.tabibak.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.SliderLayout;
//import com.glide.slider.library.SliderTypes.BaseSliderView;
//import com.glide.slider.library.SliderTypes.DefaultSliderView;
//import com.glide.slider.library.Tricks.ViewPagerEx;
import com.glide.slider.library.animations.DescriptionAnimation;
import com.glide.slider.library.slidertypes.BaseSliderView;
import com.glide.slider.library.slidertypes.DefaultSliderView;
import com.glide.slider.library.tricks.ViewPagerEx;
import com.google.gson.Gson;
import com.scit.tabibak.Activities.LoginActivity;
import com.scit.tabibak.Activities.SplashActivity;
import com.scit.tabibak.Bases.BaseFragment;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Dialogs.ContactUsDialog;
import com.scit.tabibak.Dialogs.DateDetailsDialog;
import com.scit.tabibak.Dialogs.HealthNoteDetailsDialog;
import com.scit.tabibak.Dialogs.ImportantNoteDetailsDialog;
import com.scit.tabibak.Dialogs.InfoDetailsDialog;
import com.scit.tabibak.Dialogs.MedicineDetailsDialog;
import com.scit.tabibak.Dialogs.RepliesDialog;
import com.scit.tabibak.Models.Important_notes_models.ImportantNoteObject;
import com.scit.tabibak.Models.Medical_consulation_models.ReplyObject;
import com.scit.tabibak.Models.Medical_dates_models.MedicalDateObject;
import com.scit.tabibak.Models.Medical_info_models.MedicalInfoObject;
import com.scit.tabibak.Models.Medical_notes_models.MedicalNoteObject;
import com.scit.tabibak.Models.Medicine_models.MedicineObject;
import com.scit.tabibak.Models.Startup_models.SliderObject;
import com.scit.tabibak.Models.UserObject;
import com.scit.tabibak.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;

public class HomeFragment extends BaseFragment implements ViewPagerEx.OnPageChangeListener, BaseSliderView.OnSliderClickListener {

    private RelativeLayout toolbar;
    private TextView title;
    private RelativeLayout call,help,logout;

    private SliderLayout slider;
    private CardView question,guide,info,medicine;
    private List<SliderObject> slides;
    private int slider_index = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home,container,false);
    }

    @Override
    public void init_views() {
        toolbar = base.findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        call = toolbar.findViewById(R.id.contact_us);
        help = toolbar.findViewById(R.id.help);
        logout = toolbar.findViewById(R.id.logout);
        if (!SharedPrefManager.getInstance(base).getLogged()){
            logout.setVisibility(View.GONE);
        }
        slider = base.findViewById(R.id.slider);

        question = base.findViewById(R.id.question_card);
        guide = base.findViewById(R.id.guide_card);
        info = base.findViewById(R.id.info_card);
        medicine = base.findViewById(R.id.medicine_card);
    }

    @Override
    public void init_events() {
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(base);
                builder.setTitle(getResources().getString(R.string.logout_title));
                builder.setMessage(getResources().getString(R.string.logout_message));
                builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPrefManager.getInstance(base).setLogged(false);
                        SharedPrefManager.getInstance(base).setUser(new UserObject());
                        startActivity(new Intent(base, SplashActivity.class));
                        base.finish();
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
        guide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base.open_fragment(new MedicalGuideFragment());
            }
        });
        question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPrefManager.getInstance(base).getLogged()) {
                    base.open_fragment(new MedicalConsulationFragment());
                }else {
                    Intent intent = new Intent(base, LoginActivity.class);
                    startActivity(intent);
                }
            }
        });
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base.open_fragment(new MedicalInfosFragment());
            }
        });
        medicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base.open_fragment(new MedicinesFragment());
            }
        });
        slider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("fnvbfbv", "onClick: "+"item");
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactUsDialog dialog = new ContactUsDialog(base);
                dialog.show();
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base.open_fragment(new HelpFragment());
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        title.setText(getResources().getString(R.string.home_home));
        initializingSlider();
        if (getArguments()!=null){
            if (getArguments().containsKey("target_type")) {
                    if (getArguments().getString("target_type") != null) {
                        String type = getArguments().getString("target_type");
                        switch (type) {
                            //A doctor replies to a question of mine
                            case "1": {
                                try {
                                    String object_json = getArguments().getString("input");
                                    ReplyObject[] success = new Gson().fromJson(object_json, ReplyObject[].class);
                                    final List<ReplyObject> lis = new ArrayList<>();
                                    for (ReplyObject r : success) {
                                        lis.add(r);
                                    }
                                    final String question = getArguments().getString("title");
                                    RepliesDialog dialog = new RepliesDialog(base, lis, question);
                                    dialog.show();
                                } catch (Exception e) {
                                }
                            }
                            break;
                            //Medical date
                            case "2":
                            case "9":
                            case "11":{
                                try {
                                    String object_json = getArguments().getString("input");
                                    final MedicalDateObject success = new Gson().fromJson(object_json, MedicalDateObject.class);
                                    DateDetailsDialog dialog = new DateDetailsDialog(base, success);
                                    dialog.show();

                                } catch (Exception e) {
                                }
                            }
                            break;
                            //Medical Note
                            case "4": {
                                try {
                                    String object_json = getArguments().getString("input");
                                    final MedicalNoteObject success = new Gson().fromJson(object_json, MedicalNoteObject.class);
                                    HealthNoteDetailsDialog dialog = new HealthNoteDetailsDialog(base, success);
                                    dialog.show();
                                } catch (Exception e) {
                                }
                            }
                            break;
                            //Very important
                            case "3": {
                                try {
                                    String object_json = getArguments().getString("input");
                                    final ImportantNoteObject success = new Gson().fromJson(object_json, ImportantNoteObject.class);
                                    ImportantNoteDetailsDialog dialog = new ImportantNoteDetailsDialog(base, success);
                                    dialog.show();
                                } catch (Exception e) {
                                }
                            }
                            break;
                            //Medical Info
                            case "5":{
                                String object_json = getArguments().getString("input");
                                MedicalInfoObject success = new Gson().fromJson(object_json,MedicalInfoObject.class);
                                InfoDetailsDialog dialog = new InfoDetailsDialog(base,success);
                                dialog.show();
                            }break;
                            //Medicine
                            case "7":{
                                String object_json = getArguments().getString("input");
                                MedicineObject success = new Gson().fromJson(object_json,MedicineObject.class);
                                MedicineDetailsDialog dialog = new MedicineDetailsDialog(base,success);
                                dialog.show();
                            }break;
                        }
                    }
                }
        }
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        slider_index = i;
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    private void initSlider(){

    }

    public void initializingSlider(){
        slides = new ArrayList<>();
        ArrayList<String> listUrl = new ArrayList<>();
        ArrayList<String> listName = new ArrayList<>();
        if (SharedPrefManager.getInstance(base).getSlides().size()>0){
            int counter = 0;
            for (SliderObject s : SharedPrefManager.getInstance(base).getSlides()){
                slides.add(s);
                listUrl.add(s.getImage());
                listName.add("Title_"+counter);
                counter++;
            }
        }
            RequestOptions requestOptions = new RequestOptions();
            requestOptions
                    .centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.drawable.grey_background).placeholder(R.drawable.grey_background);
            for (int i = 0; i < listUrl.size(); i++) {
                BaseSliderView sliderView = new DefaultSliderView(getContext());
                sliderView
                        .image(listUrl.get(i))
                        //.setBackgroundColor(getResources().getColor(R.color.grey))
                        .setRequestOption(requestOptions)
                        .setProgressBarVisible(true);
                sliderView.bundle(new Bundle());
                sliderView.getBundle().putString("extra", listName.get(i));
                slider.addSlider(sliderView);
                sliderView.setOnSliderClickListener(this);
            }
            slider.setPresetTransformer(com.glide.slider.library.SliderLayout.Transformer.Default);
            slider.setPresetIndicator(com.glide.slider.library.SliderLayout.PresetIndicators.Center_Bottom);
            slider.setCustomAnimation(new DescriptionAnimation());
            slider.setDuration(4000);
            slider.addOnPageChangeListener(this);
        }


    @Override
    public void onSliderClick(BaseSliderView baseSliderView) {
        BaseFunctions.openBrowser(base,slides.get(slider.getCurrentPosition()).getUrl());
    }
}
