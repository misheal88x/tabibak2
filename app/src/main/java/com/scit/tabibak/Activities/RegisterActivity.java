package com.scit.tabibak.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import de.hdodenhof.circleimageview.CircleImageView;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.service.autofill.SaveRequest;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.scit.tabibak.APIsClasses.UserAPIsClass;
import com.scit.tabibak.Adapters.CustomSpinnerAdapter;
import com.scit.tabibak.Bases.BaseActivity;
import com.scit.tabibak.Bases.BaseFragment;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Fragments.MedicalFilesFragment;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Interfaces.OnItemClickListener;
import com.scit.tabibak.Models.Startup_models.CityObject;
import com.scit.tabibak.Models.Startup_models.FacilityObject;
import com.scit.tabibak.Models.Startup_models.SpecializationObject;
import com.scit.tabibak.R;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RegisterActivity extends BaseActivity implements BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.ImageLoaderDelegate{

    private EditText username,phone,password;
    private TextView btn_register;
    private ProgressBar loading;
    private Spinner type_spinner;
    private List<String> type_list;
    private CustomSpinnerAdapter type_adapter;
    private String selected_type = "2";

    private LinearLayout doctor_layout;
    private CircleImageView profile_pic;
    private TextView profile_pic_title;
    private RelativeLayout profile_pic_layout;
    private Spinner city_spinner,profession_spinner,medical_type_spinner;
    private List<CityObject> city_list;
    private List<String> city_list_string;
    private CustomSpinnerAdapter city_adapter;
    private List<FacilityObject> medical_type_list;
    private List<String> medical_type_list_string;
    private CustomSpinnerAdapter medical_type_adapter;
    private EditText address,identity_file,email,facebook;
    private List<SpecializationObject> profession_list;
    private List<String> profession_list_string;
    private CustomSpinnerAdapter profession_adapter;
    private int selected_city = 0,selected_profession = 0,selected_medical_type = 0;
    private String imagePath = "";
    private static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";


    @Override
    public void set_layout() {
        setContentView(R.layout.activity_register);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        init_spinner();
    }

    @Override
    public void init_views() {
        username = findViewById(R.id.username);
        phone = findViewById(R.id.phone);
        password = findViewById(R.id.password);
        btn_register = findViewById(R.id.register);
        loading = findViewById(R.id.loading);
        type_spinner = findViewById(R.id.type_spinner);

        doctor_layout = findViewById(R.id.doctor_layout);
        profile_pic = findViewById(R.id.profile_pic);
        profile_pic_title = findViewById(R.id.profile_pic_title);
        profile_pic_layout = findViewById(R.id.profile_pic_layout);
        city_spinner = findViewById(R.id.city_spinner);
        profession_spinner = findViewById(R.id.profession_spinner);
        medical_type_spinner = findViewById(R.id.medical_type_spinner);
        address = findViewById(R.id.address);
        identity_file = findViewById(R.id.identity_file);
        email = findViewById(R.id.email);
        facebook = findViewById(R.id.facebook);
    }

    @Override
    public void init_events() {

        profile_pic_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},1000);
                }else {
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
                        ImagePicker.Companion.with(RegisterActivity.this)
                                .galleryOnly()
                                .cropSquare()  			//Crop image(Optional), Check Customization for more option
                                .compress(1024)			//Final image size will be less than 1 MB(Optional)
                                .start();
                    }else {
                        BSImagePicker singleSelectionPicker = new BSImagePicker.Builder("com.scit.tabibak.provider")
                                .hideCameraTile() //Default: show. Set this if you don't want user to take photo.
                                .hideGalleryTile() //Default: show. Set this if you don't want to further let user select from a gallery app. In such case, I suggest you to set maximum displaying images to Integer.MAX_VALUE.
                                .build();
                        singleSelectionPicker.show(getSupportFragmentManager(),"Picker");
                    }
                }
            }
        });
        city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,i);
                if (position == 0){
                    selected_city = 0;
                }else {
                    selected_city = city_list.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        profession_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,i);
                if (position == 0){
                    selected_profession = 0;
                }else {
                    selected_profession = profession_list.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        medical_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,i);
                if (position == 0){
                    selected_medical_type = 0;
                }else {
                    selected_medical_type = medical_type_list.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,i);
                if (position == 0){
                    selected_type = "";
                }else if (position == 1){
                    selected_type = "2";
                    doctor_layout.setVisibility(View.GONE);
                }else if (position == 2){
                    selected_type = "3";
                    doctor_layout.setVisibility(View.VISIBLE);
                    profile_pic.setImageResource(R.drawable.light_grey_circle);
                    profile_pic_title.setVisibility(View.VISIBLE);
                    city_spinner.setSelection(0);
                    selected_city = 0;
                    address.setText("");
                    profession_spinner.setSelection(0);
                    selected_profession = 0;
                    identity_file.setText("");
                    email.setText("");
                    facebook.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (username.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(RegisterActivity.this,getResources().getString(R.string.register_no_name));
                    return;
                }
                if (phone.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(RegisterActivity.this,getResources().getString(R.string.register_no_phone));
                    return;
                }
                if (phone.getText().toString().length()<10){
                    BaseFunctions.showWarningToast(RegisterActivity.this,getResources().getString(R.string.register_phone_short));
                    return;
                }
                if (password.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(RegisterActivity.this,getResources().getString(R.string.register_no_password));
                    return;
                }
                if (password.getText().toString().length()<8){
                    BaseFunctions.showWarningToast(RegisterActivity.this,getResources().getString(R.string.register_password_short));
                    return;
                }
                if (selected_type.equals("")){
                    BaseFunctions.showWarningToast(RegisterActivity.this,getResources().getString(R.string.register_no_type));
                    return;
                }
                if (selected_type.equals("3")){
                    if (imagePath.equals("")){
                        BaseFunctions.showWarningToast(RegisterActivity.this,getResources().getString(R.string.register_no_image));
                        return;
                    }
                    if (selected_medical_type == 0){
                        BaseFunctions.showWarningToast(RegisterActivity.this,getResources().getString(R.string.register_no_medical_account_type));
                        return;
                    }
                    if (selected_city == 0){
                        BaseFunctions.showWarningToast(RegisterActivity.this,getResources().getString(R.string.register_no_city));
                        return;
                    }
                    if (selected_profession == 0){
                        BaseFunctions.showWarningToast(RegisterActivity.this,getResources().getString(R.string.register_no_profession));
                        return;
                    }
                    if (address.getText().toString().equals("")){
                        BaseFunctions.showWarningToast(RegisterActivity.this,getResources().getString(R.string.register_no_address));
                        return;
                    }
                    if (identity_file.getText().toString().equals("")){
                        BaseFunctions.showWarningToast(RegisterActivity.this,getResources().getString(R.string.register_no_identity_file));
                        return;
                    }
                    if (email.getText().toString().equals("")){
                        BaseFunctions.showWarningToast(RegisterActivity.this,getResources().getString(R.string.register_no_email));
                        return;
                    }
                    if (facebook.getText().toString().equals("")){
                        BaseFunctions.showWarningToast(RegisterActivity.this,getResources().getString(R.string.register_no_facebook));
                        return;
                    }
                }
                callRegisterAPI(username.getText().toString(),phone.getText().toString(),password.getText().toString(),selected_type);
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1000){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
                    ImagePicker.Companion.with(RegisterActivity.this)
                            .galleryOnly()
                            .cropSquare() 			//Crop image(Optional), Check Customization for more option
                            .compress(1024)			//Final image size will be less than 1 MB(Optional)
                            .start();
                }else {
                    BSImagePicker singleSelectionPicker = new BSImagePicker.Builder("com.scit.tabibak.provider")
                            .hideCameraTile() //Default: show. Set this if you don't want user to take photo.
                            .hideGalleryTile() //Default: show. Set this if you don't want to further let user select from a gallery app. In such case, I suggest you to set maximum displaying images to Integer.MAX_VALUE.
                            .build();
                    singleSelectionPicker.show(getSupportFragmentManager(),"Picker");
                }



            }else {
                BaseFunctions.showErrorToast(RegisterActivity.this,getResources().getString(R.string.no_permission));
            }
        }
    }

    private void init_spinner(){
        type_list = new ArrayList<>();
        city_list = new ArrayList<>();
        city_list_string = new ArrayList<>();
        profession_list = new ArrayList<>();
        profession_list_string = new ArrayList<>();
        medical_type_list = new ArrayList<>();
        medical_type_list_string = new ArrayList<>();

        type_list.add(getResources().getString(R.string.register_pick_account_type));
        type_list.add(getResources().getString(R.string.register_normal_user));
        type_list.add(getResources().getString(R.string.register_doctor));

        city_list_string.add(getResources().getString(R.string.guide_city));
        city_list.add(new CityObject());
        if (SharedPrefManager.getInstance(RegisterActivity.this).getCities().size()>0){
            for (CityObject c : SharedPrefManager.getInstance(RegisterActivity.this).getCities()){
                city_list.add(c);
                city_list_string.add(c.getName());
            }
        }

        profession_list_string.add(getResources().getString(R.string.guide_pick_profession));
        profession_list.add(new SpecializationObject());
        if (SharedPrefManager.getInstance(RegisterActivity.this).getSpes().size()>0){
            for (SpecializationObject s : SharedPrefManager.getInstance(RegisterActivity.this).getSpes()){
                profession_list.add(s);
                profession_list_string.add(s.getName());
            }
        }

        medical_type_list_string.add(getResources().getString(R.string.register_medical_type));
        medical_type_list.add(new FacilityObject());
        if (SharedPrefManager.getInstance(RegisterActivity.this).getFacilities().size()>0){
            for (FacilityObject f : SharedPrefManager.getInstance(RegisterActivity.this).getFacilities()){
                medical_type_list.add(f);
                medical_type_list_string.add(f.getName());
            }
        }
        BaseFunctions.init_spinner(RegisterActivity.this,type_spinner,type_adapter,type_list);
        BaseFunctions.init_spinner(RegisterActivity.this,city_spinner,city_adapter,city_list_string);
        BaseFunctions.init_spinner(RegisterActivity.this,profession_spinner,profession_adapter,profession_list_string);
        BaseFunctions.init_spinner(RegisterActivity.this,medical_type_spinner,medical_type_adapter,medical_type_list_string);
    }

    private void callRegisterAPI(String name,String phone,String password,String role_id){
        String address_text = "no Address";
        String identity_file_text = "no identity file";
        String email_text = "";
        String faceook_text = "https://www.facebook.com/profile";
        if (!address.getText().toString().equals("")){
            address_text = address.getText().toString();
        }
        if (!identity_file.getText().toString().equals("")){
            identity_file_text = identity_file.getText().toString();
        }
        //if (!email.getText().toString().equals("")){
        //    email_text = email.getText().toString();
        //}
        if (!facebook.getText().toString().equals("")){
            faceook_text = facebook.getText().toString();
        }
        show_loading(true);
        UserAPIsClass.register(
                RegisterActivity.this,
                name,
                phone,
                password,
                role_id,
                String.valueOf(selected_city),
                String.valueOf(selected_profession),
                address_text,
                identity_file_text,
                email_text,
                faceook_text,
                String.valueOf(selected_medical_type),
                imagePath,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        show_loading(false);
                        //BaseFunctions.showErrorToast(RegisterActivity.this,getResources().getString(R.string.error_occured_2));
                    }

                    @Override
                    public void onResponse(Object json) {
                        show_loading(false);
                        BaseFunctions.showSuccessToast(RegisterActivity.this,getResources().getString(R.string.register_success));
                        finish();
                        BaseFunctions.runBackSlideAnimation(RegisterActivity.this);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        show_loading(false);
                        BaseFunctions.showErrorToast(RegisterActivity.this,getResources().getString(R.string.no_internet));
                    }
                });
    }

    private void show_loading(boolean status){
        if (status){
            loading.setVisibility(View.VISIBLE);
            btn_register.setVisibility(View.GONE);
            username.setEnabled(false);
            phone.setEnabled(false);
            password.setEnabled(false);
        }else {
            loading.setVisibility(View.GONE);
            btn_register.setVisibility(View.VISIBLE);
            username.setEnabled(true);
            phone.setEnabled(true);
            password.setEnabled(true);
        }
    }

    @Override
    public void loadImage(Uri imageUri, ImageView ivImage) {
        Glide.with(RegisterActivity.this).load(imageUri).into(ivImage);
    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        Uri source_uri = uri;
        Uri destination_uri = Uri.fromFile(getTempFile());
        Crop.of(source_uri,destination_uri).asSquare().start(RegisterActivity.this);
    }

    private File getTempFile() {
        if (isSDCARDMounted()) {

            File f = new File(Environment.getExternalStorageDirectory(),TEMP_PHOTO_FILE);
            try {
                f.createNewFile();
            } catch (IOException e) {

            }
            return f;
        } else {
            return null;
        }
    }

    private boolean isSDCARDMounted(){
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED))
            return true;
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        switch (requestCode) {
            case Crop.REQUEST_CROP:{
                handle_crop(resultCode,data);
            }break;
            default:{
                if (resultCode == Activity.RESULT_OK) {
                    imagePath = ImagePicker.Companion.getFilePath(data);
                    Bitmap myBitmap = BitmapFactory.decodeFile(imagePath);
                    profile_pic.setImageBitmap(myBitmap);
                    profile_pic_title.setVisibility(View.GONE);
                }
            }
        }
    }

    private void handle_crop(int code, final Intent data){
        if (code == RESULT_OK){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String filePath=  Environment.getExternalStorageDirectory()
                            + "/"+"temporary_holder.jpg";
                    if (new File(filePath).exists()){
                        imagePath = filePath;
                        Bitmap myBitmap = BitmapFactory.decodeFile(imagePath);
                        profile_pic.setImageBitmap(myBitmap);
                        profile_pic_title.setVisibility(View.GONE);
                    }

                }
            });
        }
    }
}
