package com.scit.tabibak.Models.Health_record_models;

import com.google.gson.annotations.SerializedName;

public class HealthMedicalImageObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("image") private String image = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
