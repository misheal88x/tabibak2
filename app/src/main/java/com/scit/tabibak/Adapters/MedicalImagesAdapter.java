package com.scit.tabibak.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scit.tabibak.APIsClasses.HealthRecordAPIsClass;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.ILoadImage;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Interfaces.OnItemClickListener;
import com.scit.tabibak.Models.MedicalImageObject;
import com.scit.tabibak.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class MedicalImagesAdapter extends RecyclerView.Adapter<MedicalImagesAdapter.ViewHolder>{
    private Context context;
    private List<MedicalImageObject> images;

    public MedicalImagesAdapter(Context context,List<MedicalImageObject> images) {
        this.context = context;
        this.images = images;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;
        private RelativeLayout delete;
        private ProgressBar loading;
        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            delete = view.findViewById(R.id.delete);
            loading = view.findViewById(R.id.loading);
        }
    }

    @Override
    public int getItemCount() {
        return images.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_medical_image, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        //Bitmap myBitmap = BitmapFactory.decodeFile(images.get(position));
        if (images.get(position).getImageBitmap()!=null){
            holder.image.setImageBitmap(images.get(position).getImageBitmap());
            holder.delete.setVisibility(View.VISIBLE);
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage(context.getResources().getString(R.string.health_delete_image_message));
                    builder.setPositiveButton(context.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            images.remove(position);
                            notifyDataSetChanged();
                        }
                    });
                    builder.setNegativeButton(context.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.show();

                }
            });
        }else {
            BaseFunctions.setGlideImage(context,holder.image,images.get(position).getImageUrl());
            if (images.get(position).getId()!=0){
                holder.delete.setVisibility(View.VISIBLE);
                holder.delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage(context.getResources().getString(R.string.health_delete_image_message));
                        builder.setPositiveButton(context.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                callRemoveImageAPI(holder,images.get(position).getId(),position);
                            }
                        });
                        builder.setNegativeButton(context.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        builder.show();
                    }
                });
            }else {
                holder.delete.setVisibility(View.GONE);
            }
        }
    }

    private void callRemoveImageAPI(final ViewHolder holder,int id,final int position){
        holder.loading.setVisibility(View.VISIBLE);
        HealthRecordAPIsClass.remove_image(context, id, new IResponse() {
            @Override
            public void onResponse() {
                holder.loading.setVisibility(View.GONE);
            }

            @Override
            public void onResponse(Object json) {
                holder.loading.setVisibility(View.GONE);
                images.remove(position);
                notifyDataSetChanged();
            }
        }, new IFailure() {
            @Override
            public void onFailure() {
                holder.loading.setVisibility(View.GONE);
                BaseFunctions.showErrorToast(context,context.getResources().getString(R.string.no_internet));
            }
        });
    }
}
