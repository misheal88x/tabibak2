package com.scit.tabibak.APIs;

import com.scit.tabibak.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MedicalCunsulationAPIs {
    @FormUrlEncoded
    @POST("add-post")
    Call<BaseResponse> add_question(
            @Header("Accept") String accept,
            @Field("details") String details
    );

    @GET("posts")
    Call<BaseResponse> get_all(
            @Header("Accept") String accept,
            @Query("page") int page
    );

    @GET("delete-post/{id}")
    Call<BaseResponse> delete_question(
            @Header("Accept") String accept,
            @Path("id") String id
    );

    @GET("replies/{id}")
    Call<BaseResponse> get_replies(
            @Header("Accept") String accept,
            @Path("id") String id
    );

    @GET("delete-reply/{id}")
    Call<BaseResponse> delete_reply(
            @Header("Accept") String accept,
            @Path("id") String id
    );

    @FormUrlEncoded
    @POST("add-reply")
    Call<BaseResponse> add_reply(
            @Header("Accept") String accept,
            @Field("text") String text,
            @Field("post_id") int post_id
    );
}
