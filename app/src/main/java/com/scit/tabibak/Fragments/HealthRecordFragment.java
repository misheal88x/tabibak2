package com.scit.tabibak.Fragments;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.scit.tabibak.APIsClasses.HealthRecordAPIsClass;
import com.scit.tabibak.APIsClasses.ImportantNotesAPIsClass;
import com.scit.tabibak.APIsClasses.MedicalNotesAPIsClass;
import com.scit.tabibak.Activities.LoginActivity;
import com.scit.tabibak.Activities.SplashActivity;
import com.scit.tabibak.Adapters.HealthNotesAdapter;
import com.scit.tabibak.Adapters.ImportantNotesAdapter;
import com.scit.tabibak.Adapters.MedicalImagesAdapter;
import com.scit.tabibak.Adapters.MyHealthRecordTextsAdapter;
import com.scit.tabibak.Bases.BaseFragment;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Dialogs.ContactUsDialog;
import com.scit.tabibak.Dialogs.HealthNoteDetailsDialog;
import com.scit.tabibak.Dialogs.MyHealthRecordDialog;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Interfaces.OnItemClickListener;
import com.scit.tabibak.Models.Important_notes_models.ImportantNoteObject;
import com.scit.tabibak.Models.MedicalImageObject;
import com.scit.tabibak.Models.Medical_notes_models.MedicalNoteObject;
import com.scit.tabibak.Models.UserObject;
import com.scit.tabibak.R;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.app.Activity.RESULT_OK;

public class HealthRecordFragment extends BaseFragment implements  BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.ImageLoaderDelegate,OnItemClickListener{

    private RelativeLayout toolbar;
    private TextView title;
    private RelativeLayout call,help,logout;

    private EditText my_name,my_age,my_weight,my_height;
    private CheckBox chk_blood_pressure,chk_heart,chk_diabetes,chk_tumors,chk_malignant,with_alarm;
    private Button add_previous_disease,add_chronic_diseases,add_continue_medicine,add_medical_image,add_note,add_note_image,view_health_record,edit_health_record;
    private LinearLayout previous_disease_layout,chronic_diseases_layout,continue_medicine_layout,medical_image_layout,add_note_layout;
    private EditText previous_disease_name,chronic_diseases_name,continue_medicine_name,note_title,note_desc;
    private TextView previous_disease_add,chronic_diseases_add,continue_medicine_add,note_date,note_time,note_add;
    private RecyclerView previous_disease_recycler,chronic_diseases_recycler,continue_medicine_recycler,notes_recycler,medical_images_recycler;
    private List<MedicalNoteObject> list_of_notes;
    private List<String> previous_disease_list,chronic_diseases_list,continue_medicine_list;
    private MyHealthRecordTextsAdapter previous_disease_adapter,chronic_diseases_adapter,continue_medicine_adapter;
    private HealthNotesAdapter notes_adapter;
    private MedicalImagesAdapter images_adapter;
    private LinearLayoutManager previous_disease_layout_manager,chronic_diseases_layout_manager,continue_medicine_layout_manager,
    notes_layout_manager,images_layout_manager;
    private List<MedicalImageObject> images_list;
    private String noteImagePath = "";
    private  String TEMP_PHOTO_FILE = "temporary_holder.jpg";
    private ImageView note_image;
    private int clicked_image_type = 0;

    private String selected_note_date = "";
    private String selected_note_time = "";

    private Animation fadeIn,fadeOut;
    private ImageView minimize;
    private TextView added_notes;
    private ProgressBar loading,loading_add;
    private LinearLayout no_data,no_internet,error;

    private LinearLayout health_record_layout;
    private LinearLayout health_notes_layout;
    private LinearLayout no_internet_base_layout;
    private ProgressBar loading_base_data,loading_save_record;
    private TextView save_health_record;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_health_record,container,false);
    }

    @Override
    public void init_views() {
        //Toolbar
        toolbar = base.findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        call = toolbar.findViewById(R.id.contact_us);
        help = toolbar.findViewById(R.id.help);
        logout = toolbar.findViewById(R.id.logout);
        if (!SharedPrefManager.getInstance(base).getLogged()){
            logout.setVisibility(View.GONE);
        }
        //EditText
        my_name = base.findViewById(R.id.name);
        my_age = base.findViewById(R.id.age);
        my_weight = base.findViewById(R.id.weight);
        my_height = base.findViewById(R.id.height);
        previous_disease_name = base.findViewById(R.id.previous_disease_name);
        chronic_diseases_name = base.findViewById(R.id.chronic_diseases_name);
        continue_medicine_name = base.findViewById(R.id.continue_medicine_name);
        note_title = base.findViewById(R.id.note_title);
        note_desc = base.findViewById(R.id.note_desc);
        //Checkbox
        chk_blood_pressure = base.findViewById(R.id.blood_pressure);
        chk_heart = base.findViewById(R.id.heart);
        chk_diabetes = base.findViewById(R.id.diabetes);
        chk_tumors = base.findViewById(R.id.tumors);
        chk_malignant = base.findViewById(R.id.malignant);
        with_alarm = base.findViewById(R.id.with_alarm);
        //Button
        add_previous_disease = base.findViewById(R.id.add_previous_disease);
        add_chronic_diseases = base.findViewById(R.id.add_chronic_diseases);
        add_continue_medicine = base.findViewById(R.id.add_continue_medicine);
        add_medical_image = base.findViewById(R.id.add_medical_image);
        add_note = base.findViewById(R.id.add_note);
        add_note_image = base.findViewById(R.id.add_note_image);
        view_health_record = base.findViewById(R.id.view_health_record);
        edit_health_record = base.findViewById(R.id.edit_health_record);
        //LinearLayout
        previous_disease_layout = base.findViewById(R.id.add_previous_disease_layout);
        chronic_diseases_layout  = base.findViewById(R.id.add_chronic_diseases_layout);
        continue_medicine_layout = base.findViewById(R.id.add_continue_medicine_layout);
        medical_image_layout = base.findViewById(R.id.add_medical_image_layout);
        add_note_layout = base.findViewById(R.id.add_note_layout);
        no_data = base.findViewById(R.id.no_data_layout);
        no_internet = base.findViewById(R.id.no_internet_layout);
        error = base.findViewById(R.id.error_layout);
        health_record_layout = base.findViewById(R.id.health_record_layout);
        health_notes_layout = base.findViewById(R.id.health_notes_layout);
        no_internet_base_layout = base.findViewById(R.id.no_internet_base_layout);
        //TextView
        previous_disease_add = base.findViewById(R.id.previous_disease_add);
        chronic_diseases_add = base.findViewById(R.id.chronic_diseases_add);
        continue_medicine_add = base.findViewById(R.id.continue_medicine_add);
        note_date = base.findViewById(R.id.note_date);
        note_time = base.findViewById(R.id.note_time);
        note_add = base.findViewById(R.id.add_note_btn);
        added_notes = base.findViewById(R.id.added_notes_title);
        save_health_record = base.findViewById(R.id.save_health_record);
        //RecyclerView
        previous_disease_recycler = base.findViewById(R.id.previous_diseases_recycler);
        chronic_diseases_recycler = base.findViewById(R.id.chronic_diseases_recycler);
        continue_medicine_recycler = base.findViewById(R.id.continue_medicine_recycler);
        notes_recycler = base.findViewById(R.id.notes_recycler);
        medical_images_recycler = base.findViewById(R.id.medical_images_recycler);
        //ImageView
        note_image = base.findViewById(R.id.note_image);
        minimize = base.findViewById(R.id.minimize);
        //Animation
        fadeIn = AnimationUtils.loadAnimation(base,R.anim.fade_in);
        fadeOut = AnimationUtils.loadAnimation(base,R.anim.fade_out);
        //ProgressBars
        loading = base.findViewById(R.id.loading);
        loading_add = base.findViewById(R.id.loading_add);
        loading_base_data = base.findViewById(R.id.loading_base_data);
        loading_save_record = base.findViewById(R.id.loading_save_record);
    }

    @Override
    public void init_events() {
        view_health_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyHealthRecordDialog dialog = new MyHealthRecordDialog(base,"mine",0);
                dialog.show();
            }
        });
        edit_health_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base.open_fragment(new EditMedicalRecordFragment());
            }
        });
        add_previous_disease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_previous_disease.setVisibility(View.GONE);
                previous_disease_layout.setVisibility(View.VISIBLE);
                previous_disease_layout.startAnimation(fadeIn);
            }
        });
        add_continue_medicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_continue_medicine.setVisibility(View.GONE);
                continue_medicine_layout.setVisibility(View.VISIBLE);
                continue_medicine_layout.startAnimation(fadeIn);
            }
        });
        add_chronic_diseases.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_chronic_diseases.setVisibility(View.GONE);
                chronic_diseases_layout.setVisibility(View.VISIBLE);
                chronic_diseases_layout.startAnimation(fadeIn);
            }
        });
        add_medical_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicked_image_type = 1;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE},1000);
            }
        });
        add_note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_note.setVisibility(View.GONE);
                add_note_layout.setVisibility(View.VISIBLE);
                add_note_layout.startAnimation(fadeIn);
                minimize.setVisibility(View.VISIBLE);
            }
        });
        minimize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                note_title.setText("");
                note_desc.setText("");
                noteImagePath = "";
                note_image.setVisibility(View.GONE);
                with_alarm.setChecked(false);
                note_date.setText(getResources().getString(R.string.health_note_date));
                note_time.setText(getResources().getString(R.string.health_note_time));
                note_date.setVisibility(View.GONE);
                note_time.setVisibility(View.GONE);
                add_note_layout.setVisibility(View.GONE);
                add_note.setVisibility(View.VISIBLE);
                minimize.setVisibility(View.GONE);
            }
        });
        previous_disease_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //add_previous_disease.setVisibility(View.VISIBLE);
                //previous_disease_layout.setVisibility(View.GONE);
                if (previous_disease_name.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.health_record_previous_disease_empty));
                    return;
                }
                previous_disease_list.add(previous_disease_name.getText().toString());
                previous_disease_adapter.notifyDataSetChanged();
                previous_disease_name.setText("");
            }
        });
        continue_medicine_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //add_continue_medicine.setVisibility(View.VISIBLE);
                //continue_medicine_layout.setVisibility(View.GONE);
                if (continue_medicine_name.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.health_record_continue_medicine_empty));
                    return;
                }
                continue_medicine_list.add(continue_medicine_name.getText().toString());
                continue_medicine_adapter.notifyDataSetChanged();
                continue_medicine_name.setText("");
            }
        });
        chronic_diseases_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //add_chronic_diseases.setVisibility(View.VISIBLE);
                //chronic_diseases_layout.setVisibility(View.GONE);
                if (chronic_diseases_name.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.health_record_chronic_disease_empty));
                    return;
                }
                chronic_diseases_list.add(chronic_diseases_name.getText().toString());
                chronic_diseases_adapter.notifyDataSetChanged();
                chronic_diseases_name.setText("");
            }
        });
        note_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (note_title.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.important_no_title));
                    return;
                }
                if (note_desc.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.important_no_desc));
                    return;
                }
                if (noteImagePath.equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.important_no_image));
                    return;
                }
                if (with_alarm.isChecked()){
                    if (selected_note_date.equals("")){
                        BaseFunctions.showWarningToast(base,getResources().getString(R.string.important_no_date));
                        return;
                    }
                    if (selected_note_time.equals("")){
                        BaseFunctions.showWarningToast(base,getResources().getString(R.string.important_no_time));
                        return;
                    }
                }
                callAddNoteAPI(note_title.getText().toString(),note_desc.getText().toString());
                /*
                note_title.setText("");
                note_desc.setText("");
                noteImagePath = "";
                note_image.setVisibility(View.GONE);
                with_alarm.setChecked(false);
                note_date.setText(getResources().getString(R.string.health_note_date));
                note_time.setText(getResources().getString(R.string.health_note_time));
                note_date.setVisibility(View.GONE);
                note_time.setVisibility(View.GONE);
                add_note_layout.setVisibility(View.GONE);
                add_note.setVisibility(View.VISIBLE);

                 */
            }
        });
        with_alarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    note_date.setVisibility(View.VISIBLE);
                    note_date.startAnimation(fadeIn);
                    note_time.setVisibility(View.VISIBLE);
                    note_time.startAnimation(fadeIn);
                }else {
                    note_date.setVisibility(View.GONE);
                    note_time.setVisibility(View.GONE);
                    note_date.setText(getResources().getString(R.string.health_note_date));
                    note_time.setText(getResources().getString(R.string.health_note_time));
                }
            }
        });
        add_note_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicked_image_type = 2;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE},1000);
            }
        });

        note_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar newCalendar = Calendar.getInstance();
                DatePickerDialog cal = new DatePickerDialog(base, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, month, day);

                        String year_str = new String();
                        String month_str = new String();
                        String day_str = new String();
                        year_str = String.valueOf(year);
                        if (month + 1 < 10) {
                            month_str = "0" + String.valueOf(month + 1);
                        } else {
                            month_str = String.valueOf(month + 1);
                        }
                        if (day < 10) {
                            day_str = "0" + String.valueOf(day);
                        } else {
                            day_str = String.valueOf(day);
                        }
                        String desDate = year_str + "-" + month_str + "-" + day_str;
                        selected_note_date = desDate;
                        note_date.setText(selected_note_date);
                    }

                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                cal.show();
            }
        });
        note_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar newCalendar = Calendar.getInstance();
                TimePickerDialog time_picker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hour, int minute) {
                        String hour_str = new String();
                        String minute_str = new String();
                        if (hour<10){
                            hour_str = "0"+String.valueOf(hour);
                        }else {
                            hour_str = String.valueOf(hour);
                        }
                        if (minute<10){
                            minute_str = "0"+String.valueOf(minute);
                        }else {
                            minute_str = String.valueOf(minute);
                        }
                        String fullTime = hour_str+":"+minute_str+":00";

                        selected_note_time = fullTime;
                        note_time.setText(selected_note_time);
                    }
                },newCalendar.get(Calendar.HOUR_OF_DAY),newCalendar.get(Calendar.MINUTE), DateFormat.is24HourFormat(base));
                time_picker.show();
            }
        });
        error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callGetNotesAPI();
            }
        });

        save_health_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (my_name.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.health_record_no_name));
                    return;
                }
                if (my_age.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.health_record_no_age));
                    return;
                }
                if (my_weight.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.health_record_no_weight));
                    return;
                }
                if (my_height.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.health_record_no_height));
                    return;
                }
                callSaveRecordAPI();
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactUsDialog dialog = new ContactUsDialog(base);
                dialog.show();
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base.open_fragment(new HelpFragment());
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(base);
                builder.setTitle(getResources().getString(R.string.logout_title));
                builder.setMessage(getResources().getString(R.string.logout_message));
                builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPrefManager.getInstance(base).setLogged(false);
                        SharedPrefManager.getInstance(base).setUser(new UserObject());
                        startActivity(new Intent(base, SplashActivity.class));
                        base.finish();
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        title.setText(getResources().getString(R.string.record_health_record));
        init_recyclers();
        callCheckRecordAPI();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1000){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
                    ImagePicker.Companion.with(HealthRecordFragment.this)
                            .galleryOnly()
                            .crop(16f,9f)	    			//Crop image(Optional), Check Customization for more option
                            .compress(1024)			//Final image size will be less than 1 MB(Optional)
                            .start();
                }else {
                    BSImagePicker singleSelectionPicker = new BSImagePicker.Builder("com.scit.tabibak.provider")
                            .hideCameraTile() //Default: show. Set this if you don't want user to take photo.
                            .hideGalleryTile() //Default: show. Set this if you don't want to further let user select from a gallery app. In such case, I suggest you to set maximum displaying images to Integer.MAX_VALUE.
                            .build();
                    singleSelectionPicker.show(getChildFragmentManager(),"Picker");
                }

            }else {
                BaseFunctions.showErrorToast(base,getResources().getString(R.string.no_permission));
            }
        }
    }



    private void init_recyclers(){
        images_list = new ArrayList<>();
        list_of_notes = new ArrayList<>();
        previous_disease_list = new ArrayList<>();
        chronic_diseases_list = new ArrayList<>();
        continue_medicine_list = new ArrayList<>();

        previous_disease_adapter = new MyHealthRecordTextsAdapter(base,previous_disease_list,true);
        chronic_diseases_adapter = new MyHealthRecordTextsAdapter(base,chronic_diseases_list,true);
        continue_medicine_adapter = new MyHealthRecordTextsAdapter(base,continue_medicine_list,true);
        notes_adapter = new HealthNotesAdapter(base,list_of_notes,this);
        images_adapter = new MedicalImagesAdapter(base,images_list);

        previous_disease_layout_manager = new LinearLayoutManager(base);
        chronic_diseases_layout_manager = new LinearLayoutManager(base);
        continue_medicine_layout_manager = new LinearLayoutManager(base);
        notes_layout_manager = new LinearLayoutManager(base);
        images_layout_manager = new LinearLayoutManager(base,LinearLayoutManager.HORIZONTAL,false);

        previous_disease_recycler.setLayoutManager(previous_disease_layout_manager);
        chronic_diseases_recycler.setLayoutManager(chronic_diseases_layout_manager);
        continue_medicine_recycler.setLayoutManager(continue_medicine_layout_manager);
        notes_recycler.setLayoutManager(notes_layout_manager);
        medical_images_recycler.setLayoutManager(images_layout_manager);

        previous_disease_recycler.setAdapter(previous_disease_adapter);
        chronic_diseases_recycler.setAdapter(chronic_diseases_adapter);
        continue_medicine_recycler.setAdapter(continue_medicine_adapter);
        notes_recycler.setAdapter(notes_adapter);
        medical_images_recycler.setAdapter(images_adapter);
    }

    @Override
    public void loadImage(Uri imageUri, ImageView ivImage) {
        Glide.with(base).load(imageUri).into(ivImage);
    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        Uri source_uri = uri;
        Uri destination_uri = Uri.fromFile(getTempFile());
        Crop.of(source_uri,destination_uri).withAspect(16,9).start(base,HealthRecordFragment.this);
    }

    private File getTempFile() {
        if (isSDCARDMounted()) {
            int oneTimeID = (int) SystemClock.uptimeMillis();
            TEMP_PHOTO_FILE = oneTimeID+"_"+"temporary_holder.jpg";
            File f = new File(Environment.getExternalStorageDirectory(),TEMP_PHOTO_FILE);
            try {
                f.createNewFile();
            } catch (IOException e) {

            }
            return f;
        } else {
            return null;
        }
    }

    private boolean isSDCARDMounted(){
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED))
            return true;
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Crop.REQUEST_CROP:{
                handle_crop(resultCode,data);
            }break;
            default:{
                if (resultCode == Activity.RESULT_OK) {
                    String filePath = ImagePicker.Companion.getFilePath(data);
                    if (clicked_image_type == 1){
                        MedicalImageObject o = new MedicalImageObject();
                        o.setImagePath(filePath);
                        Bitmap myBitmap = BitmapFactory.decodeFile(filePath);
                        o.setImageBitmap(myBitmap);
                        images_list.add(o);
                        images_adapter.notifyDataSetChanged();
                    }else {
                        noteImagePath = filePath;
                        Bitmap myBitmap = BitmapFactory.decodeFile(noteImagePath);
                        note_image.setImageBitmap(myBitmap);
                        note_image.setVisibility(View.VISIBLE);
                        note_image.startAnimation(fadeIn);
                    }
                }
            }
        }
    }

    private void handle_crop(int code, final Intent data){
        if (code == RESULT_OK){
            base.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String filePath=  Environment.getExternalStorageDirectory()
                            + "/"+TEMP_PHOTO_FILE;
                    if (new File(filePath).exists()){
                        if (clicked_image_type == 1){
                            MedicalImageObject o = new MedicalImageObject();
                            o.setImagePath(filePath);
                            Bitmap myBitmap = BitmapFactory.decodeFile(filePath);
                            o.setImageBitmap(myBitmap);
                           images_list.add(o);
                           images_adapter.notifyDataSetChanged();
                        }else {
                            noteImagePath = filePath;
                            Bitmap myBitmap = BitmapFactory.decodeFile(noteImagePath);
                            note_image.setImageBitmap(myBitmap);
                            note_image.setVisibility(View.VISIBLE);
                            note_image.startAnimation(fadeIn);
                        }

                    }

                }
            });
        }
    }

    @Override
    public void onItemClick(int position) {
        HealthNoteDetailsDialog dialog = new HealthNoteDetailsDialog(base,list_of_notes.get(position));
        dialog.show();
    }

    private void callGetNotesAPI(){
        loading.setVisibility(View.VISIBLE);
        added_notes.setVisibility(View.GONE);
        list_of_notes.clear();
        notes_adapter = new HealthNotesAdapter(base,list_of_notes,this);
        notes_recycler.setAdapter(notes_adapter);
        no_data.setVisibility(View.GONE);
        error.setVisibility(View.GONE);
        no_internet.setVisibility(View.GONE);
        MedicalNotesAPIsClass.get_all(base,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        loading.setVisibility(View.GONE);
                        error.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        loading.setVisibility(View.GONE);
                        String j = new Gson().toJson(json);
                        try {
                            MedicalNoteObject[] success = new Gson().fromJson(j,MedicalNoteObject[].class);
                            if (success.length>0){
                                added_notes.setVisibility(View.VISIBLE);
                                for (MedicalNoteObject o : success){
                                    list_of_notes.add(o);
                                    notes_adapter.notifyDataSetChanged();
                                }
                            }else {
                                added_notes.setVisibility(View.GONE);
                                no_data.setVisibility(View.VISIBLE);
                            }
                        }catch (Exception e){
                            callGetNotesAPI();
                        }

                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        loading.setVisibility(View.GONE);
                        no_internet.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void callAddNoteAPI(String title,String details){
        showAddLoading(true);
        MedicalNotesAPIsClass.add_note(
                base,
                title,
                details,
                selected_note_date,
                selected_note_time,
                noteImagePath,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        showAddLoading(false);
                    }

                    @Override
                    public void onResponse(Object json) {
                        showAddLoading(false);
                        BaseFunctions.showSuccessToast(base,getResources().getString(R.string.file_add_success));
                        note_title.setText("");
                        note_desc.setText("");
                        noteImagePath = "";
                        note_image.setVisibility(View.GONE);
                        with_alarm.setChecked(false);
                        note_date.setText(getResources().getString(R.string.health_note_date));
                        note_time.setText(getResources().getString(R.string.health_note_time));
                        note_date.setVisibility(View.GONE);
                        note_time.setVisibility(View.GONE);
                        add_note_layout.setVisibility(View.GONE);
                        add_note.setVisibility(View.VISIBLE);
                        minimize.setVisibility(View.GONE);
                        BaseFunctions.playMusic(base,R.raw.added);
                        callGetNotesAPI();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        showAddLoading(false);
                        BaseFunctions.showErrorToast(base,getResources().getString(R.string.no_internet));
                    }
                });
    }

    private void showAddLoading(boolean state){
        if (state){
            loading_add.setVisibility(View.VISIBLE);
            note_add.setVisibility(View.GONE);
            note_title.setEnabled(false);
            note_desc.setEnabled(false);
            add_note_image.setEnabled(false);
        }else {
            loading_add.setVisibility(View.GONE);
            note_add.setVisibility(View.VISIBLE);
            note_title.setEnabled(true);
            note_desc.setEnabled(true);
            add_note_image.setEnabled(true);
        }
    }

    private void callCheckRecordAPI(){
        loading_base_data.setVisibility(View.VISIBLE);
        health_record_layout.setVisibility(View.GONE);
        health_notes_layout.setVisibility(View.GONE);
        HealthRecordAPIsClass.check_record(
                base,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        loading_base_data.setVisibility(View.GONE);
                        health_record_layout.setVisibility(View.VISIBLE);
                        my_name.setText(SharedPrefManager.getInstance(base).getUser().getName());
                    }

                    @Override
                    public void onResponse(Object json) {
                        loading_base_data.setVisibility(View.GONE);
                        health_notes_layout.setVisibility(View.VISIBLE);
                        callGetNotesAPI();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        loading_base_data.setVisibility(View.GONE);
                        no_internet_base_layout.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void callSaveRecordAPI(){
        String name_text = my_name.getText().toString();
        String age_text = my_age.getText().toString();
        String weight_text = my_weight.getText().toString();
        String height_text = my_height.getText().toString();

        List<String> diseases_list = new ArrayList<>();
        if (chk_blood_pressure.isChecked()){
            diseases_list.add("1");
        }
        if (chk_diabetes.isChecked()){
            diseases_list.add("3");
        }
        if (chk_heart.isChecked()){
            diseases_list.add("2");
        }
        if (chk_malignant.isChecked()){
            diseases_list.add("5");
        }
        if (chk_tumors.isChecked()){
            diseases_list.add("4");
        }

        List<HealthRecordContents> previous_disease_content_list = new ArrayList<>();
        List<HealthRecordContents> chronic_diseases_content_list = new ArrayList<>();
        List<HealthRecordContents> continue_medicine_content_list = new ArrayList<>();


        if (previous_disease_list.size()>0){
            for (String s : previous_disease_list){
                previous_disease_content_list.add(new HealthRecordContents(s));
            }
        }
        if (chronic_diseases_list.size()>0){
            for (String s : chronic_diseases_list){
                chronic_diseases_content_list.add(new HealthRecordContents(s));
            }
        }
        if (continue_medicine_list.size()>0){
            for (String s : continue_medicine_list){
                continue_medicine_content_list.add(new HealthRecordContents(s));
            }
        }
        List<String> images_paths_string = new ArrayList<>();
        if (images_list.size()>0){
            for (MedicalImageObject o : images_list){
                images_paths_string.add(o.getImagePath());
            }
        }

        save_health_record.setVisibility(View.GONE);
        loading_save_record.setVisibility(View.VISIBLE);
        HealthRecordAPIsClass.save_record(
                base,
                name_text,
                age_text,
                height_text,
                weight_text,
                new Gson().toJson(diseases_list),
                new Gson().toJson(previous_disease_content_list),
                new Gson().toJson(chronic_diseases_content_list),
                new Gson().toJson(continue_medicine_content_list),
                images_paths_string,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        save_health_record.setVisibility(View.VISIBLE);
                        loading_save_record.setVisibility(View.GONE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        save_health_record.setVisibility(View.VISIBLE);
                        loading_save_record.setVisibility(View.GONE);
                        health_record_layout.setVisibility(View.GONE);
                        health_notes_layout.setVisibility(View.VISIBLE);
                        BaseFunctions.showSuccessToast(base,getResources().getString(R.string.health_record_saved_success));
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        save_health_record.setVisibility(View.VISIBLE);
                        loading_save_record.setVisibility(View.GONE);
                        BaseFunctions.showErrorToast(base,getResources().getString(R.string.no_internet));
                    }
                });
    }

    class HealthRecordContents{
        @SerializedName("name") private String name = "";

        public HealthRecordContents(String name) {
            this.name = name;
        }
    }
}
