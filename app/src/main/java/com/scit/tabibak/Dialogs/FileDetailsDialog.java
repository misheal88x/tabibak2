package com.scit.tabibak.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Interfaces.ILoadImage;
import com.scit.tabibak.Models.MedicalFileObject;
import com.scit.tabibak.R;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;

public class FileDetailsDialog extends AlertDialog {
    private Context context;
    private TextView title,desc;
    private ImageView image;
    private ProgressBar loading_image;
    private MedicalFileObject file;

    public FileDetailsDialog(@NonNull Context context,MedicalFileObject file) {
        super(context);
        this.context = context;
        this.file = file;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_file_details);

        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;

        init_views();
        init_events();
        init_dialog();
    }

    private void init_views() {
        title = findViewById(R.id.file_title);
        desc = findViewById(R.id.desc);
        image = findViewById(R.id.image);
        loading_image = findViewById(R.id.loading_image);
    }

    private void init_events() {
    }

    private void init_dialog() {
        title.setText(file.getTitle());
        desc.setText(file.getDetails());
        loading_image.setVisibility(View.VISIBLE);
        BaseFunctions.setGlideImageWithEvents(context, image, file.getImage(), new ILoadImage() {
            @Override
            public void onLoaded() {
                loading_image.setVisibility(View.GONE);
            }

            @Override
            public void onFailed() {
                loading_image.setVisibility(View.GONE);
            }
        });
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewImageDialog dialog = new ViewImageDialog(context,file.getImage());
                dialog.show();
            }
        });
    }
}
