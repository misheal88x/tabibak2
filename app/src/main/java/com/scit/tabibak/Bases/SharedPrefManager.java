package com.scit.tabibak.Bases;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.scit.tabibak.Models.Startup_models.CityObject;
import com.scit.tabibak.Models.Startup_models.FacilityObject;
import com.scit.tabibak.Models.Startup_models.SliderObject;
import com.scit.tabibak.Models.Startup_models.SpecializationObject;
import com.scit.tabibak.Models.UserObject;

import java.util.ArrayList;
import java.util.List;

public class SharedPrefManager {
    private static SharedPrefManager mInstance;
    private static Context mCtx;

    private static final String SHARED_PREF_NAME = "Main";
    private static final String VIEW_LANGUAGE = "view_language";
    private static final String IS_USER_LOGGED = "is_user_logged";
    private static final String IS_NOTIY_CLICKED = "is_notiy_clicked";
    private static final String USER = "user";
    private static final String CITIES = "cities";
    private static final String FACILITIES = "facilities";
    private static final String SLIDES = "slides";
    private static final String SPECIALIZATIONS = "specializations";
    private static final String CALL = "call_number";
    private static final String CONTACT = "contact";


    private SharedPrefManager(Context context) {
        mCtx = context;
    }
    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }

    public void setViewLanguage(String lan){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(VIEW_LANGUAGE,lan);
        editor.commit();
    }

    public String getViewLanguage() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString(VIEW_LANGUAGE, "ar");
        return value;
    }

    public void setCall(String lan){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(CALL,lan);
        editor.commit();
    }

    public String getCall() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString(CALL, "");
        return value;
    }

    public void setContact(String value){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(CONTACT,value);
        editor.commit();
    }

    public String getContact() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString(CONTACT, "");
        return value;
    }

    public void setLogged(boolean value){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(IS_USER_LOGGED,value);
        editor.commit();
    }

    public boolean getLogged() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        boolean value = sharedPreferences.getBoolean(IS_USER_LOGGED, false);
        return value;
    }

    public void setNotiyClicked(boolean value){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(IS_NOTIY_CLICKED,value);
        editor.commit();
    }

    public boolean getNotiyClicked() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        boolean value = sharedPreferences.getBoolean(IS_NOTIY_CLICKED, false);
        return value;
    }

    public void setCities(List<CityObject> c){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(CITIES,new Gson().toJson(c));
        editor.commit();
    }

    public List<CityObject> getCities() {
        List<CityObject> lis = new ArrayList<>();
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(CITIES,"[]");
        CityObject[] arr = new Gson().fromJson(json,CityObject[].class);
        if (arr.length>0){
            for (CityObject c : arr){
                lis.add(c);
            }
        }
        return lis;
    }

    public void setFacilities(List<FacilityObject> c){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(FACILITIES,new Gson().toJson(c));
        editor.commit();
    }

    public List<FacilityObject> getFacilities() {
        List<FacilityObject> lis = new ArrayList<>();
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(FACILITIES,"[]");
        FacilityObject[] arr = new Gson().fromJson(json,FacilityObject[].class);
        if (arr.length>0){
            for (FacilityObject c : arr){
                lis.add(c);
            }
        }
        return lis;
    }

    public void setSlides(List<SliderObject> c){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SLIDES,new Gson().toJson(c));
        editor.commit();
    }

    public List<SliderObject> getSlides() {
        List<SliderObject> lis = new ArrayList<>();
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(SLIDES,"[]");
        SliderObject[] arr = new Gson().fromJson(json,SliderObject[].class);
        if (arr.length>0){
            for (SliderObject c : arr){
                lis.add(c);
            }
        }
        return lis;
    }

    public void setSpes(List<SpecializationObject> c){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SPECIALIZATIONS,new Gson().toJson(c));
        editor.commit();
    }

    public List<SpecializationObject> getSpes() {
        List<SpecializationObject> lis = new ArrayList<>();
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(SPECIALIZATIONS,"[]");
        SpecializationObject[] arr = new Gson().fromJson(json,SpecializationObject[].class);
        if (arr.length>0){
            for (SpecializationObject c : arr){
                lis.add(c);
            }
        }
        return lis;
    }

    public void setUser(UserObject user){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String json = new Gson().toJson(user);
        editor.putString(USER,json);
        editor.commit();
    }

    public UserObject getUser(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        UserObject u = new Gson().fromJson(sharedPreferences.getString(USER,"{}"),UserObject.class);
        return u;
    }
}
