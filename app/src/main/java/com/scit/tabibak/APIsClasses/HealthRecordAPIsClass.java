package com.scit.tabibak.APIsClasses;

import android.content.Context;

import com.scit.tabibak.APIs.HealthRecordAPIs;
import com.scit.tabibak.APIs.MedicalFilesAPIs;
import com.scit.tabibak.APIs.StartupAPIs;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.BaseRetrofit;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Models.BaseResponse;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Part;

public class HealthRecordAPIsClass extends BaseRetrofit {

    public static void save_record(
            final Context context,
            String name,
            String age,
            String tall,
            String weight,
            String disease_id,
            String precedents,
            String genetic,
            String medicines,
            List<String> images,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        RequestBody nameRequest = BaseFunctions.uploadFileStringConverter(name);
        RequestBody ageRequest = BaseFunctions.uploadFileStringConverter(age);
        RequestBody tallRequest = BaseFunctions.uploadFileStringConverter(tall);
        RequestBody weightRequest = BaseFunctions.uploadFileStringConverter(weight);
        RequestBody disease_idRequest = BaseFunctions.uploadFileStringConverter(disease_id);
        RequestBody precedentsRequest = BaseFunctions.uploadFileStringConverter(precedents);
        RequestBody geneticRequest = BaseFunctions.uploadFileStringConverter(genetic);
        RequestBody medicinesRequest = BaseFunctions.uploadFileStringConverter(medicines);
        MultipartBody.Part[] multipartTypedOutput = new MultipartBody.Part[images.size()];
        if (images.size()>0){
            for (int i = 0; i <images.size() ; i++) {
                File file = new File(images.get(i));
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),file);
                multipartTypedOutput[i] = MultipartBody.Part.createFormData("images[]", file.getName(), requestBody);
            }
        }
        Retrofit retrofit = configureRetrofitWithBearer(context);
        HealthRecordAPIs api = retrofit.create(HealthRecordAPIs.class);
        Call<BaseResponse> call = api.save_report("application/json",
                nameRequest,ageRequest,tallRequest,weightRequest,disease_idRequest,
                precedentsRequest,geneticRequest,medicinesRequest,multipartTypedOutput);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void check_record(
            final Context context,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        HealthRecordAPIs api = retrofit.create(HealthRecordAPIs.class);
        Call<BaseResponse> call = api.check_record("application/json");
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void get_my_record(
            final Context context,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        HealthRecordAPIs api = retrofit.create(HealthRecordAPIs.class);
        Call<BaseResponse> call = api.get_my_record("application/json");
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void get_other_record(
            final Context context,
            int id,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        HealthRecordAPIs api = retrofit.create(HealthRecordAPIs.class);
        Call<BaseResponse> call = api.get_other_record("application/json",String.valueOf(id));
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
    public static void remove_image(
            final Context context,
            int id,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        HealthRecordAPIs api = retrofit.create(HealthRecordAPIs.class);
        Call<BaseResponse> call = api.remove_image("application/json",String.valueOf(id));
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
}
