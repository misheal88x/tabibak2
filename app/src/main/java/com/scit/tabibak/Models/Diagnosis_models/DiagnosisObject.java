package com.scit.tabibak.Models.Diagnosis_models;

import com.google.firebase.auth.UserInfo;
import com.google.gson.annotations.SerializedName;
import com.scit.tabibak.Models.UserObject;

public class DiagnosisObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("medical_status") private String medical_status = "";
    @SerializedName("medical_diagnosis") private String medical_diagnosis = "";
    @SerializedName("prescription") private String prescription = "";
    @SerializedName("user_id") private int user_id = 0;
    @SerializedName("doctor_id") private int doctor_id = 0;
    @SerializedName("created_at") private String created_at = "";
    @SerializedName("updated_at") private String updated_at = "";
    @SerializedName("doctor") private UserObject doctor = new UserObject();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMedical_status() {
        return medical_status;
    }

    public void setMedical_status(String medical_status) {
        this.medical_status = medical_status;
    }

    public String getMedical_diagnosis() {
        return medical_diagnosis;
    }

    public void setMedical_diagnosis(String medical_diagnosis) {
        this.medical_diagnosis = medical_diagnosis;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(int doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public UserObject getDoctor() {
        return doctor;
    }

    public void setDoctor(UserObject doctor) {
        this.doctor = doctor;
    }

    public String getPrescription() {
        return prescription;
    }

    public void setPrescription(String prescription) {
        this.prescription = prescription;
    }
}
