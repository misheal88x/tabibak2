package com.scit.tabibak.APIsClasses;

import android.content.Context;

import com.scit.tabibak.APIs.MedicalFilesAPIs;
import com.scit.tabibak.APIs.MedicinesAPIs;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.BaseRetrofit;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Models.BaseResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Multipart;

public class MedicalFilesAPIsClass extends BaseRetrofit {

    public static void get_all(
            final Context context,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicalFilesAPIs api = retrofit.create(MedicalFilesAPIs.class);
        Call<BaseResponse> call = api.get_all("application/json");
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void add_file(
            final Context context,
            String title,
            String details,
            String image,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        RequestBody titleRequest = BaseFunctions.uploadFileStringConverter(title);
        RequestBody detailsRequest = BaseFunctions.uploadFileStringConverter(details);
        MultipartBody.Part body = BaseFunctions.uploadFileImageConverter("image",image);

        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicalFilesAPIs api = retrofit.create(MedicalFilesAPIs.class);
        Call<BaseResponse> call = api.add_file("application/json",
                titleRequest,detailsRequest,body);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void delete_file(
            final Context context,
            int file_id,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicalFilesAPIs api = retrofit.create(MedicalFilesAPIs.class);
        Call<BaseResponse> call = api.delete_file("application/json",String.valueOf(file_id));
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

}
