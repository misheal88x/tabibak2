package com.scit.tabibak.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.scit.tabibak.APIsClasses.MedicalCosulationAPIsClass;
import com.scit.tabibak.APIsClasses.MedicalInfosAPIsClass;
import com.scit.tabibak.APIsClasses.MedicinesAPIsClass;
import com.scit.tabibak.APIsClasses.NotificationsAPIsClass;
import com.scit.tabibak.Adapters.QuestionsAdapter;
import com.scit.tabibak.Bases.BaseActivity;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Dialogs.RepliesDialog;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Models.Important_notes_models.ImportantNoteObject;
import com.scit.tabibak.Models.Medical_consulation_models.ReplyObject;
import com.scit.tabibak.Models.Medical_dates_models.MedicalDateObject;
import com.scit.tabibak.Models.Medical_info_models.MedicalInfoObject;
import com.scit.tabibak.Models.Medical_notes_models.MedicalNoteObject;
import com.scit.tabibak.Models.Medicine_models.MedicineObject;
import com.scit.tabibak.R;

import java.util.ArrayList;
import java.util.List;

public class NotificationDetailsActivity extends BaseActivity {

    private String target_title = "",target_type = "";

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_notification_details);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        target_type = getIntent().getStringExtra("target_type");
        String target_id = getIntent().getStringExtra("target_id");
        target_title = getIntent().getStringExtra("target_title");

        switch (target_type){
            //A doctor replies to a question of mine
            case "1":{
                callRepliesAPI(Integer.valueOf(target_id));
            }break;
            //Medical date
            case "2":{
                callGetDateAPI(Integer.valueOf(target_id));
            }break;
            //Medical record
            case "4":{
                callGetMedicalNoteAPI(Integer.valueOf(target_id));
            }break;
            //Very important
            case "3":{
                callGetImportantNoteAPI(Integer.valueOf(target_id));
            }break;
            //Medical Info
            case "5" : {
                callGetInfoAPI(Integer.valueOf(target_id));
            }break;
            //New doctor
            case "6" : {
                Intent intent = new Intent(NotificationDetailsActivity.this,MainActivity.class);
                intent.putExtra("input","");
                intent.putExtra("title","");
                intent.putExtra("target_type",target_type);
                startActivity(intent);
                finish();
            }break;
            //New medicine
            //case "7":{
            //    callGetMedicineAPI(Integer.valueOf(target_id));
            //}break;
            //General Notification
            case "8":{
                Intent intent = new Intent(NotificationDetailsActivity.this,MainActivity.class);
                intent.putExtra("input","");
                intent.putExtra("title","");
                intent.putExtra("target_type",target_type);
                startActivity(intent);
                finish();
            }break;
            //Doctor adds date to patient
            case "9":{
                callGetDateAPI(Integer.valueOf(target_id));
            }break;
            //Doctor delete date
            case "10":{
                Intent intent = new Intent(NotificationDetailsActivity.this,MainActivity.class);
                intent.putExtra("input","");
                intent.putExtra("title","");
                intent.putExtra("target_type",target_type);
                startActivity(intent);
                finish();
            }break;
            //Doctor edit date
            case "11":{
                callGetDateAPI(Integer.valueOf(target_id));
            }break;

        }
    }

    @Override
    public void init_views() {

    }

    @Override
    public void init_events() {

    }

    @Override
    public void set_fragment_place() {

    }

    private void callGetDateAPI(int id){
        NotificationsAPIsClass.get_date(
                NotificationDetailsActivity.this,
                id,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        finish();
                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        MedicalDateObject success = new Gson().fromJson(j,MedicalDateObject.class);
                        SharedPrefManager.getInstance(NotificationDetailsActivity.this).setNotiyClicked(true);
                        Intent intent = new Intent(NotificationDetailsActivity.this,MainActivity.class);
                        intent.putExtra("input",new Gson().toJson(success));
                        intent.putExtra("title","");
                        intent.putExtra("target_type",target_type);
                        startActivity(intent);
                        finish();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        finish();
                    }
                });
    }

    private void callGetInfoAPI(int id){
        MedicalInfosAPIsClass.get_info_details(
                NotificationDetailsActivity.this,
                id,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        finish();
                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        MedicalInfoObject success = new Gson().fromJson(j,MedicalInfoObject.class);
                        SharedPrefManager.getInstance(NotificationDetailsActivity.this).setNotiyClicked(true);
                        Intent intent = new Intent(NotificationDetailsActivity.this,MainActivity.class);
                        intent.putExtra("input",new Gson().toJson(success));
                        intent.putExtra("title","");
                        intent.putExtra("target_type",target_type);
                        startActivity(intent);
                        finish();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        finish();
                    }
                });
    }

    private void callGetImportantNoteAPI(int id){
        NotificationsAPIsClass.get_important_note(
                NotificationDetailsActivity.this,
                id,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        finish();
                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        ImportantNoteObject success = new Gson().fromJson(j,ImportantNoteObject.class);
                        SharedPrefManager.getInstance(NotificationDetailsActivity.this).setNotiyClicked(true);
                        Intent intent = new Intent(NotificationDetailsActivity.this,MainActivity.class);
                        intent.putExtra("input",new Gson().toJson(success));
                        intent.putExtra("title","");
                        intent.putExtra("target_type",target_type);
                        startActivity(intent);
                        finish();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        finish();
                    }
                });
    }

    private void callGetMedicalNoteAPI(int id){
        NotificationsAPIsClass.get_medical_note(
                NotificationDetailsActivity.this,
                id,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        finish();
                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        MedicalNoteObject success = new Gson().fromJson(j,MedicalNoteObject.class);
                        SharedPrefManager.getInstance(NotificationDetailsActivity.this).setNotiyClicked(true);
                        Intent intent = new Intent(NotificationDetailsActivity.this,MainActivity.class);
                        intent.putExtra("input",new Gson().toJson(success));
                        intent.putExtra("title","");
                        intent.putExtra("target_type",target_type);
                        startActivity(intent);
                        finish();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        finish();
                    }
                });
    }

    private void callGetMedicineAPI(int id){
        MedicinesAPIsClass.get_med(
                NotificationDetailsActivity.this,
                id,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        finish();
                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        MedicineObject success = new Gson().fromJson(j,MedicineObject.class);
                        SharedPrefManager.getInstance(NotificationDetailsActivity.this).setNotiyClicked(true);
                        Intent intent = new Intent(NotificationDetailsActivity.this,MainActivity.class);
                        intent.putExtra("input",new Gson().toJson(success));
                        intent.putExtra("title","");
                        intent.putExtra("target_type",target_type);
                        startActivity(intent);
                        finish();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        finish();
                    }
                });
    }

    private void callRepliesAPI(int id){
        MedicalCosulationAPIsClass.get_replies(NotificationDetailsActivity.this,
                id,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        finish();
                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        ReplyObject[] success = new Gson().fromJson(j,ReplyObject[].class);
                        if (success.length>0){
                            List<ReplyObject> lis = new ArrayList<>();
                            for (ReplyObject r : success){
                                lis.add(r);
                            }
                            SharedPrefManager.getInstance(NotificationDetailsActivity.this).setNotiyClicked(true);
                            Intent intent = new Intent(NotificationDetailsActivity.this,MainActivity.class);
                            intent.putExtra("input",new Gson().toJson(lis));
                            intent.putExtra("title",target_title);
                            intent.putExtra("target_type",target_type);
                            startActivity(intent);
                            finish();
                        }else {
                            finish();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        finish();
                    }
                });
    }


}
