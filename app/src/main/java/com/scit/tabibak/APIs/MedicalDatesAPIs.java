package com.scit.tabibak.APIs;

import com.scit.tabibak.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface MedicalDatesAPIs {
    @GET("appointments")
    Call<BaseResponse> get_all(
            @Header("Accept") String accept
    );

    @FormUrlEncoded
    @POST("add-appointment")
    Call<BaseResponse> add_date(
            @Header("Accept") String accept,
            @Field("title") String title,
            @Field("details") String details,
            @Field("date") String date,
            @Field("time") String time,
            @Field("user_id") int user_id,
            @Field("doctor_id") int doctor_id
    );

    @GET("delete-appointment/{id}")
    Call<BaseResponse> delete_date(
            @Header("accept") String accept,
            @Path("id") String id
    );

    @FormUrlEncoded
    @POST("request-appointment")
    Call<BaseResponse> request_date(@Header("accept") String accept,
                                    @Field("doctor_id") int doctor_id,
                                    @Field("complaint") String complaint,
                                    @Field("note") String note);

    @GET("delete-appointment-requests/{id}")
    Call<BaseResponse> delete_date_request(@Header("accept") String accept,
                                           @Path("id") String id);

    @GET("doctor-requests/{id}")
    Call<BaseResponse> get_doctor_requests(
            @Header("accept") String accept,
            @Path("id") String id
    );
    @GET("doctor-appointments")
    Call<BaseResponse> get_doctor_dates(
            @Header("accept") String accept
    );

    @FormUrlEncoded
    @POST("update-appointment")
    Call<BaseResponse> edit_date(
            @Header("Accept") String accept,
            @Field("id") int id,
            @Field("date") String date,
            @Field("time") String time

    );

}
