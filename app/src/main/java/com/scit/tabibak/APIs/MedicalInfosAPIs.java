package com.scit.tabibak.APIs;

import com.scit.tabibak.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MedicalInfosAPIs {
    @GET("medical-infos")
    Call<BaseResponse> get_all(
            @Header("Accept") String accept,
            @Query("page") int page
    );

    @GET("medical-info/{id}")
    Call<BaseResponse> get_info_details(
            @Header("Accept") String accept,
            @Path("id") String id
    );
}
