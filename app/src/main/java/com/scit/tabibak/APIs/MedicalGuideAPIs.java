package com.scit.tabibak.APIs;

import com.scit.tabibak.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface MedicalGuideAPIs {
    @FormUrlEncoded
    @POST("search")
    Call<BaseResponse> search(
            @Header("Accept") String accept,
            @Field("city_id") int city_id,
            @Field("type") int type,
            @Field("specialization_id") int specialization_id,
            @Field("name") String name
    );
}
