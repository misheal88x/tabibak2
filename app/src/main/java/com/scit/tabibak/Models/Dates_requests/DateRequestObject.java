package com.scit.tabibak.Models.Dates_requests;

import com.google.gson.annotations.SerializedName;
import com.scit.tabibak.Models.UserObject;

public class DateRequestObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("user_id") private int user_id = 0;
    @SerializedName("doctor_id") private int doctor_id = 0;
    @SerializedName("complaint") private String complaint = "";
    @SerializedName("note") private String note = "";
    @SerializedName("created_at") private String created_at = "";
    @SerializedName("updated_at") private String updated_at = "";
    @SerializedName("patient") private UserObject patient = new UserObject();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(int doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getComplaint() {
        return complaint;
    }

    public void setComplaint(String complaint) {
        this.complaint = complaint;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public UserObject getPatient() {
        return patient;
    }

    public void setPatient(UserObject patient) {
        this.patient = patient;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
