package com.scit.tabibak.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scit.tabibak.APIsClasses.MedicalDatesAPIsClass;
import com.scit.tabibak.Activities.SplashActivity;
import com.scit.tabibak.Adapters.DatesAdapter;
import com.scit.tabibak.Adapters.DatesRequestsAdapter;
import com.scit.tabibak.Bases.BaseFragment;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Dialogs.ContactUsDialog;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Models.Dates_requests.DateRequestObject;
import com.scit.tabibak.Models.Medical_dates_models.MedicalDateObject;
import com.scit.tabibak.Models.UserObject;
import com.scit.tabibak.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class DatesRequestsFragment extends BaseFragment {

    private RelativeLayout toolbar;
    private TextView title;
    private RelativeLayout call,help,logout;

    private RecyclerView requests_recycler;
    private List<DateRequestObject> list;
    private DatesRequestsAdapter requests_adapter;
    private LinearLayoutManager requests_layout_manager;

    private ProgressBar loading;
    private LinearLayout no_data,no_internet,error;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dates_requests,container,false);
    }

    @Override
    public void init_views() {
        toolbar = base.findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        call = toolbar.findViewById(R.id.contact_us);
        help = toolbar.findViewById(R.id.help);
        logout = toolbar.findViewById(R.id.logout);
        if (!SharedPrefManager.getInstance(base).getLogged()){
            logout.setVisibility(View.GONE);
        }

        loading = base.findViewById(R.id.loading);
        no_data = base.findViewById(R.id.no_data_layout);
        no_internet = base.findViewById(R.id.no_internet_layout);
        error = base.findViewById(R.id.error_layout);

        requests_recycler = base.findViewById(R.id.requests_recycler);
    }

    @Override
    public void init_events() {
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactUsDialog dialog = new ContactUsDialog(base);
                dialog.show();
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base.open_fragment(new HelpFragment());
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(base);
                builder.setTitle(getResources().getString(R.string.logout_title));
                builder.setMessage(getResources().getString(R.string.logout_message));
                builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPrefManager.getInstance(base).setLogged(false);
                        SharedPrefManager.getInstance(base).setUser(new UserObject());
                        startActivity(new Intent(base, SplashActivity.class));
                        base.finish();
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
        error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callGetRequestsAPI();
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        title.setText(getResources().getString(R.string.health_record_dates_requests));
        init_recycler();
    }

    private void init_recycler(){
        list = new ArrayList<>();
        requests_adapter = new DatesRequestsAdapter(base,list);
        requests_layout_manager = new LinearLayoutManager(base);
        requests_recycler.setLayoutManager(requests_layout_manager);
        requests_recycler.setAdapter(requests_adapter);
        callGetRequestsAPI();
    }

    private void callGetRequestsAPI(){
        loading.setVisibility(View.VISIBLE);
        list.clear();
        requests_adapter = new DatesRequestsAdapter(base,list);
        requests_recycler.setAdapter(requests_adapter);
        no_data.setVisibility(View.GONE);
        error.setVisibility(View.GONE);
        no_internet.setVisibility(View.GONE);
        MedicalDatesAPIsClass.get_doctor_requests(base,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        loading.setVisibility(View.GONE);
                        error.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        loading.setVisibility(View.GONE);
                        String j = new Gson().toJson(json);
                        try {
                            DateRequestObject[] success = new Gson().fromJson(j,DateRequestObject[].class);
                            if (success.length>0){
                                for (DateRequestObject o : success){
                                    list.add(o);
                                    requests_adapter.notifyDataSetChanged();
                                }
                            }else {
                                no_data.setVisibility(View.VISIBLE);
                            }
                        }catch (Exception e){
                            callGetRequestsAPI();
                        }

                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        loading.setVisibility(View.GONE);
                        no_internet.setVisibility(View.VISIBLE);
                    }
                });
    }
}
