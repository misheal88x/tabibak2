package com.scit.tabibak.APIs;

import com.scit.tabibak.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface StartupAPIs {
    @GET("start")
    Call<BaseResponse> getStart(
            @Header("Accept") String accept
    );

    @GET("help")
    Call<BaseResponse> getHelp(
            @Header("Accept") String accept
    );

    @GET("about")
    Call<BaseResponse> getAbout(
            @Header("Accept") String accept
    );
}
