package com.scit.tabibak.APIsClasses;

import android.content.Context;
import android.util.Log;

import com.scit.tabibak.APIs.ImportantNotesAPIs;
import com.scit.tabibak.APIs.MedicalDatesAPIs;
import com.scit.tabibak.APIs.MedicalFilesAPIs;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.BaseRetrofit;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Models.BaseResponse;
import com.scit.tabibak.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ImportantNotesAPIsClass extends BaseRetrofit {

    public static void get_all(
            final Context context,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        ImportantNotesAPIs api = retrofit.create(ImportantNotesAPIs.class);
        Call<BaseResponse> call = api.get_all("application/json");
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void add_note(
            final Context context,
            String title,
            String details,
            String date,
            String time,
            String image,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        RequestBody titleRequest = BaseFunctions.uploadFileStringConverter(title);
        RequestBody detailsRequest = BaseFunctions.uploadFileStringConverter(details);
        RequestBody timeRequest = BaseFunctions.uploadFileStringConverter(time);
        RequestBody dateRequest = BaseFunctions.uploadFileStringConverter(date);
        MultipartBody.Part body = null;
        if (!image.equals("")) {
            body = BaseFunctions.uploadFileImageConverter("image", image);
        }

        Retrofit retrofit = configureRetrofitWithBearer(context);
        ImportantNotesAPIs api = retrofit.create(ImportantNotesAPIs.class);
        Call<BaseResponse> call = api.add_note("application/json",titleRequest,
                detailsRequest,dateRequest,timeRequest,body);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                //BaseFunctions.processResponse(context,response,onResponse);
                if (response.isSuccessful()){
                    if (response.code() == 200){
                        if (response.body()!=null){
                            if (response.body().getStatus() == 0){
                                onResponse.onResponse();
                                BaseFunctions.showErrorToast(context,response.body().getMessage());
                            } else if (response.body().getStatus()==1){
                                onResponse.onResponse(response.body().getData());
                            }else {
                                onResponse.onResponse();
                                BaseFunctions.showErrorToast(context,response.body().getMessage());
                            }
                        }else {
                            onResponse.onResponse();
                        }
                    }else {
                        onResponse.onResponse();
                    }
                }else {

                    BufferedReader reader = null;
                    StringBuilder sb = new StringBuilder();
                    reader = new BufferedReader(new InputStreamReader(response.errorBody().byteStream()));
                    String line;
                    try {
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    String finallyError = sb.toString();
                    Log.i("jdfdjbfjdbf", "onResponse: "+finallyError);
                    //try {
                    //    BaseFunctions.showErrorToast(context, response.body().getMessage());
                    //}catch (Exception e){
                    //    BaseFunctions.showErrorToast(context, context.getResources().getString(R.string.error_occured_2));
                    //}
                    onResponse.onResponse();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void delete_note(
            final Context context,
            int note_id,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        ImportantNotesAPIs api = retrofit.create(ImportantNotesAPIs.class);
        Call<BaseResponse> call = api.delete_note("application/json",String.valueOf(note_id));
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

}
