package com.scit.tabibak.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scit.tabibak.Activities.SplashActivity;
import com.scit.tabibak.Adapters.SearchResultsAdapter;
import com.scit.tabibak.Bases.BaseFragment;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Dialogs.ContactUsDialog;
import com.scit.tabibak.Interfaces.OnItemClickListener;
import com.scit.tabibak.Models.Medical_guide_models.SearchResultObject;
import com.scit.tabibak.Models.UserObject;
import com.scit.tabibak.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SearchResultsFragment extends BaseFragment implements OnItemClickListener {


    private RelativeLayout toolbar;
    private TextView title;
    private RelativeLayout call,help,logout;

    private RecyclerView recycler;
    private List<SearchResultObject> list;
    private SearchResultsAdapter adapter;
    private LinearLayoutManager layoutManager;

    private LinearLayout no_internet_layout,no_data_layot,error_layout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search_results,container,false);
    }

    @Override
    public void init_views() {
        toolbar = base.findViewById(R.id.toolbar);
        title = base.findViewById(R.id.title);
        call = base.findViewById(R.id.contact_us);
        help = base.findViewById(R.id.help);
        logout = base.findViewById(R.id.logout);
        if (!SharedPrefManager.getInstance(base).getLogged()){
            logout.setVisibility(View.GONE);
        }

        recycler = base.findViewById(R.id.result_recycler);

        no_internet_layout = base.findViewById(R.id.no_internet_layout);
        no_data_layot = base.findViewById(R.id.no_data_layout);
        error_layout = base.findViewById(R.id.error_layout);

    }

    @Override
    public void init_events() {
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(base);
                builder.setTitle(getResources().getString(R.string.logout_title));
                builder.setMessage(getResources().getString(R.string.logout_message));
                builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPrefManager.getInstance(base).setLogged(false);
                        SharedPrefManager.getInstance(base).setUser(new UserObject());
                        startActivity(new Intent(base, SplashActivity.class));
                        base.finish();
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactUsDialog dialog = new ContactUsDialog(base);
                dialog.show();
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base.open_fragment(new HelpFragment());
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        title.setText(getResources().getString(R.string.result_title));
        init_recycler();
    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new SearchResultsAdapter(base,list,this);
        layoutManager = new LinearLayoutManager(base);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);
        SearchResultObject[] success = new Gson().fromJson(getArguments().getString("results"),SearchResultObject[].class);
        for (SearchResultObject o : success){
            list.add(o);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onItemClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("data",list.get(position));
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(bundle);
        base.open_fragment(fragment);
    }
}
