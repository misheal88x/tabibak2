package com.scit.tabibak.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scit.tabibak.APIsClasses.MedicalGuideAPIsClass;
import com.scit.tabibak.Activities.SplashActivity;
import com.scit.tabibak.Adapters.CustomSpinnerAdapter;
import com.scit.tabibak.Bases.BaseFragment;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Dialogs.ContactUsDialog;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Models.Medical_guide_models.SearchResultObject;
import com.scit.tabibak.Models.Startup_models.CityObject;
import com.scit.tabibak.Models.Startup_models.SpecializationObject;
import com.scit.tabibak.Models.UserObject;
import com.scit.tabibak.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

public class MedicalGuideFragment extends BaseFragment {

    private RelativeLayout toolbar;
    private TextView title;
    private RelativeLayout call,help,logout;

    private Spinner city_spinner;
    private List<String> city_list_string;
    private List<CityObject> city_list;
    private CustomSpinnerAdapter city_adapter;

    private CheckBox by_name,by_profession;
    private EditText name;

    private int selected_search_type = 0;

    private Spinner profession_spinner;
    private List<String> profession_list_string;
    private List<SpecializationObject> profession_list;
    private CustomSpinnerAdapter profession_adapter;

    private int selected_city_id = 0,selected_profession_id = 0;

    private RelativeLayout profession_layout;

    private TextView search;

    private Animation fadeIn,fadeOut;

    private ProgressBar loading;

    private List<SearchResultObject> results;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_medical_guide,container,false);
    }

    @Override
    public void init_views() {
        toolbar = base.findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        call = toolbar.findViewById(R.id.contact_us);
        help = toolbar.findViewById(R.id.help);
        logout = toolbar.findViewById(R.id.logout);
        if (!SharedPrefManager.getInstance(base).getLogged()){
            logout.setVisibility(View.GONE);
        }

        city_spinner = base.findViewById(R.id.city_spinner);
        profession_spinner = base.findViewById(R.id.profession_spinner);

        profession_layout = base.findViewById(R.id.profession_layout);

        by_name = base.findViewById(R.id.by_name);
        by_profession = base.findViewById(R.id.by_profession);

        name = base.findViewById(R.id.name);

        search = base.findViewById(R.id.search);

        fadeIn = AnimationUtils.loadAnimation(base,R.anim.fade_in);
        fadeOut = AnimationUtils.loadAnimation(base,R.anim.fade_out);

        loading = base.findViewById(R.id.loading);

        results = new ArrayList<>();
    }

    @Override
    public void init_events() {
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(base);
                builder.setTitle(getResources().getString(R.string.logout_title));
                builder.setMessage(getResources().getString(R.string.logout_message));
                builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPrefManager.getInstance(base).setLogged(false);
                        SharedPrefManager.getInstance(base).setUser(new UserObject());
                        startActivity(new Intent(base, SplashActivity.class));
                        base.finish();
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactUsDialog dialog = new ContactUsDialog(base);
                dialog.show();
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base.open_fragment(new HelpFragment());
            }
        });

        city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,i);
                if (position == 0){
                    selected_city_id = 0;
                }else {
                    selected_city_id = city_list.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        profession_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,i);
                if (position == 0){
                    selected_profession_id = 0;
                }else {
                    selected_profession_id = profession_list.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        by_name.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    selected_search_type = 1;
                    profession_layout.setVisibility(View.GONE);
                    by_profession.setChecked(false);
                    profession_spinner.setSelection(0);
                    name.setVisibility(View.VISIBLE);
                    name.startAnimation(fadeIn);
                    selected_search_type = 1;
                }else {
                    selected_search_type = 0;
                    name.setVisibility(View.GONE);
                    name.setText("");
                }
            }
        });
        by_profession.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    name.setVisibility(View.GONE);
                    name.setText("");
                    by_name.setChecked(false);
                    profession_layout.setVisibility(View.VISIBLE);
                    profession_layout.startAnimation(fadeIn);
                    selected_search_type = 2;
                }else {
                    selected_search_type = 0;
                    profession_spinner.setSelection(0);
                    profession_layout.setVisibility(View.GONE);
                }
            }
        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_city_id == 0){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.guide_no_city));
                    return;
                }
                if (selected_search_type == 0){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.guide_no_type));
                    return;
                }
                if (selected_search_type == 1){
                    if (name.getText().toString().equals("")){
                        BaseFunctions.showWarningToast(base,getResources().getString(R.string.guide_no_name));
                        return;
                    }
                }else if (selected_search_type == 2){
                    if (selected_profession_id == 0){
                        BaseFunctions.showWarningToast(base,getResources().getString(R.string.guide_no_profession));
                        return;
                    }
                }
                callSearchAPI();
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        title.setText(getResources().getString(R.string.home_medical_guide));
        init_spinners();
    }

    private void init_spinners(){
        city_list_string = new ArrayList<>();
        profession_list_string = new ArrayList<>();
        city_list = new ArrayList<>();
        profession_list = new ArrayList<>();

        city_list_string.add(getResources().getString(R.string.guide_city));
        city_list.add(new CityObject());
        if (SharedPrefManager.getInstance(base).getCities().size()>0){
            for (CityObject c : SharedPrefManager.getInstance(base).getCities()){
                city_list.add(c);
                city_list_string.add(c.getName());
            }
        }

        profession_list_string.add(getResources().getString(R.string.guide_pick_profession));
        profession_list.add(new SpecializationObject());
        if (SharedPrefManager.getInstance(base).getSpes().size()>0){
            for (SpecializationObject s : SharedPrefManager.getInstance(base).getSpes()){
                profession_list.add(s);
                profession_list_string.add(s.getName());
            }
        }

        BaseFunctions.init_spinner(base,city_spinner,city_adapter,city_list_string);
        BaseFunctions.init_spinner(base,profession_spinner,profession_adapter,profession_list_string);
    }

    private void callSearchAPI(){
        int city = selected_city_id;
        int search_type = selected_search_type;
        String name_txt = "";
        int pro_id = 0;
        if (search_type == 1){
            name_txt = name.getText().toString();
        }else {
            pro_id = selected_profession_id;
        }
        loading.setVisibility(View.VISIBLE);
        search.setVisibility(View.GONE);
        MedicalGuideAPIsClass.search(
                base,
                city,
                search_type,
                pro_id,
                name_txt,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        loading.setVisibility(View.GONE);
                        search.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        loading.setVisibility(View.GONE);
                        search.setVisibility(View.VISIBLE);
                        String j = new Gson().toJson(json);
                        try {
                            SearchResultObject[] success = new Gson().fromJson(j, SearchResultObject[].class);
                            if (success.length > 0) {
                                for (SearchResultObject s : success) {
                                    results.add(s);
                                }
                                Bundle bundle = new Bundle();
                                bundle.putString("results", new Gson().toJson(results));
                                SearchResultsFragment fragment = new SearchResultsFragment();
                                fragment.setArguments(bundle);
                                base.open_fragment(fragment);
                            } else {
                                BaseFunctions.showErrorToast(base, getResources().getString(R.string.no_results));
                            }
                        }catch (Exception e){
                            callSearchAPI();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        loading.setVisibility(View.GONE);
                        search.setVisibility(View.VISIBLE);
                        BaseFunctions.showErrorToast(base,getResources().getString(R.string.no_internet));
                    }
                });
    }
}
