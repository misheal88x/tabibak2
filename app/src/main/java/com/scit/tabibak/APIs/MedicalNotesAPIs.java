package com.scit.tabibak.APIs;

import com.scit.tabibak.Models.BaseResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface MedicalNotesAPIs {
    @GET("get-medical-notes")
    Call<BaseResponse> get_all(
            @Header("Accept") String accept);

    @Multipart
    @POST("add-medical-note")
    Call<BaseResponse> add_note(
            @Header("Accept") String accept,
            @Part("title") RequestBody title,
            @Part("details") RequestBody details,
            @Part("alarm_date") RequestBody alarm_date,
            @Part("alarm_time") RequestBody alarm_time,
            @Part MultipartBody.Part image);

}
