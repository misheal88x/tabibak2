package com.scit.tabibak.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Interfaces.ILoadImage;
import com.scit.tabibak.Interfaces.OnItemClickListener;
import com.scit.tabibak.Models.MedicalFileObject;
import com.scit.tabibak.Models.MedicalImageObject;
import com.scit.tabibak.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MedicalFilesAdapter extends RecyclerView.Adapter<MedicalFilesAdapter.ViewHolder> {
    private Context context;
    private List<MedicalFileObject> list;

    private OnItemClickListener onItemClickListener;

    public MedicalFilesAdapter(Context context,List<MedicalFileObject> list,OnItemClickListener onItemClickListener) {
        this.context = context;
        this.list = list;
        this.onItemClickListener = onItemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView image;
        private TextView title,text;
        private ProgressBar loading;
        private RelativeLayout layout;

        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            title = view.findViewById(R.id.title);
            text = view.findViewById(R.id.text);
            loading = view.findViewById(R.id.loading);
            layout = view.findViewById(R.id.layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_medicine, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        MedicalFileObject o = list.get(position);
        //holder.loading.setVisibility(View.VISIBLE);
        BaseFunctions.setFrescoImage(holder.image,o.getImage());
        holder.title.setText(o.getTitle());
        if (o.getDetails().length()>25){
            holder.text.setText(o.getDetails().substring(0,24)+"...");
        }else {
            holder.text.setText(o.getDetails());
        }

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(position);
            }
        });
    }
}
