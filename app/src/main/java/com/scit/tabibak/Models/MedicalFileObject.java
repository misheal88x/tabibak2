package com.scit.tabibak.Models;

import com.google.gson.annotations.SerializedName;

public class MedicalFileObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("title") private String title = "";
    @SerializedName("details") private String details = "";
    @SerializedName("image") private String image = "";
    @SerializedName("user_id") private int user_id = 0;
    @SerializedName("created_at") private String created_at = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
