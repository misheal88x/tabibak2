package com.scit.tabibak.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scit.tabibak.APIsClasses.StartupAPIsClass;
import com.scit.tabibak.Activities.SplashActivity;
import com.scit.tabibak.Bases.BaseFragment;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Dialogs.ContactUsDialog;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Models.Startup_models.HelpResponse;
import com.scit.tabibak.Models.UserObject;
import com.scit.tabibak.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import im.delight.android.webview.AdvancedWebView;

public class HelpFragment extends BaseFragment {

    private RelativeLayout toolbar;
    private TextView title;
    private RelativeLayout call,help,logout;
    private View divider;
    //private WebView webView;
    private AdvancedWebView mWebView;
    private ProgressBar loading;
    private LinearLayout no_data,no_internet,error;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_help,container,false);
    }

    @Override
    public void init_views() {
        toolbar = base.findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        call = toolbar.findViewById(R.id.contact_us);
        help = toolbar.findViewById(R.id.help);
        logout = toolbar.findViewById(R.id.logout);
        if (!SharedPrefManager.getInstance(base).getLogged()){
            logout.setVisibility(View.GONE);
        }
        //webView = base.findViewById(R.id.web_view);
        mWebView = base.findViewById(R.id.web_view);
        loading = base.findViewById(R.id.loading);
        no_data = base.findViewById(R.id.no_data_layout);
        no_internet = base.findViewById(R.id.no_internet_layout);
        error = base.findViewById(R.id.error_layout);
    }

    @Override
    public void init_events() {
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactUsDialog dialog = new ContactUsDialog(base);
                dialog.show();
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(base);
                builder.setTitle(getResources().getString(R.string.logout_title));
                builder.setMessage(getResources().getString(R.string.logout_message));
                builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPrefManager.getInstance(base).setLogged(false);
                        SharedPrefManager.getInstance(base).setUser(new UserObject());
                        startActivity(new Intent(base, SplashActivity.class));
                        base.finish();
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
        error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callHelpAPI();
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        help.setVisibility(View.GONE);
        title.setText(getResources().getString(R.string.home_help));
        callHelpAPI();
    }

    private void callHelpAPI(){
        loading.setVisibility(View.VISIBLE);
        no_internet.setVisibility(View.GONE);
        no_data.setVisibility(View.GONE);
        error.setVisibility(View.GONE);
        StartupAPIsClass.get_help(base,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        loading.setVisibility(View.GONE);
                        error.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        loading.setVisibility(View.GONE);
                        String j = new Gson().toJson(json);
                        HelpResponse success = new Gson().fromJson(j,HelpResponse.class);
                        if (success.getText()!=null){
                            if (!success.getText().equals("")){
                                mWebView.loadHtml(success.getText());
                                //BaseFunctions.setWebViewWithoutBody(webView,success.getText());
                            }else {
                                no_data.setVisibility(View.VISIBLE);
                            }
                        }else {
                            no_data.setVisibility(View.VISIBLE);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        no_internet.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                    }
                });
    }
}
