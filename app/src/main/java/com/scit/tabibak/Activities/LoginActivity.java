package com.scit.tabibak.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
//import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.scit.tabibak.APIsClasses.UserAPIsClass;
import com.scit.tabibak.Bases.BaseActivity;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Fragments.MedicalConsulationFragment;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Models.UserObject;
import com.scit.tabibak.R;

public class LoginActivity extends BaseActivity {

    private EditText phone,password;
    private TextView btn_login,btn_register;
    private ProgressBar loading;
    @Override
    public void set_layout() {
        setContentView(R.layout.activity_login);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {

    }

    @Override
    public void init_views() {
        phone = findViewById(R.id.phone);
        password = findViewById(R.id.password);
        btn_login = findViewById(R.id.login);
        btn_register = findViewById(R.id.register);
        loading = findViewById(R.id.loading);
        BaseFunctions.underlineText(btn_register,getResources().getString(R.string.login_register));
    }

    @Override
    public void init_events() {
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phone.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(LoginActivity.this,getResources().getString(R.string.register_no_phone));
                    return;
                }
                if (phone.getText().toString().length()<10){
                    BaseFunctions.showWarningToast(LoginActivity.this,getResources().getString(R.string.register_phone_short));
                    return;
                }
                if (password.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(LoginActivity.this,getResources().getString(R.string.register_no_password));
                    return;
                }
                if (password.getText().toString().length()<8){
                    BaseFunctions.showWarningToast(LoginActivity.this,getResources().getString(R.string.register_password_short));
                    return;
                }
                callLoginAPI(phone.getText().toString(),password.getText().toString());
            }
        });
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void callLoginAPI(String phone,String password){
        show_loading(true);
        UserAPIsClass.login(
                LoginActivity.this,
                phone,
                password,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        show_loading(false);
                        //BaseFunctions.showErrorToast(LoginActivity.this,getResources().getString(R.string.error_occured_2));
                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        UserObject success = new Gson().fromJson(j,UserObject.class);
                        SharedPrefManager.getInstance(LoginActivity.this).setLogged(true);
                        SharedPrefManager.getInstance(LoginActivity.this).setUser(success);

                        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(LoginActivity.this, new OnSuccessListener<InstanceIdResult>() {
                            @Override
                            public void onSuccess(InstanceIdResult instanceIdResult) {
                                String token = instanceIdResult.getToken();
                                Log.e("newToken1", token);
                                callUpdateTokenAPI(token);
                            }
                        });
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        show_loading(false);
                        BaseFunctions.showErrorToast(LoginActivity.this,getResources().getString(R.string.no_internet));
                    }
                });
    }

    private void show_loading(boolean status){
        if (status){
            loading.setVisibility(View.VISIBLE);
            btn_login.setVisibility(View.GONE);
            phone.setEnabled(false);
            password.setEnabled(false);
        }else {
            loading.setVisibility(View.GONE);
            btn_login.setVisibility(View.VISIBLE);
            phone.setEnabled(true);
            password.setEnabled(true);
        }
    }

    private void callUpdateTokenAPI(String token){
        UserAPIsClass.update_token(LoginActivity.this,
                token,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        Log.i("firebase_token_update", "onResponse: "+"Failed");
                        show_loading(false);
                        finish();
                        BaseFunctions.runBackSlideAnimation(LoginActivity.this);
                    }

                    @Override
                    public void onResponse(Object json) {
                        Log.i("firebase_token_update", "onResponse: "+"Success");
                        show_loading(false);
                        startActivity(new Intent(LoginActivity.this,MainActivity.class));
                        finish();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Log.i("firebase_token_update", "onResponse: "+"No Internet");
                        show_loading(false);
                        finish();
                        BaseFunctions.runBackSlideAnimation(LoginActivity.this);
                    }
                });
    }
}
