package com.scit.tabibak.Fragments;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.gson.Gson;
import com.scit.tabibak.APIsClasses.ImportantNotesAPIsClass;
import com.scit.tabibak.APIsClasses.MedicalFilesAPIsClass;
import com.scit.tabibak.Activities.SplashActivity;
import com.scit.tabibak.Adapters.HealthNotesAdapter;
import com.scit.tabibak.Adapters.ImportantNotesAdapter;
import com.scit.tabibak.Adapters.MedicalFilesAdapter;
import com.scit.tabibak.Adapters.MyHealthRecordTextsAdapter;
import com.scit.tabibak.Bases.BaseFragment;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Dialogs.ContactUsDialog;
import com.scit.tabibak.Dialogs.HealthNoteDetailsDialog;
import com.scit.tabibak.Dialogs.ImportantNoteDetailsDialog;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Interfaces.OnItemClickListener;
import com.scit.tabibak.Models.Important_notes_models.ImportantNoteObject;
import com.scit.tabibak.Models.MedicalFileObject;
import com.scit.tabibak.Models.UserObject;
import com.scit.tabibak.R;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.app.Activity.RESULT_OK;

public class VeryImportantFragment extends BaseFragment implements  BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.ImageLoaderDelegate, OnItemClickListener {

    private RelativeLayout toolbar;
    private TextView title;
    private RelativeLayout call,help,logout;

    private CheckBox with_alarm;
    private Button add_note,add_note_image;
    private ImageView minimize;
    private LinearLayout add_note_layout;
    private EditText note_title,note_desc;
    private TextView note_date,note_time,note_add;
    private RecyclerView notes_recycler;
    private List<ImportantNoteObject> list;
    private ImportantNotesAdapter notes_adapter;
    private LinearLayoutManager notes_layout_manager;
    private String noteImagePath = "";
    private static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";
    private ImageView note_image;
    private String selected_note_date = "";
    private String selected_note_time = "";

    private Animation fadeIn,fadeOut;

    private TextView added_notes;
    private ProgressBar loading,loading_add;
    private LinearLayout no_data,no_internet,error;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_very_important,container,false);
    }

    @Override
    public void init_views() {
        //Toolbar
        toolbar = base.findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        call = toolbar.findViewById(R.id.contact_us);
        help = toolbar.findViewById(R.id.help);
        logout = toolbar.findViewById(R.id.logout);
        if (!SharedPrefManager.getInstance(base).getLogged()){
            logout.setVisibility(View.GONE);
        }
        //EditText
        note_title = base.findViewById(R.id.note_title);
        note_desc = base.findViewById(R.id.note_desc);
        //Checkbox
        with_alarm = base.findViewById(R.id.with_alarm);
        //Button
        add_note = base.findViewById(R.id.add_note);
        add_note_image = base.findViewById(R.id.add_note_image);
        //LinearLayout
        add_note_layout = base.findViewById(R.id.add_note_layout);
        no_data = base.findViewById(R.id.no_data_layout);
        no_internet = base.findViewById(R.id.no_internet_layout);
        error = base.findViewById(R.id.error_layout);
        //TextView
        note_date = base.findViewById(R.id.note_date);
        note_time = base.findViewById(R.id.note_time);
        note_add = base.findViewById(R.id.add_note_btn);
        added_notes = base.findViewById(R.id.added_notes_title);
        //RecyclerView
        notes_recycler = base.findViewById(R.id.notes_recycler);
        //ImageView
        note_image = base.findViewById(R.id.note_image);
        minimize = base.findViewById(R.id.minimize);
        //Animation
        fadeIn = AnimationUtils.loadAnimation(base,R.anim.fade_in);
        fadeOut = AnimationUtils.loadAnimation(base,R.anim.fade_out);
        //ProgressBars
        loading = base.findViewById(R.id.loading);
        loading_add = base.findViewById(R.id.loading_add);
    }

    @Override
    public void init_events() {
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(base);
                builder.setTitle(getResources().getString(R.string.logout_title));
                builder.setMessage(getResources().getString(R.string.logout_message));
                builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPrefManager.getInstance(base).setLogged(false);
                        SharedPrefManager.getInstance(base).setUser(new UserObject());
                        startActivity(new Intent(base, SplashActivity.class));
                        base.finish();
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactUsDialog dialog = new ContactUsDialog(base);
                dialog.show();
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base.open_fragment(new HelpFragment());
            }
        });
        add_note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_note.setVisibility(View.GONE);
                add_note_layout.setVisibility(View.VISIBLE);
                add_note_layout.startAnimation(fadeIn);
                minimize.setVisibility(View.VISIBLE);
            }
        });
        minimize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                note_title.setText("");
                note_desc.setText("");
                noteImagePath = "";
                note_image.setVisibility(View.GONE);
                with_alarm.setChecked(false);
                note_date.setText(getResources().getString(R.string.health_note_date));
                note_time.setText(getResources().getString(R.string.health_note_time));
                note_date.setVisibility(View.GONE);
                note_time.setVisibility(View.GONE);
                add_note_layout.setVisibility(View.GONE);
                add_note.setVisibility(View.VISIBLE);
                minimize.setVisibility(View.GONE);
            }
        });
        note_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (note_title.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.important_no_title));
                    return;
                }
                if (note_desc.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.important_no_desc));
                    return;
                }
                //if (noteImagePath.equals("")){
                //    BaseFunctions.showWarningToast(base,getResources().getString(R.string.important_no_image));
                //    return;
               // }
                if (with_alarm.isChecked()){
                    if (selected_note_date.equals("")){
                        BaseFunctions.showWarningToast(base,getResources().getString(R.string.important_no_date));
                        return;
                    }
                    if (selected_note_time.equals("")){
                        BaseFunctions.showWarningToast(base,getResources().getString(R.string.important_no_time));
                        return;
                    }
                }
                callAddNoteAPI(note_title.getText().toString(),note_desc.getText().toString());
                /*
                note_title.setText("");
                note_desc.setText("");
                noteImagePath = "";
                note_image.setVisibility(View.GONE);
                with_alarm.setChecked(false);
                note_date.setText(getResources().getString(R.string.health_note_date));
                note_time.setText(getResources().getString(R.string.health_note_time));
                note_date.setVisibility(View.GONE);
                note_time.setVisibility(View.GONE);
                add_note_layout.setVisibility(View.GONE);
                add_note.setVisibility(View.VISIBLE);

                 */
            }
        });
        with_alarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    note_date.setVisibility(View.VISIBLE);
                    note_date.startAnimation(fadeIn);
                    note_time.setVisibility(View.VISIBLE);
                    note_time.startAnimation(fadeIn);
                }else {
                    note_date.setVisibility(View.GONE);
                    note_time.setVisibility(View.GONE);
                    note_date.setText(getResources().getString(R.string.health_note_date));
                    note_time.setText(getResources().getString(R.string.health_note_time));
                }
            }
        });
        add_note_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE},1000);
            }
        });

        note_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar newCalendar = Calendar.getInstance();
                DatePickerDialog cal = new DatePickerDialog(base, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, month, day);

                        String year_str = new String();
                        String month_str = new String();
                        String day_str = new String();
                        year_str = String.valueOf(year);
                        if (month + 1 < 10) {
                            month_str = "0" + String.valueOf(month + 1);
                        } else {
                            month_str = String.valueOf(month + 1);
                        }
                        if (day < 10) {
                            day_str = "0" + String.valueOf(day);
                        } else {
                            day_str = String.valueOf(day);
                        }
                        String desDate = year_str + "-" + month_str + "-" + day_str;
                        selected_note_date = desDate;
                        note_date.setText(selected_note_date);
                    }

                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                cal.show();
            }
        });
        note_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar newCalendar = Calendar.getInstance();
                TimePickerDialog time_picker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hour, int minute) {
                        String hour_str = new String();
                        String minute_str = new String();
                        if (hour<10){
                            hour_str = "0"+String.valueOf(hour);
                        }else {
                            hour_str = String.valueOf(hour);
                        }
                        if (minute<10){
                            minute_str = "0"+String.valueOf(minute);
                        }else {
                            minute_str = String.valueOf(minute);
                        }
                        String fullTime = hour_str+":"+minute_str+":00";

                        selected_note_time = fullTime;
                        note_time.setText(selected_note_time);
                    }
                },newCalendar.get(Calendar.HOUR_OF_DAY),newCalendar.get(Calendar.MINUTE), DateFormat.is24HourFormat(base));
                time_picker.show();
            }
        });

        error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callGetNotesAPI();
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        title.setText(getResources().getString(R.string.record_important));
        init_recyclers();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1000){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
                    ImagePicker.Companion.with(VeryImportantFragment.this)
                            .galleryOnly()
                            .crop(16f,9f)	    			//Crop image(Optional), Check Customization for more option
                            .compress(1024)			//Final image size will be less than 1 MB(Optional)
                            .start();
                }else {
                    BSImagePicker singleSelectionPicker = new BSImagePicker.Builder("com.scit.tabibak.provider")
                            .hideCameraTile() //Default: show. Set this if you don't want user to take photo.
                            .hideGalleryTile() //Default: show. Set this if you don't want to further let user select from a gallery app. In such case, I suggest you to set maximum displaying images to Integer.MAX_VALUE.
                            .build();
                    singleSelectionPicker.show(getChildFragmentManager(),"Picker");
                }

            }else {
                BaseFunctions.showErrorToast(base,getResources().getString(R.string.no_permission));
            }
        }
    }

    private void init_recyclers(){
        list = new ArrayList<>();
        notes_adapter = new ImportantNotesAdapter(base,list,this);
        notes_layout_manager = new LinearLayoutManager(base);
        notes_recycler.setLayoutManager(notes_layout_manager);
        notes_recycler.setAdapter(notes_adapter);
        callGetNotesAPI();
    }

    @Override
    public void loadImage(Uri imageUri, ImageView ivImage) {
        Glide.with(base).load(imageUri).into(ivImage);
    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        Uri source_uri = uri;
        Uri destination_uri = Uri.fromFile(getTempFile());
        Crop.of(source_uri,destination_uri).withAspect(16,9).start(base,VeryImportantFragment.this);
    }

    private File getTempFile() {
        if (isSDCARDMounted()) {

            File f = new File(Environment.getExternalStorageDirectory(),TEMP_PHOTO_FILE);
            try {
                f.createNewFile();
            } catch (IOException e) {

            }
            return f;
        } else {
            return null;
        }
    }

    private boolean isSDCARDMounted(){
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED))
            return true;
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Crop.REQUEST_CROP:{
                handle_crop(resultCode,data);
            }break;
            default:{
                if (resultCode == Activity.RESULT_OK) {
                    String filePath = ImagePicker.Companion.getFilePath(data);
                    noteImagePath = filePath;
                    Bitmap myBitmap = BitmapFactory.decodeFile(noteImagePath);
                    note_image.setImageBitmap(myBitmap);
                    note_image.setVisibility(View.VISIBLE);
                    note_image.startAnimation(fadeIn);
                }
            }
        }
    }

    private void handle_crop(int code, final Intent data){
        if (code == RESULT_OK){
            base.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String filePath=  Environment.getExternalStorageDirectory()
                            + "/"+"temporary_holder.jpg";
                    if (new File(filePath).exists()){
                            noteImagePath = filePath;
                            Bitmap myBitmap = BitmapFactory.decodeFile(noteImagePath);
                            note_image.setImageBitmap(myBitmap);
                            note_image.setVisibility(View.VISIBLE);
                            note_image.startAnimation(fadeIn);
                    }

                }
            });
        }
    }

    @Override
    public void onItemClick(int position) {
        ImportantNoteDetailsDialog dialog = new ImportantNoteDetailsDialog(base,list.get(position));
        dialog.show();
    }

    private void callGetNotesAPI(){
        loading.setVisibility(View.VISIBLE);
        added_notes.setVisibility(View.GONE);
        list.clear();
        notes_adapter = new ImportantNotesAdapter(base,list,this);
        notes_recycler.setAdapter(notes_adapter);
        no_data.setVisibility(View.GONE);
        error.setVisibility(View.GONE);
        no_internet.setVisibility(View.GONE);
        ImportantNotesAPIsClass.get_all(base,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        loading.setVisibility(View.GONE);
                        error.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        loading.setVisibility(View.GONE);
                        String j = new Gson().toJson(json);
                        try {
                            ImportantNoteObject[] success = new Gson().fromJson(j, ImportantNoteObject[].class);
                            if (success.length > 0) {
                                added_notes.setVisibility(View.VISIBLE);
                                for (ImportantNoteObject o : success) {
                                    list.add(o);
                                    notes_adapter.notifyDataSetChanged();
                                }
                            } else {
                                added_notes.setVisibility(View.GONE);
                                no_data.setVisibility(View.VISIBLE);
                            }
                        }catch (Exception e){
                            callGetNotesAPI();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        loading.setVisibility(View.GONE);
                        no_internet.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void callAddNoteAPI(String title,String details){
        showAddLoading(true);
        ImportantNotesAPIsClass.add_note(
                base,
                title,
                details,
                selected_note_date,
                selected_note_time,
                noteImagePath,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        showAddLoading(false);
                    }

                    @Override
                    public void onResponse(Object json) {
                        showAddLoading(false);
                        BaseFunctions.showSuccessToast(base,getResources().getString(R.string.file_add_success));
                        note_title.setText("");
                        note_desc.setText("");
                        noteImagePath = "";
                        note_image.setVisibility(View.GONE);
                        with_alarm.setChecked(false);
                        note_date.setText(getResources().getString(R.string.health_note_date));
                        note_time.setText(getResources().getString(R.string.health_note_time));
                        note_date.setVisibility(View.GONE);
                        note_time.setVisibility(View.GONE);
                        add_note_layout.setVisibility(View.GONE);
                        add_note.setVisibility(View.VISIBLE);
                        minimize.setVisibility(View.GONE);
                        BaseFunctions.playMusic(base,R.raw.added);
                        callGetNotesAPI();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        showAddLoading(false);
                        BaseFunctions.showErrorToast(base,getResources().getString(R.string.no_internet));
                    }
                });
    }

    private void showAddLoading(boolean state){
        if (state){
            loading_add.setVisibility(View.VISIBLE);
            note_add.setVisibility(View.GONE);
            note_title.setEnabled(false);
            note_desc.setEnabled(false);
            add_note_image.setEnabled(false);
        }else {
            loading_add.setVisibility(View.GONE);
            note_add.setVisibility(View.VISIBLE);
            note_title.setEnabled(true);
            note_desc.setEnabled(true);
            add_note_image.setEnabled(true);
        }
    }
}
