package com.scit.tabibak.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.scit.tabibak.Bases.BaseFragment;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Interfaces.ILoadImage;
import com.scit.tabibak.Models.Medical_guide_models.SearchResultObject;
import com.scit.tabibak.Models.Startup_models.CityObject;
import com.scit.tabibak.Models.Startup_models.SpecializationObject;
import com.scit.tabibak.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ProfileFragment extends BaseFragment {

    private TextView name,city,address,profession,identity_file,phone,email,facebook;
    private ImageView profile_pic;
    private ProgressBar load_image_progress;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile,container,false);
    }

    @Override
    public void init_views() {
        name = base.findViewById(R.id.name);
        city = base.findViewById(R.id.city);
        address = base.findViewById(R.id.address);
        profession = base.findViewById(R.id.profession);
        identity_file = base.findViewById(R.id.identity_file);
        phone = base.findViewById(R.id.phone);
        email = base.findViewById(R.id.email);
        facebook = base.findViewById(R.id.facebook);

        profile_pic = base.findViewById(R.id.profile_pic);
        load_image_progress = base.findViewById(R.id.load_image);
    }

    @Override
    public void init_events() {

    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        SearchResultObject o = (SearchResultObject) getArguments().getSerializable("data");

        load_image_progress.setVisibility(View.VISIBLE);
        BaseFunctions.setGlideImageWithEvents(base, profile_pic, o.getAvatar(), new ILoadImage() {
            @Override
            public void onLoaded() {
                load_image_progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailed() {
                load_image_progress.setVisibility(View.GONE);
            }
        });

        name.setText(o.getName());
        if (SharedPrefManager.getInstance(base).getCities().size()>0){
            for (CityObject c : SharedPrefManager.getInstance(base).getCities()){
                if (c.getId() == o.getCity_id()){
                    city.setText(c.getName());
                    break;
                }
            }
        }
        address.setText(o.getAddress());
        if (SharedPrefManager.getInstance(base).getSpes().size()>0){
            for (SpecializationObject s : SharedPrefManager.getInstance(base).getSpes()){
                if (s.getId() == o.getSpecialization_id()){
                    profession.setText(s.getName());
                    break;
                }
            }
        }
        identity_file.setText(o.getIdentification_file());
        phone.setText(o.getPhone());
        email.setText(o.getEmail());
        facebook.setText(o.getFacebook());
    }

}
