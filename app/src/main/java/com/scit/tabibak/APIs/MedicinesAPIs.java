package com.scit.tabibak.APIs;

import com.scit.tabibak.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MedicinesAPIs {
    @GET("medicines")
    Call<BaseResponse> get_all(@Header("Accept") String accept,
                               @Query("page") int page);

    @GET("medicines/{id}")
    Call<BaseResponse> get_med(@Header("Accept") String accept,
                               @Path("id") String id);
    @GET("search-med/{name}")
    Call<BaseResponse> search(
            @Header("Accept") String accept,
            @Path("name") String name
    );
}
