package com.scit.tabibak.Dialogs;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;

import com.scit.tabibak.APIsClasses.MedicalCosulationAPIsClass;
import com.scit.tabibak.APIsClasses.MedicalDatesAPIsClass;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.R;

import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;

public class AddDoctorDateDialog extends AlertDialog {
    private Context context;
    private int id;
    private EditText title,description;
    private TextView date,time,add_date;
    private ProgressBar loading;

    private String selected_date = "",selected_time = "";

    public AddDoctorDateDialog(@NonNull Context context, int id) {
        super(context);
        this.context = context;
        this.id = id;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_add_doctor_date);

        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

        init_views();
        init_events();
        init_dialog();
    }

    private void init_views() {
        title = findViewById(R.id.date_title);
        description = findViewById(R.id.desc);
        date = findViewById(R.id.date);
        time = findViewById(R.id.time);
        loading = findViewById(R.id.loading);
        add_date = findViewById(R.id.add);
    }

    private void init_events() {
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar newCalendar = Calendar.getInstance();
                DatePickerDialog cal = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, month, day);

                        String year_str = new String();
                        String month_str = new String();
                        String day_str = new String();
                        year_str = String.valueOf(year);
                        if (month + 1 < 10) {
                            month_str = "0" + String.valueOf(month + 1);
                        } else {
                            month_str = String.valueOf(month + 1);
                        }
                        if (day < 10) {
                            day_str = "0" + String.valueOf(day);
                        } else {
                            day_str = String.valueOf(day);
                        }
                        String desDate = year_str + "-" + month_str + "-" + day_str;
                        selected_date = desDate;
                        date.setText(selected_date);
                    }

                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                cal.show();
            }
        });

        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar newCalendar = Calendar.getInstance();
                TimePickerDialog time_picker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hour, int minute) {
                        String hour_str = new String();
                        String minute_str = new String();
                        if (hour<10){
                            hour_str = "0"+String.valueOf(hour);
                        }else {
                            hour_str = String.valueOf(hour);
                        }
                        if (minute<10){
                            minute_str = "0"+String.valueOf(minute);
                        }else {
                            minute_str = String.valueOf(minute);
                        }
                        String fullTime = hour_str+":"+minute_str+":00";

                        selected_time = fullTime;
                        time.setText(selected_time);
                    }
                },newCalendar.get(Calendar.HOUR_OF_DAY),newCalendar.get(Calendar.MINUTE), DateFormat.is24HourFormat(context));
                time_picker.show();
            }
        });

        add_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (title.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(context,context.getResources().getString(R.string.medical_date_no_title));
                    return;
                }
                if (description.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(context,context.getResources().getString(R.string.medical_date_no_desc));
                    return;
                }
                if (selected_date.equals("")){
                    BaseFunctions.showWarningToast(context,context.getResources().getString(R.string.medical_date_no_date));
                    return;
                }
                if (selected_time.equals("")){
                    BaseFunctions.showWarningToast(context,context.getResources().getString(R.string.medical_date_no_time));
                    return;
                }
                callAddDateAPI(title.getText().toString(),description.getText().toString());
            }
        });
    }

    private void init_dialog() {

    }

    private void callAddDateAPI(String title,String details){
        loading.setVisibility(View.VISIBLE);
        add_date.setVisibility(View.GONE);
        MedicalDatesAPIsClass.add_date(
                context,
                title,
                details,
                selected_date,
                selected_time,
                id,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        loading.setVisibility(View.GONE);
                        add_date.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        loading.setVisibility(View.GONE);
                        add_date.setVisibility(View.VISIBLE);
                        BaseFunctions.showSuccessToast(context, context.getResources().getString(R.string.medical_date_success));
                        cancel();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        loading.setVisibility(View.GONE);
                        add_date.setVisibility(View.VISIBLE);
                        BaseFunctions.showErrorToast(context,context.getResources().getString(R.string.no_internet));
                    }
                });
    }
}
