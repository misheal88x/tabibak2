package com.scit.tabibak.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scit.tabibak.Activities.LoginActivity;
import com.scit.tabibak.Activities.MainActivity;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Dialogs.RequestDateDialog;
import com.scit.tabibak.Interfaces.OnItemClickListener;
import com.scit.tabibak.Models.Medical_guide_models.SearchResultObject;
import com.scit.tabibak.Models.Startup_models.FacilityObject;
import com.scit.tabibak.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class SearchResultsAdapter extends RecyclerView.Adapter<SearchResultsAdapter.ViewHolder> {
    private Context context;
    private List<SearchResultObject> list;
    private OnItemClickListener onItemClickListener;



    public SearchResultsAdapter( Context context,List<SearchResultObject> list,OnItemClickListener onItemClickListener) {
        this.context = context;
        this.list = list;
        this.onItemClickListener = onItemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name,type,request_date;
        private ImageView logo;
        private CircleImageView icon;
        private CardView layout;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            type = view.findViewById(R.id.type);
            request_date = view.findViewById(R.id.request_date);
            logo = view.findViewById(R.id.logo);
            icon = view.findViewById(R.id.icon);
            layout = view.findViewById(R.id.layout);

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_search_result, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final SearchResultObject o = list.get(position);
        holder.name.setText(o.getName());
        if (SharedPrefManager.getInstance(context).getFacilities().size()>0){
            for (FacilityObject f : SharedPrefManager.getInstance(context).getFacilities()){
                if (f.getId() == o.getFacility_id()){
                    holder.type.setText(f.getName());
                    break;
                }
            }
        }
        BaseFunctions.setGlideImage(context,holder.icon,o.getAvatar());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(position);
            }
        });
        holder.request_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPrefManager.getInstance(context).getLogged()){
                    RequestDateDialog dialog = new RequestDateDialog(context,o.getId());
                    dialog.show();
                }else {
                    Intent intent = new Intent(context, LoginActivity.class);
                    context.startActivity(intent);
                }
            }
        });
    }
}
