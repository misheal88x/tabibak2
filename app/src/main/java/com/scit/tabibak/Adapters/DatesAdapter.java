package com.scit.tabibak.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scit.tabibak.APIsClasses.MedicalDatesAPIsClass;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.ILoadImage;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Interfaces.OnItemClickListener;
import com.scit.tabibak.Models.MedicalFileObject;
import com.scit.tabibak.Models.Medical_dates_models.MedicalDateObject;
import com.scit.tabibak.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class DatesAdapter extends RecyclerView.Adapter<DatesAdapter.ViewHolder>{
    private Context context;
    private List<MedicalDateObject> list;
    private OnItemClickListener onItemClickListener;

    public DatesAdapter(Context context,List<MedicalDateObject> list,OnItemClickListener onItemClickListener) {
        this.context = context;
        this.list = list;
        this.onItemClickListener = onItemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView title,date,time,delete;
        private CardView layout;
        private ProgressBar loading;

        public ViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            date = view.findViewById(R.id.date);
            time = view.findViewById(R.id.time);
            delete = view.findViewById(R.id.delete);
            loading = view.findViewById(R.id.loading);
            layout = view.findViewById(R.id.layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_date, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final MedicalDateObject o = list.get(position);
        holder.title.setText(o.getTitle());
        holder.date.setText(o.getDate());
        holder.time.setText(o.getTime());
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(context.getResources().getString(R.string.medical_date_delete_message));
                builder.setPositiveButton(context.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callDeleteAPI(holder,o.getId(),position);
                    }
                });
                builder.setNegativeButton(context.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(position);
            }
        });
    }

    private void callDeleteAPI(final ViewHolder holder, int id, final int position){
        holder.delete.setVisibility(View.GONE);
        holder.loading.setVisibility(View.VISIBLE);
        MedicalDatesAPIsClass.delete_date(context, id, new IResponse() {
            @Override
            public void onResponse() {
                holder.delete.setVisibility(View.VISIBLE);
                holder.loading.setVisibility(View.GONE);
            }

            @Override
            public void onResponse(Object json) {
                holder.delete.setVisibility(View.VISIBLE);
                holder.loading.setVisibility(View.GONE);
                BaseFunctions.showSuccessToast(context,context.getResources().getString(R.string.medical_date_delete_success));
                list.remove(position);
                notifyDataSetChanged();
            }
        }, new IFailure() {
            @Override
            public void onFailure() {
                holder.delete.setVisibility(View.VISIBLE);
                holder.loading.setVisibility(View.GONE);
                BaseFunctions.showErrorToast(context,context.getResources().getString(R.string.no_internet));
            }
        });
    }
}
