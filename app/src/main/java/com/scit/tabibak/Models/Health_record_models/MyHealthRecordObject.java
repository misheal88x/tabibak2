package com.scit.tabibak.Models.Health_record_models;

import com.google.gson.annotations.SerializedName;
import com.scit.tabibak.Models.Diagnosis_models.DiagnosisObject;

import java.util.ArrayList;
import java.util.List;

public class MyHealthRecordObject {
    @SerializedName("precedent") private List<PreviousDiseaseObject> precedent = new ArrayList<>();
    @SerializedName("genetic") private List<ChronicDiseaseObject> genetic = new ArrayList<>();
    @SerializedName("medical_images") private List<HealthMedicalImageObject> medical_images = new ArrayList<>();
    @SerializedName("medical_reports") private List<BasicHealthInfo> medical_reports = new ArrayList<>();
    @SerializedName("medicines") private List<ContinueMedicinesObject> medicines = new ArrayList<>();
    @SerializedName("diagnoses") private List<DiagnosisObject> diagnoses = new ArrayList<>();

    public List<PreviousDiseaseObject> getPrecedent() {
        return precedent;
    }

    public void setPrecedent(List<PreviousDiseaseObject> precedent) {
        this.precedent = precedent;
    }

    public List<ChronicDiseaseObject> getGenetic() {
        return genetic;
    }

    public void setGenetic(List<ChronicDiseaseObject> genetic) {
        this.genetic = genetic;
    }

    public List<HealthMedicalImageObject> getMedical_images() {
        return medical_images;
    }

    public void setMedical_images(List<HealthMedicalImageObject> medical_images) {
        this.medical_images = medical_images;
    }

    public List<BasicHealthInfo> getMedical_reports() {
        return medical_reports;
    }

    public void setMedical_reports(List<BasicHealthInfo> medical_reports) {
        this.medical_reports = medical_reports;
    }

    public List<ContinueMedicinesObject> getMedicines() {
        return medicines;
    }

    public void setMedicines(List<ContinueMedicinesObject> medicines) {
        this.medicines = medicines;
    }

    public List<DiagnosisObject> getDiagnoses() {
        return diagnoses;
    }

    public void setDiagnoses(List<DiagnosisObject> diagnoses) {
        this.diagnoses = diagnoses;
    }
}
