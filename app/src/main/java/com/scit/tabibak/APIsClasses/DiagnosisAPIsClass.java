package com.scit.tabibak.APIsClasses;

import android.content.Context;

import com.scit.tabibak.APIs.DiagnosisAPIs;
import com.scit.tabibak.APIs.MedicalDatesAPIs;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.BaseRetrofit;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DiagnosisAPIsClass extends BaseRetrofit {
    public static void add(
            final Context context,
            int user_id,
            String status,
            String text,
            String medicines,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        DiagnosisAPIs api = retrofit.create(DiagnosisAPIs.class);
        Call<BaseResponse> call = api.add("application/json",user_id,status,text,medicines);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
}
