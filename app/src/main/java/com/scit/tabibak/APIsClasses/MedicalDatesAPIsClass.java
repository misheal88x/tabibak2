package com.scit.tabibak.APIsClasses;

import android.content.Context;

import com.scit.tabibak.APIs.MedicalDatesAPIs;
import com.scit.tabibak.APIs.MedicalFilesAPIs;
import com.scit.tabibak.APIs.StartupAPIs;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.BaseRetrofit;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MedicalDatesAPIsClass extends BaseRetrofit {


    public static void get_all(
            final Context context,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicalDatesAPIs api = retrofit.create(MedicalDatesAPIs.class);
        Call<BaseResponse> call = api.get_all("application/json");
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void add_date(
            final Context context,
            String title,
            String details,
            String date,
            String time,
            int id,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicalDatesAPIs api = retrofit.create(MedicalDatesAPIs.class);
        Call<BaseResponse> call = api.add_date("application/json",title,details,date,time,id,
                SharedPrefManager.getInstance(context).getUser().getId());
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void delete_date(
            final Context context,
            int date_id,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicalDatesAPIs api = retrofit.create(MedicalDatesAPIs.class);
        Call<BaseResponse> call = api.delete_date("application/json",String.valueOf(date_id));
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void request_date(
            final Context context,
            int doctor_id,
            String complaint,
            String note,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicalDatesAPIs api = retrofit.create(MedicalDatesAPIs.class);
        Call<BaseResponse> call = api.request_date("application/json",doctor_id,complaint,note);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void delete_date_request(
            final Context context,
            int id,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicalDatesAPIs api = retrofit.create(MedicalDatesAPIs.class);
        Call<BaseResponse> call = api.delete_date_request("application/json",String.valueOf(id));
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void get_doctor_requests(
            final Context context,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicalDatesAPIs api = retrofit.create(MedicalDatesAPIs.class);
        Call<BaseResponse> call = api.get_doctor_requests("application/json",
                String.valueOf(SharedPrefManager.getInstance(context).getUser().getId()));
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void get_doctor_dates(
            final Context context,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicalDatesAPIs api = retrofit.create(MedicalDatesAPIs.class);
        Call<BaseResponse> call = api.get_doctor_dates("application/json");
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
    public static void edit_date(
            final Context context,
            int id,
            String date,
            String time,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicalDatesAPIs api = retrofit.create(MedicalDatesAPIs.class);
        Call<BaseResponse> call = api.edit_date("application/json",id,date,time);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
}
