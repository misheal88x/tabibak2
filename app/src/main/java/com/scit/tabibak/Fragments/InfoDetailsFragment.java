package com.scit.tabibak.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scit.tabibak.Activities.SplashActivity;
import com.scit.tabibak.Bases.BaseFragment;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Dialogs.ContactUsDialog;
import com.scit.tabibak.Interfaces.ILoadImage;
import com.scit.tabibak.Models.Medical_info_models.MedicalInfoObject;
import com.scit.tabibak.Models.Startup_models.SpecializationObject;
import com.scit.tabibak.Models.UserObject;
import com.scit.tabibak.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import im.delight.android.webview.AdvancedWebView;

public class InfoDetailsFragment extends BaseFragment {

    private RelativeLayout toolbar;
    private TextView title;
    private RelativeLayout call,help,logout;

    private CircleImageView icon;
    private TextView name,profession;
    private ImageView image;
    private ProgressBar loading;
    private AdvancedWebView web_view;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_info_details,container,false);
    }

    @Override
    public void init_views() {
        toolbar = base.findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        call = toolbar.findViewById(R.id.contact_us);
        help = toolbar.findViewById(R.id.help);
        logout = toolbar.findViewById(R.id.logout);
        if (!SharedPrefManager.getInstance(base).getLogged()){
            logout.setVisibility(View.GONE);
        }

        icon = base.findViewById(R.id.icon);
        name = base.findViewById(R.id.name);
        profession = base.findViewById(R.id.profession);
        image = base.findViewById(R.id.image);
        loading = base.findViewById(R.id.loading);
        web_view = base.findViewById(R.id.web_view);
    }

    @Override
    public void init_events() {
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactUsDialog dialog = new ContactUsDialog(base);
                dialog.show();
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base.open_fragment(new HelpFragment());
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(base);
                builder.setTitle(getResources().getString(R.string.logout_title));
                builder.setMessage(getResources().getString(R.string.logout_message));
                builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPrefManager.getInstance(base).setLogged(false);
                        SharedPrefManager.getInstance(base).setUser(new UserObject());
                        startActivity(new Intent(base, SplashActivity.class));
                        base.finish();
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        MedicalInfoObject m = (MedicalInfoObject)getArguments().getSerializable("info");
        title.setText(m.getName());
        BaseFunctions.setGlideImage(base,icon,m.getOwner().getAvatar());
        name.setText(m.getOwner().getName());
        if (SharedPrefManager.getInstance(base).getSpes().size()>0){
            for (SpecializationObject s : SharedPrefManager.getInstance(base).getSpes()){
                if (s.getId() == m.getOwner().getSpecialization_id()){
                    profession.setText(s.getName());
                    break;
                }
            }
        }
        web_view.loadHtml( m.getDetails());


        loading.setVisibility(View.VISIBLE);
        BaseFunctions.setGlideImageWithEvents(base, image, m.getImage(), new ILoadImage() {
            @Override
            public void onLoaded() {
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailed() {
                loading.setVisibility(View.GONE);
            }
        });
    }
}
