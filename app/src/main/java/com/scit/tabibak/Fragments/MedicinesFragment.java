package com.scit.tabibak.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.scit.tabibak.APIsClasses.MedicalGuideAPIsClass;
import com.scit.tabibak.APIsClasses.MedicinesAPIsClass;
import com.scit.tabibak.Activities.SplashActivity;
import com.scit.tabibak.Adapters.DatesAdapter;
import com.scit.tabibak.Adapters.MedicinesAdapter;
import com.scit.tabibak.Bases.BaseFragment;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Dialogs.ContactUsDialog;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Interfaces.OnItemClickListener;
import com.scit.tabibak.Models.Medical_guide_models.SearchResultObject;
import com.scit.tabibak.Models.Medical_info_models.MedicalInfoObject;
import com.scit.tabibak.Models.Medicine_models.MedicineObject;
import com.scit.tabibak.Models.Medicine_models.MedicinesResponse;
import com.scit.tabibak.Models.UserObject;
import com.scit.tabibak.R;
import com.trendyol.bubblescrollbarlib.BubbleScrollBar;
import com.trendyol.bubblescrollbarlib.BubbleTextProvider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MedicinesFragment extends BaseFragment implements OnItemClickListener {

    private RelativeLayout toolbar;
    private TextView title;
    private RelativeLayout call,help,logout;

    private NestedScrollView scrollView;

    private EditText search;
    private RecyclerView recycler;
    private List<MedicineObject> list,origin;
    private MedicinesAdapter adapter;
    private LinearLayoutManager layoutManager;

    private LinearLayout no_data,no_internet,error;
    private ProgressBar loading,load_more,load_search;

    private int currentPage = 1;
    private boolean continue_paginate = true;
    private int per_page = 20;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_medicines,container,false);
    }


    @Override
    public void init_views() {
        toolbar = base.findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        call = toolbar.findViewById(R.id.contact_us);
        help = toolbar.findViewById(R.id.help);
        logout = toolbar.findViewById(R.id.logout);
        if (!SharedPrefManager.getInstance(base).getLogged()){
            logout.setVisibility(View.GONE);
        }

        search = base.findViewById(R.id.search);
        recycler = base.findViewById(R.id.medicines);
        no_data = base.findViewById(R.id.no_data_layout);
        no_internet = base.findViewById(R.id.no_internet_layout);
        error = base.findViewById(R.id.error_layout);
        loading = base.findViewById(R.id.loading);
        scrollView = base.findViewById(R.id.layout);
        load_more = base.findViewById(R.id.load_more);
        load_search = base.findViewById(R.id.load_search);
    }

    @Override
    public void init_events() {
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(base);
                builder.setTitle(getResources().getString(R.string.logout_title));
                builder.setMessage(getResources().getString(R.string.logout_message));
                builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPrefManager.getInstance(base).setLogged(false);
                        SharedPrefManager.getInstance(base).setUser(new UserObject());
                        startActivity(new Intent(base, SplashActivity.class));
                        base.finish();
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactUsDialog dialog = new ContactUsDialog(base);
                dialog.show();
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base.open_fragment(new HelpFragment());
            }
        });
        error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callMedicinesAPI(currentPage,0);
            }
        });
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (list.size()>=per_page){
                            if (continue_paginate){
                                currentPage++;
                                callMedicinesAPI(currentPage,1);
                            }
                        }
                    }
                }
            }
        });
        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (search.getText().toString().equals("")){
                        BaseFunctions.showErrorToast(getContext(),"يجب عليك كتابة اسم الدواء");
                        return false;
                    }else {
                        callSearchAPI();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        title.setText(getResources().getString(R.string.home_medicine));
        init_recycler();
    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new MedicinesAdapter(base,list,this);
        layoutManager = new LinearLayoutManager(base);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);
        callMedicinesAPI(currentPage,0);
    }

    @Override
    public void onItemClick(int position) {
        currentPage = 1;
        continue_paginate = true;
        Bundle bundle = new Bundle();
        bundle.putInt("id",list.get(position).getId());
        MedicineDetailsFragment f = new MedicineDetailsFragment();
        f.setArguments(bundle);
        base.open_fragment(f);
    }

    private void callMedicinesAPI(final int page, final int type){
        if (type == 1){
            load_more.setVisibility(View.VISIBLE);
        }else {
            loading.setVisibility(View.VISIBLE);
            no_data.setVisibility(View.GONE);
            error.setVisibility(View.GONE);
            no_internet.setVisibility(View.GONE);
        }
        MedicinesAPIsClass.get_all(
                base,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        try {
                            MedicinesResponse success = new Gson().fromJson(j, MedicinesResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    process_data(type);
                                    per_page = success.getPer_page();
                                    for (MedicineObject m : success.getData()) {
                                        list.add(m);
                                        adapter.notifyDataSetChanged();
                                    }
                                }else {
                                    no_data(type);
                                }
                            }else {
                                error_happend(type);
                            }
                        }catch (Exception e){
                            callMedicinesAPI(page,type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        loading.setVisibility(View.GONE);
                        no_internet.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void error_happend(int type){
        if (type == 1){
            load_more.setVisibility(View.GONE);
        }else {
            loading.setVisibility(View.GONE);
            error.setVisibility(View.VISIBLE);
            no_data.setVisibility(View.GONE);
            no_internet.setVisibility(View.GONE);
        }
    }
    private void process_data(int type){
        if (type == 0){
            search.setVisibility(View.VISIBLE);
            loading.setVisibility(View.GONE);
            error.setVisibility(View.GONE);
            no_data.setVisibility(View.GONE);
            no_internet.setVisibility(View.GONE);
        }else {
            load_more.setVisibility(View.GONE);
        }
    }
    private void no_data(int type){
        if (type == 0){
            loading.setVisibility(View.GONE);
            no_data.setVisibility(View.VISIBLE);
        }else {
            load_more.setVisibility(View.GONE);
            BaseFunctions.showWarningToast(base,getResources().getString(R.string.no_more));
            continue_paginate = false;
        }
    }

    private void no_internet(int type){
        if (type == 0){
            loading.setVisibility(View.GONE);
            no_internet.setVisibility(View.VISIBLE);
        }else {
            load_more.setVisibility(View.GONE);
            BaseFunctions.showErrorToast(base,getResources().getString(R.string.no_internet));
        }
    }

    public static void sendMessageByWhatsapp(Context context, String message){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        String[] addresses = new String[]{"mishealibrahim1994@gmail.com"};
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Error");
        intent.putExtra(Intent.EXTRA_TEXT, message);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }else {
            PackageManager pm=context.getPackageManager();
            try {

                Intent waIntent = new Intent(Intent.ACTION_SEND);
                waIntent.setType("text/plain");
                String text = message;

                PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                //Check if package exists or not. If not then code
                //in catch block will be called
                waIntent.setPackage("com.whatsapp");

                waIntent.putExtra(Intent.EXTRA_TEXT, text);
                context.startActivity(Intent.createChooser(waIntent, "Share with"));

            } catch (PackageManager.NameNotFoundException e) {
                Toast.makeText(context, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    private void callSearchAPI(){
        load_search.setVisibility(View.VISIBLE);
        MedicinesAPIsClass.search(
                base,
                search.getText().toString(),
                new IResponse() {
                    @Override
                    public void onResponse() {
                        load_search.setVisibility(View.GONE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        load_search.setVisibility(View.GONE);
                        String j = new Gson().toJson(json);
                        try {
                            List<MedicineObject> results = new ArrayList<>();
                            MedicineObject[] success = new Gson().fromJson(j, MedicineObject[].class);
                            if (success.length > 0) {
                                for (MedicineObject s : success) {
                                    results.add(s);
                                }
                                Bundle bundle = new Bundle();
                                bundle.putString("results", new Gson().toJson(results));
                                MedicinesSearchResultFragment fragment = new MedicinesSearchResultFragment();
                                fragment.setArguments(bundle);
                                base.open_fragment(fragment);
                            } else {
                                BaseFunctions.showErrorToast(base, getResources().getString(R.string.no_results));
                            }
                        }catch (Exception e){
                            callSearchAPI();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        load_search.setVisibility(View.GONE);
                        BaseFunctions.showErrorToast(base,getResources().getString(R.string.no_internet));
                    }
                });
    }
}
