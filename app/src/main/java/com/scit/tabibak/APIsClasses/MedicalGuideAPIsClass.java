package com.scit.tabibak.APIsClasses;

import android.content.Context;

import com.scit.tabibak.APIs.MedicalDatesAPIs;
import com.scit.tabibak.APIs.MedicalGuideAPIs;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.BaseRetrofit;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Field;

public class MedicalGuideAPIsClass extends BaseRetrofit {
    public static void search(
            final Context context,
            int city_id,
            int type,
            int specialization_id,
            String name,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicalGuideAPIs api = retrofit.create(MedicalGuideAPIs.class);
        Call<BaseResponse> call = api.search("application/json",city_id,type,specialization_id,name);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
}
