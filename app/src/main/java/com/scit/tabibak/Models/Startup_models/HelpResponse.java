package com.scit.tabibak.Models.Startup_models;

import com.google.gson.annotations.SerializedName;

public class HelpResponse {
    @SerializedName("id") private int id = 0;
    @SerializedName("text") private String text = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
