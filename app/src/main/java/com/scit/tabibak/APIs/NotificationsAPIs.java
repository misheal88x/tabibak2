package com.scit.tabibak.APIs;

import com.scit.tabibak.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface NotificationsAPIs {
    @GET("get-appointment/{id}")
    Call<BaseResponse> get_date(
            @Header("Accept") String accept,
            @Path("id") String id
    );

    @GET("get-important-note/{id}")
    Call<BaseResponse> get_important_note(
            @Header("Accept") String accept,
            @Path("id") String id
    );

    @GET("get-medical-note/{id}")
    Call<BaseResponse> get_medical_note(
            @Header("Accept") String accept,
            @Path("id") String id
    );
}
