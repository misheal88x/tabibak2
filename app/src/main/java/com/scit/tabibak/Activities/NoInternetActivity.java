package com.scit.tabibak.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.scit.tabibak.Bases.BaseActivity;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.R;

public class NoInternetActivity extends BaseActivity {

    private Button retry;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_no_internet);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {

    }

    @Override
    public void init_views() {
        retry = findViewById(R.id.retry);
    }

    @Override
    public void init_events() {
        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BaseFunctions.isOnline(NoInternetActivity.this)){
                    startActivity(new Intent(NoInternetActivity.this,SplashActivity.class));
                    finish();
                }else {
                    BaseFunctions.showErrorToast(NoInternetActivity.this,getResources().getString(R.string.no_internet));
                }
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }
}
