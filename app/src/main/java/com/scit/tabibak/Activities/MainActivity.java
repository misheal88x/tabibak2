package com.scit.tabibak.Activities;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.asksira.bsimagepicker.BSImagePicker;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.scit.tabibak.APIsClasses.UserAPIsClass;
import com.scit.tabibak.Bases.BaseActivity;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Dialogs.DateDetailsDialog;
import com.scit.tabibak.Dialogs.HealthNoteDetailsDialog;
import com.scit.tabibak.Dialogs.ImportantNoteDetailsDialog;
import com.scit.tabibak.Dialogs.RepliesDialog;
import com.scit.tabibak.Fragments.AboutFragment;
import com.scit.tabibak.Fragments.HomeFragment;
import com.scit.tabibak.Fragments.MedicalConsulationFragment;
import com.scit.tabibak.Fragments.MedicalRecordFragment;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Models.Important_notes_models.ImportantNoteObject;
import com.scit.tabibak.Models.Medical_consulation_models.ReplyObject;
import com.scit.tabibak.Models.Medical_dates_models.MedicalDateObject;
import com.scit.tabibak.Models.Medical_notes_models.MedicalNoteObject;
import com.scit.tabibak.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity {


    private boolean back_pressed = false;
    private LinearLayout home_layout,record_layout,about_layout;
    private ImageView home_icon,record_icon,about_icon;
    private TextView home_text,record_text,about_text;
    private int clicked_tab = 0;
    private Animation fadeIn,fadeOut;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_main);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {

        if (SharedPrefManager.getInstance(MainActivity.this).getLogged()) {
            String token = FirebaseInstanceId.getInstance().getToken();
            Log.i("newToken", "init_activity: "+token);
            callUpdateTokenAPI(token);
            //FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(MainActivity.this, new OnSuccessListener<InstanceIdResult>() {
            //    @Override
           //     public void onSuccess(InstanceIdResult instanceIdResult) {
           //         String token = instanceIdResult.getToken();
            //        Log.e("newToken1", token);
            //        callUpdateTokenAPI(token);
           //     }
           // });
        }

        if (getIntent().hasExtra("type")){
            if (getIntent().getStringExtra("type")!=null){
                if (getIntent().getStringExtra("type").equals("record")){
                    click_record();
                }
            }else {
                click_home();
            }
        }else {
            click_home();
        }
    }

    @Override
    public void init_views() {
        home_layout = findViewById(R.id.home_layout);
        record_layout = findViewById(R.id.record_layout);
        about_layout = findViewById(R.id.about_layout);
        home_icon = findViewById(R.id.home_icon);
        record_icon = findViewById(R.id.record_icon);
        about_icon = findViewById(R.id.about_icon);
        home_text = findViewById(R.id.home_text);
        record_text = findViewById(R.id.record_text);
        about_text = findViewById(R.id.about_text);
        fadeIn = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_in);
        fadeOut = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_out);
    }

    @Override
    public void init_events() {
        home_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clicked_tab != 0){
                    clicked_tab = 0;
                    click_home();
                }
            }
        });

        record_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clicked_tab!=1){
                    clicked_tab = 1;
                    click_record();
                }
            }
        });

        about_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clicked_tab!=2){
                    clicked_tab=2;
                    click_about();
                }
            }
        });
    }

    @Override
    public void set_fragment_place() {
        fragment_place = findViewById(R.id.container);
    }

    @Override
    public void onBackPressed() {
        Fragment f = fragmentManager.findFragmentById(R.id.container);
        if (f != null){
            if (f instanceof HomeFragment){
                if (!back_pressed){
                    back_pressed = true;
                    BaseFunctions.showInfoToast(MainActivity.this,getResources().getString(R.string.back_message));
                }else {
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
            }else if (f instanceof MedicalRecordFragment || f instanceof AboutFragment){
                clicked_tab = 0;
                click_home();
            }else {
                fragmentManager.popBackStack();
            }
        }
    }

    private void click_home(){
        home_layout.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_red_rectangle));
        record_layout.setBackgroundDrawable(null);
        about_layout.setBackgroundDrawable(null);

        home_layout.startAnimation(fadeIn);

        home_text.setTextColor(getResources().getColor(R.color.white));
        record_text.setTextColor(getResources().getColor(R.color.grey));
        about_text.setTextColor(getResources().getColor(R.color.grey));

        home_icon.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.white));
        record_icon.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.grey));
        about_icon.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.grey));

        if (getIntent().hasExtra("target_type")){
            if (getIntent().getStringExtra("target_type")!=null){
                if (getIntent().getStringExtra("target_type").equals("6")||
                        getIntent().getStringExtra("target_type").equals("8")||
                        getIntent().getStringExtra("target_type").equals("10")){
                    open_fragment(new HomeFragment());
                }else {
                    if (SharedPrefManager.getInstance(MainActivity.this).getNotiyClicked()){
                        Bundle bundle = new Bundle();
                        bundle.putString("target_type",getIntent().getStringExtra("target_type"));
                        bundle.putString("input",getIntent().getStringExtra("input"));
                        bundle.putString("title",getIntent().getStringExtra("title"));
                        SharedPrefManager.getInstance(MainActivity.this).setNotiyClicked(false);
                        HomeFragment fragment = new HomeFragment();
                        fragment.setArguments(bundle);
                        open_fragment(fragment);
                    }else {
                        open_fragment(new HomeFragment());
                    }
                }
            }else {
                open_fragment(new HomeFragment());
            }
        }else {
            open_fragment(new HomeFragment());
        }


        if (getIntent().hasExtra("type")){
            if (getIntent().getStringExtra("type")!=null){
                if (getIntent().getStringExtra("type").equals("consulation")){
                    open_fragment(new MedicalConsulationFragment());
                }
            }
        }
    }

    private void click_record(){
        if (SharedPrefManager.getInstance(MainActivity.this).getLogged()){
            home_layout.setBackgroundDrawable(null);
            record_layout.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_red_rectangle));
            about_layout.setBackgroundDrawable(null);

            record_layout.startAnimation(fadeIn);

            home_text.setTextColor(getResources().getColor(R.color.grey));
            record_text.setTextColor(getResources().getColor(R.color.white));
            about_text.setTextColor(getResources().getColor(R.color.grey));

            home_icon.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.grey));
            record_icon.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.white));
            about_icon.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.grey));

            open_fragment(new MedicalRecordFragment());
        }else{
            clicked_tab = -1;
            Intent intent = new Intent(MainActivity.this,LoginActivity.class);
            startActivity(intent);
        }
    }

    private void click_about(){
        home_layout.setBackgroundDrawable(null);
        record_layout.setBackgroundDrawable(null);
        about_layout.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_red_rectangle));

        about_layout.startAnimation(fadeIn);

        home_text.setTextColor(getResources().getColor(R.color.grey));
        record_text.setTextColor(getResources().getColor(R.color.grey));
        about_text.setTextColor(getResources().getColor(R.color.white));

        home_icon.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.grey));
        record_icon.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.grey));
        about_icon.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.white));

        open_fragment(new AboutFragment());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void callUpdateTokenAPI(String token){
        UserAPIsClass.update_token(MainActivity.this,
                token,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        Log.i("firebase_token_update", "onResponse: "+"Failed");
                    }

                    @Override
                    public void onResponse(Object json) {
                        Log.i("firebase_token_update", "onResponse: "+"Success");
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Log.i("firebase_token_update", "onResponse: "+"No Internet");
                    }
                });
    }
}
