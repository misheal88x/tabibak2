package com.scit.tabibak.Models;

import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;

public class MedicalImageObject {
    @SerializedName("image_id") private int id = 0;
    @SerializedName("image_path") private String imagePath = "";
    @SerializedName("image_bitmap") private Bitmap imageBitmap;
    @SerializedName("image_url") private String imageUrl = "";

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Bitmap getImageBitmap() {
        return imageBitmap;
    }

    public void setImageBitmap(Bitmap imageBitmap) {
        this.imageBitmap = imageBitmap;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
