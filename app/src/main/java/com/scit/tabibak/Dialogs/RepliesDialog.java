package com.scit.tabibak.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.scit.tabibak.Adapters.RepliesAdapter;
import com.scit.tabibak.Models.Medical_consulation_models.ReplyObject;
import com.scit.tabibak.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class RepliesDialog extends AlertDialog {

    private Context context;
    private RecyclerView replies_recycler;
    private RepliesAdapter replies_adapter;
    private LinearLayoutManager replies_layout_manager;
    private List<ReplyObject> list;
    private String question;
    private TextView tv_question;

    public RepliesDialog(@NonNull Context context,List<ReplyObject> list,String question) {
        super(context);
        this.context = context;
        this.list = list;
        this.question = question;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_replies);

        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;

        init_views();
        init_events();
        init_dialog();
    }

    private void init_views() {
        tv_question = findViewById(R.id.question);
        replies_recycler = findViewById(R.id.replies_recycler);
    }

    private void init_events() {
    }

    private void init_dialog() {
        init_recycler();
        if (question.equals("")){
            tv_question.setVisibility(View.GONE);
        }else {
            tv_question.setText(question);
        }
    }

    private void init_recycler(){
        replies_adapter = new RepliesAdapter(context,list);
        replies_layout_manager = new LinearLayoutManager(context);
        replies_recycler.setLayoutManager(replies_layout_manager);
        replies_recycler.setAdapter(replies_adapter);
    }
}
