package com.scit.tabibak.Models.Medical_consulation_models;

import com.google.gson.annotations.SerializedName;
import com.scit.tabibak.Models.UserObject;

import java.util.ArrayList;
import java.util.List;

public class QestionObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("details") private String details = "";
    @SerializedName("approved") private int approved = 0;
    @SerializedName("created_at") private String created_at = "";
    @SerializedName("updated_at") private String updated_at = "";
    @SerializedName("user_id") private int user_id = 0;
    @SerializedName("replies") private List<ReplyObject> replies = new ArrayList<>();
    @SerializedName("owner") private UserObject owner = new UserObject();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getApproved() {
        return approved;
    }

    public void setApproved(int approved) {
        this.approved = approved;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public List<ReplyObject> getReplies() {
        return replies;
    }

    public void setReplies(List<ReplyObject> replies) {
        this.replies = replies;
    }

    public UserObject getOwner() {
        return owner;
    }

    public void setOwner(UserObject owner) {
        this.owner = owner;
    }
}
