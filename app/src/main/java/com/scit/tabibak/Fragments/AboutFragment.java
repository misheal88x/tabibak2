package com.scit.tabibak.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.scit.tabibak.APIsClasses.StartupAPIsClass;
import com.scit.tabibak.Bases.BaseFragment;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Models.Startup_models.HelpResponse;
import com.scit.tabibak.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import im.delight.android.webview.AdvancedWebView;

public class AboutFragment extends BaseFragment {

    //private WebView webView;
    private AdvancedWebView mWebView;
    private ProgressBar loading;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about,container,false);
    }

    @Override
    public void init_views() {
        //webView = base.findViewById(R.id.web_view);
        mWebView = base.findViewById(R.id.web_view);
        loading = base.findViewById(R.id.loading);
    }

    @Override
    public void init_events() {

    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        CallAboutAPI();
    }

    private void CallAboutAPI(){
        mWebView.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);
        StartupAPIsClass.get_about(base,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        mWebView.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        mWebView.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                        String j = new Gson().toJson(json);
                        HelpResponse success = new Gson().fromJson(j,HelpResponse.class);
                        if (success.getText()!=null){
                            mWebView.loadHtml(success.getText());
                            //BaseFunctions.setWebViewWithoutBody(webView,success.getText());
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        mWebView.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                        BaseFunctions.showErrorToast(base,getResources().getString(R.string.no_internet));
                    }
                });
    }
}
