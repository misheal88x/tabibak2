package com.scit.tabibak.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Interfaces.OnItemClickListener;
import com.scit.tabibak.Models.Medicine_models.MedicineObject;
import com.scit.tabibak.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MedicinesAdapter extends RecyclerView.Adapter<MedicinesAdapter.ViewHolder> {
    private Context context;
    private List<MedicineObject> list;


    private OnItemClickListener onItemClickListener;

    public MedicinesAdapter(Context context,List<MedicineObject> list,OnItemClickListener onItemClickListener) {
        this.context = context;
        this.list = list;
        this.onItemClickListener = onItemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;
        private TextView title,text;
        private ProgressBar loading;
        private RelativeLayout layout;

        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            title = view.findViewById(R.id.title);
            text = view.findViewById(R.id.text);
            loading = view.findViewById(R.id.loading);
            layout = view.findViewById(R.id.layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_medicine, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        MedicineObject m = list.get(position);

        holder.title.setText(m.getName());
        if (m.getSub_details().length()>25){
            holder.text.setText(m.getSub_details().substring(0,24).replaceAll("&nbsp;","")+"...");
        }else {
            holder.text.setText(m.getSub_details());
        }

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(position);
            }
        });
    }
}
