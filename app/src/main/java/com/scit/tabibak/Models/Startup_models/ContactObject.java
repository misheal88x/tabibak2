package com.scit.tabibak.Models.Startup_models;

import com.google.gson.annotations.SerializedName;

public class ContactObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("phone") private String phone = "";
    @SerializedName("contact_info") private String contact_info = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getContact_info() {
        return contact_info;
    }

    public void setContact_info(String contact_info) {
        this.contact_info = contact_info;
    }
}
