package com.scit.tabibak.Fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.service.autofill.SaveRequest;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.Gson;
import com.scit.tabibak.APIsClasses.MedicalDatesAPIsClass;
import com.scit.tabibak.APIsClasses.MedicalFilesAPIsClass;
import com.scit.tabibak.Activities.SplashActivity;
import com.scit.tabibak.Adapters.DatesAdapter;
import com.scit.tabibak.Adapters.MedicalFilesAdapter;
import com.scit.tabibak.Bases.BaseFragment;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Dialogs.ContactUsDialog;
import com.scit.tabibak.Dialogs.DateDetailsDialog;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Interfaces.OnItemClickListener;
import com.scit.tabibak.Models.MedicalFileObject;
import com.scit.tabibak.Models.Medical_dates_models.MedicalDateObject;
import com.scit.tabibak.Models.UserObject;
import com.scit.tabibak.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MedicalDatesFragment extends BaseFragment implements OnItemClickListener {

    private RelativeLayout toolbar;
    private TextView title;
    private RelativeLayout call,help,logout;

    private Button add_date;
    private LinearLayout add_date_layout;
    private EditText date_title,desc;
    private TextView date,time,add_date_btn;
    private ImageView minimize;

    private TextView added_dates_title;
    private RecyclerView dates_recycler;
    private List<MedicalDateObject> list;
    private DatesAdapter dates_adapter;
    private LinearLayoutManager dates_layout_manager;

    private Animation fadeIn,fadeOut;
    private ProgressBar loading,loading_add;
    private LinearLayout no_data,no_internet,error;

    private String selected_date = "",selected_time = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_medical_dates,container,false);
    }

    @Override
    public void init_views() {
        toolbar = base.findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        call = toolbar.findViewById(R.id.contact_us);
        help = toolbar.findViewById(R.id.help);
        logout = toolbar.findViewById(R.id.logout);
        if (!SharedPrefManager.getInstance(base).getLogged()){
            logout.setVisibility(View.GONE);
        }

        add_date = base.findViewById(R.id.add_date);
        add_date_layout = base.findViewById(R.id.add_date_layout);
        date_title = base.findViewById(R.id.date_title);
        desc = base.findViewById(R.id.desc);
        date = base.findViewById(R.id.date);
        time = base.findViewById(R.id.time);
        add_date_btn = base.findViewById(R.id.add_date_btn);
        minimize = base.findViewById(R.id.minimize);
        loading = base.findViewById(R.id.loading);
        loading_add = base.findViewById(R.id.loading_add);
        no_data = base.findViewById(R.id.no_data_layout);
        no_internet = base.findViewById(R.id.no_internet_layout);
        error = base.findViewById(R.id.error_layout);

        added_dates_title = base.findViewById(R.id.added_dates_title);
        dates_recycler = base.findViewById(R.id.dates_recycler);

        fadeIn = AnimationUtils.loadAnimation(base,R.anim.fade_in);
        fadeOut = AnimationUtils.loadAnimation(base,R.anim.fade_out);
    }

    @Override
    public void init_events() {
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactUsDialog dialog = new ContactUsDialog(base);
                dialog.show();
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base.open_fragment(new HelpFragment());
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(base);
                builder.setTitle(getResources().getString(R.string.logout_title));
                builder.setMessage(getResources().getString(R.string.logout_message));
                builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPrefManager.getInstance(base).setLogged(false);
                        SharedPrefManager.getInstance(base).setUser(new UserObject());
                        startActivity(new Intent(base, SplashActivity.class));
                        base.finish();
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
        add_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_date.setVisibility(View.GONE);
                add_date_layout.setVisibility(View.VISIBLE);
                add_date_layout.startAnimation(fadeIn);
                minimize.setVisibility(View.VISIBLE);
            }
        });
        minimize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                date_title.setText("");
                desc.setText("");
                date.setText(getResources().getString(R.string.medical_date_date));
                time.setText(getResources().getString(R.string.medical_date_time));
                selected_date = "";
                selected_time = "";
                add_date_layout.setVisibility(View.GONE);
                add_date_layout.startAnimation(fadeOut);
                add_date.setVisibility(View.VISIBLE);
                minimize.setVisibility(View.GONE);
            }
        });
        add_date_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (date_title.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.medical_date_no_title));
                    return;
                }
                if (desc.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.medical_date_no_desc));
                    return;
                }
                if (selected_date.equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.medical_date_no_date));
                    return;
                }
                if (selected_time.equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.medical_date_no_time));
                    return;
                }
                callAddDateAPI(date_title.getText().toString(),desc.getText().toString());
            }
        });
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar newCalendar = Calendar.getInstance();
                DatePickerDialog cal = new DatePickerDialog(base, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, month, day);

                        String year_str = new String();
                        String month_str = new String();
                        String day_str = new String();
                        year_str = String.valueOf(year);
                        if (month + 1 < 10) {
                            month_str = "0" + String.valueOf(month + 1);
                        } else {
                            month_str = String.valueOf(month + 1);
                        }
                        if (day < 10) {
                            day_str = "0" + String.valueOf(day);
                        } else {
                            day_str = String.valueOf(day);
                        }
                        String desDate = year_str + "-" + month_str + "-" + day_str;
                        selected_date = desDate;
                        date.setText(selected_date);
                    }

                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                cal.show();
            }
        });

        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar newCalendar = Calendar.getInstance();
                TimePickerDialog time_picker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hour, int minute) {
                        String hour_str = new String();
                        String minute_str = new String();
                        if (hour<10){
                            hour_str = "0"+String.valueOf(hour);
                        }else {
                            hour_str = String.valueOf(hour);
                        }
                        if (minute<10){
                            minute_str = "0"+String.valueOf(minute);
                        }else {
                            minute_str = String.valueOf(minute);
                        }
                        String fullTime = hour_str+":"+minute_str+":00";

                        selected_time = fullTime;
                        time.setText(selected_time);
                    }
                },newCalendar.get(Calendar.HOUR_OF_DAY),newCalendar.get(Calendar.MINUTE), DateFormat.is24HourFormat(base));
                time_picker.show();
            }
        });
        error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callGetDatesAPI();
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        title.setText(getResources().getString(R.string.record_medical_dates));
        init_recycler();
    }

    private void init_recycler(){
        list = new ArrayList<>();
        dates_adapter = new DatesAdapter(base,list,this);
        dates_layout_manager = new LinearLayoutManager(base);
        dates_recycler.setLayoutManager(dates_layout_manager);
        dates_recycler.setAdapter(dates_adapter);
        callGetDatesAPI();
    }

    @Override
    public void onItemClick(int position) {
        DateDetailsDialog dialog = new DateDetailsDialog(base,list.get(position));
        dialog.show();
    }

    private void callGetDatesAPI(){
        loading.setVisibility(View.VISIBLE);
        added_dates_title.setVisibility(View.GONE);
        list.clear();
        dates_adapter = new DatesAdapter(base,list,this);
        dates_recycler.setAdapter(dates_adapter);
        no_data.setVisibility(View.GONE);
        error.setVisibility(View.GONE);
        no_internet.setVisibility(View.GONE);
        MedicalDatesAPIsClass.get_all(base,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        loading.setVisibility(View.GONE);
                        error.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        loading.setVisibility(View.GONE);
                        String j = new Gson().toJson(json);
                        try {
                            MedicalDateObject[] success = new Gson().fromJson(j,MedicalDateObject[].class);
                            if (success.length>0){
                                added_dates_title.setVisibility(View.VISIBLE);
                                for (MedicalDateObject o : success){
                                    list.add(o);
                                    dates_adapter.notifyDataSetChanged();
                                }
                            }else {
                                added_dates_title.setVisibility(View.GONE);
                                no_data.setVisibility(View.VISIBLE);
                            }
                        }catch (Exception e){
                            callGetDatesAPI();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        loading.setVisibility(View.GONE);
                        no_internet.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void callAddDateAPI(String title,String details){
        showAddLoading(true);
        MedicalDatesAPIsClass.add_date(
                base,
                title,
                details,
                selected_date,
                selected_time,
                0,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        showAddLoading(false);
                    }

                    @Override
                    public void onResponse(Object json) {
                        showAddLoading(false);
                        BaseFunctions.showSuccessToast(base, getResources().getString(R.string.medical_date_success));
                        date_title.setText("");
                        desc.setText("");
                        date.setText(getResources().getString(R.string.medical_date_date));
                        time.setText(getResources().getString(R.string.medical_date_time));
                        selected_date = "";
                        selected_time = "";
                        add_date_layout.setVisibility(View.GONE);
                        add_date_layout.startAnimation(fadeOut);
                        add_date.setVisibility(View.VISIBLE);
                        minimize.setVisibility(View.GONE);
                        BaseFunctions.playMusic(base, R.raw.added);
                        callGetDatesAPI();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        showAddLoading(false);
                        BaseFunctions.showErrorToast(base,getResources().getString(R.string.no_internet));
                    }
                });
    }

    private void showAddLoading(boolean state){
        if (state){
            loading_add.setVisibility(View.VISIBLE);
            add_date_btn.setVisibility(View.GONE);
            title.setEnabled(false);
            desc.setEnabled(false);
            date.setEnabled(false);
            time.setEnabled(false);
        }else {
            loading_add.setVisibility(View.GONE);
            add_date_btn.setVisibility(View.VISIBLE);
            title.setEnabled(true);
            desc.setEnabled(true);
            date.setEnabled(true);
            time.setEnabled(true);
        }
    }
}
