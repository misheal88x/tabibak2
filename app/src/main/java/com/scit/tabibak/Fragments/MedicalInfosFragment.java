package com.scit.tabibak.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scit.tabibak.APIs.MedicalInfosAPIs;
import com.scit.tabibak.APIsClasses.MedicalInfosAPIsClass;
import com.scit.tabibak.APIsClasses.MedicinesAPIsClass;
import com.scit.tabibak.Activities.SplashActivity;
import com.scit.tabibak.Adapters.InforsAdapter;
import com.scit.tabibak.Bases.BaseFragment;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Dialogs.ContactUsDialog;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Interfaces.OnItemClickListener;
import com.scit.tabibak.Models.Medical_info_models.MedicalInfoObject;
import com.scit.tabibak.Models.Medical_info_models.MedicalInfosResponse;
import com.scit.tabibak.Models.Medicine_models.MedicineObject;
import com.scit.tabibak.Models.Medicine_models.MedicinesResponse;
import com.scit.tabibak.Models.UserObject;
import com.scit.tabibak.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MedicalInfosFragment extends BaseFragment implements OnItemClickListener {

    private RelativeLayout toolbar;
    private TextView title;
    private RelativeLayout call,help,logout;

    private NestedScrollView scrollView;

    private RecyclerView recycler;
    private InforsAdapter adapter;
    private List<MedicalInfoObject> list;
    private LinearLayoutManager layoutManager;

    private LinearLayout no_data,no_internet,error;
    private ProgressBar loading,load_more;

    private int currentPage = 1;
    private boolean continue_paginate = true;
    private int per_page = 20;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_medical_infos,container,false);
    }

    @Override
    public void init_views() {
        toolbar = base.findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        call = toolbar.findViewById(R.id.contact_us);
        help = toolbar.findViewById(R.id.help);
        logout = toolbar.findViewById(R.id.logout);
        if (!SharedPrefManager.getInstance(base).getLogged()){
            logout.setVisibility(View.GONE);
        }

        no_data = base.findViewById(R.id.no_data_layout);
        no_internet = base.findViewById(R.id.no_internet_layout);
        error = base.findViewById(R.id.error_layout);
        loading = base.findViewById(R.id.loading);

        recycler = base.findViewById(R.id.infos);
        scrollView = base.findViewById(R.id.layout);
        load_more = base.findViewById(R.id.load_more);
    }

    @Override
    public void init_events() {
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(base);
                builder.setTitle(getResources().getString(R.string.logout_title));
                builder.setMessage(getResources().getString(R.string.logout_message));
                builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPrefManager.getInstance(base).setLogged(false);
                        SharedPrefManager.getInstance(base).setUser(new UserObject());
                        startActivity(new Intent(base, SplashActivity.class));
                        base.finish();
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
        error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callInfosAPI(currentPage,0);
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactUsDialog dialog = new ContactUsDialog(base);
                dialog.show();
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base.open_fragment(new HelpFragment());
            }
        });
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (list.size()>=per_page){
                            if (continue_paginate){
                                currentPage++;
                                callInfosAPI(currentPage,1);
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        title.setText(getResources().getString(R.string.home_medical_info));
        init_recycler();
    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new InforsAdapter(base,list,this);
        layoutManager = new LinearLayoutManager(base);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);
        callInfosAPI(currentPage,0);
    }

    @Override
    public void onItemClick(int position) {
        currentPage = 1;
        continue_paginate = true;
        Bundle bundle = new Bundle();
        bundle.putSerializable("info",list.get(position));
        InfoDetailsFragment fragment = new InfoDetailsFragment();
        fragment.setArguments(bundle);
        base.open_fragment(fragment);
    }

    private void callInfosAPI(final int page, final int type){
        if (type == 1){
            load_more.setVisibility(View.VISIBLE);
        }else {
            loading.setVisibility(View.VISIBLE);
            no_data.setVisibility(View.GONE);
            error.setVisibility(View.GONE);
            no_internet.setVisibility(View.GONE);
        }
        MedicalInfosAPIsClass.get_all(
                base,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        try {
                            MedicalInfosResponse success = new Gson().fromJson(j, MedicalInfosResponse.class);
                            if (success.getData() != null) {
                                if (success.getData().size() > 0) {
                                    process_data(type);
                                    per_page = success.getPer_page();
                                    for (MedicalInfoObject m : success.getData()) {
                                        list.add(m);
                                        adapter.notifyDataSetChanged();
                                    }
                                } else {
                                    no_data(type);
                                }
                            } else {
                                error_happend(type);
                            }
                        }catch (Exception e){
                            if (type == 0){
                                callInfosAPI(page,type);
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        no_internet(type);
                    }
                });
    }

    private void error_happend(int type){
        if (type == 1){
            load_more.setVisibility(View.GONE);
        }else {
            loading.setVisibility(View.GONE);
            error.setVisibility(View.VISIBLE);
            no_data.setVisibility(View.GONE);
            no_internet.setVisibility(View.GONE);
        }
    }
    private void process_data(int type){
        if (type == 0){
            loading.setVisibility(View.GONE);
            error.setVisibility(View.GONE);
            no_data.setVisibility(View.GONE);
            no_internet.setVisibility(View.GONE);
        }else {
            load_more.setVisibility(View.GONE);
        }
    }
    private void no_data(int type){
        if (type == 0){
            loading.setVisibility(View.GONE);
            no_data.setVisibility(View.VISIBLE);
        }else {
            load_more.setVisibility(View.GONE);
            BaseFunctions.showWarningToast(base,getResources().getString(R.string.no_more));
            continue_paginate = false;
        }
    }

    private void no_internet(int type){
        if (type == 0){
            loading.setVisibility(View.GONE);
            no_internet.setVisibility(View.VISIBLE);
        }else {
            load_more.setVisibility(View.GONE);
            BaseFunctions.showErrorToast(base,getResources().getString(R.string.no_internet));
        }
    }
}
