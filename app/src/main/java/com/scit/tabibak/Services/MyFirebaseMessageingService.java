package com.scit.tabibak.Services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.scit.tabibak.APIsClasses.UserAPIsClass;
import com.scit.tabibak.Activities.MainActivity;
import com.scit.tabibak.Activities.NotificationDetailsActivity;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

public class MyFirebaseMessageingService extends FirebaseMessagingService {
    final int MY_NOTIFICATION_ID = 1;
    final String NOTIFICATION_CHANNEL_ID = "10001";
    NotificationManager notificationManager;
    NotificationCompat.Builder builder;
    //final Context context = this;

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        String recent_token = FirebaseInstanceId.getInstance().getToken();
        Log.e("newToken1",recent_token);
        if (SharedPrefManager.getInstance(MyFirebaseMessageingService.this).getLogged()){
            callUpdateTokenAPI(recent_token);
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            Map<String, String> params = remoteMessage.getData();
            JSONObject jsonObject = new JSONObject(params);
            Log.i("JSON_OBJECTT", "the : " + jsonObject.toString());
            sendNotification(jsonObject);
        }catch (Exception e){
            Log.i("JSON_OBJECTT", "onMessageReceived: "+e.getMessage());
        }
    }
    private void sendNotification(JSONObject jsonObject) {

        String content = "";
        String target_type = "";
        String target_id = "";
        String target_title = "";
        String target_user = "";

        try {
            if (jsonObject.has("msg_title"))
            content = jsonObject.getString("msg_title");
            if (jsonObject.has("target_type"))
            target_type = jsonObject.getString("target_type");
            if (jsonObject.has("target_id"))
            target_id = jsonObject.getString("target_id");
            if (jsonObject.has("target_title"))
            target_title = jsonObject.getString("target_title");
            if (jsonObject.has("target_user"))
            target_user = jsonObject.getString("target_user");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (SharedPrefManager.getInstance(this).getLogged()) {
            if (target_user.equals(String.valueOf(SharedPrefManager.getInstance(this).getUser().getId()))) {
                switch (target_type) {
                    //A doctor replies to a customer's question
                    case "1": {
                        Intent intent = new Intent(getApplicationContext(), NotificationDetailsActivity.class);
                        intent.putExtra("target_type", target_type);
                        intent.putExtra("target_id", target_id);
                        intent.putExtra("target_title", target_title);
                        PendingIntent pendingIntent = PendingIntent.getActivity(
                                getApplicationContext(),
                                1,
                                intent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                                        | PendingIntent.FLAG_ONE_SHOT);
                        BaseFunctions.showNotification(getApplicationContext(), pendingIntent, content, builder, notificationManager, NOTIFICATION_CHANNEL_ID, MY_NOTIFICATION_ID, target_type);

                    }
                    break;
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                    case "6":
                    //case "7":
                    case "8":
                    case "9":
                    case "10":
                    case "11":{
                        Intent intent = new Intent(getApplicationContext(), NotificationDetailsActivity.class);
                        intent.putExtra("target_type", target_type);
                        intent.putExtra("target_id", target_id);
                        intent.putExtra("target_title", "");
                        PendingIntent pendingIntent = PendingIntent.getActivity(
                                getApplicationContext(),
                                1,
                                intent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                                        | PendingIntent.FLAG_ONE_SHOT);
                        BaseFunctions.showNotification(getApplicationContext(), pendingIntent, content, builder, notificationManager, NOTIFICATION_CHANNEL_ID, MY_NOTIFICATION_ID, target_type);
                    }
                    break;
                }
            }
        }
    }

    private void callUpdateTokenAPI(String token){
        UserAPIsClass.update_token(MyFirebaseMessageingService.this,
                token,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        Log.i("firebase_token_update", "onResponse: "+"Failed");
                    }

                    @Override
                    public void onResponse(Object json) {
                        Log.i("firebase_token_update", "onResponse: "+"Success");
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Log.i("firebase_token_update", "onResponse: "+"No Internet");
                    }
                });
    }
}
