package com.scit.tabibak.Models.Startup_models;

import com.google.gson.annotations.SerializedName;

public class SpecializationObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("name") private String name = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
