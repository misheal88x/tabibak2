package com.scit.tabibak.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.scit.tabibak.APIsClasses.DiagnosisAPIsClass;
import com.scit.tabibak.APIsClasses.MedicalCosulationAPIsClass;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.R;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;

public class AddDiagnosisDialog extends AlertDialog {
    private Context context;
    private int id;
    private EditText text,status,medicines;
    private ProgressBar loading;
    private TextView add;

    public AddDiagnosisDialog(@NonNull Context context, int id) {
        super(context);
        this.context = context;
        this.id = id;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_add_diagnosis);

        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

        init_views();
        init_events();
        init_dialog();
    }

    private void init_views() {
        text = findViewById(R.id.text);
        status = findViewById(R.id.status);
        medicines = findViewById(R.id.medicines);
        loading = findViewById(R.id.loading);
        add = findViewById(R.id.add);
    }

    private void init_events() {
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(context,context.getResources().getString(R.string.add_diagnosis_no_status));
                    return;
                }
                if (text.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(context,context.getResources().getString(R.string.add_diagnosis_no_text));
                    return;
                }
                if (medicines.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(context,context.getResources().getString(R.string.add_diagnosis_no_medicines));
                    return;
                }
                callAddReplyAPI();
            }
        });
    }

    private void init_dialog() {

    }

    private void callAddReplyAPI(){
        loading.setVisibility(View.VISIBLE);
        add.setVisibility(View.GONE);
        DiagnosisAPIsClass.add(context,
                id,
                status.getText().toString(),
                text.getText().toString(),
                medicines.getText().toString(),
                new IResponse() {
                    @Override
                    public void onResponse() {
                        loading.setVisibility(View.GONE);
                        add.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        loading.setVisibility(View.GONE);
                        add.setVisibility(View.VISIBLE);
                        BaseFunctions.showSuccessToast(context,context.getResources().getString(R.string.add_diagnosis_success));
                        cancel();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        loading.setVisibility(View.GONE);
                        add.setVisibility(View.VISIBLE);
                        BaseFunctions.showErrorToast(context,context.getResources().getString(R.string.no_internet));
                    }
                });
    }
}
