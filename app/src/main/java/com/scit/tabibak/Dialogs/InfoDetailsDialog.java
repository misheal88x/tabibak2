package com.scit.tabibak.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Interfaces.ILoadImage;
import com.scit.tabibak.Models.MedicalFileObject;
import com.scit.tabibak.Models.Medical_info_models.MedicalInfoObject;
import com.scit.tabibak.Models.Startup_models.SpecializationObject;
import com.scit.tabibak.R;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;
import de.hdodenhof.circleimageview.CircleImageView;
import im.delight.android.webview.AdvancedWebView;

public class InfoDetailsDialog extends AlertDialog {
    private Context context;
    private MedicalInfoObject info;
    private TextView title,name,profession;
    private AdvancedWebView web_view;
    private CircleImageView icon;
    private ImageView image;
    private ProgressBar loading;

    public InfoDetailsDialog(@NonNull Context context, MedicalInfoObject info) {
        super(context);
        this.context = context;
        this.info = info;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_info_details);

        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;

        init_views();
        init_events();
        init_dialog();
    }

    private void init_views() {
        title = findViewById(R.id.title);
        name = findViewById(R.id.name);
        profession = findViewById(R.id.profession);
        web_view = findViewById(R.id.web_view);
        icon = findViewById(R.id.icon);
        image = findViewById(R.id.image);
        loading = findViewById(R.id.loading);
    }

    private void init_events() {
    }

    private void init_dialog() {
        title.setText(info.getName());
        BaseFunctions.setGlideImage(context,icon,info.getOwner().getAvatar());
        name.setText(info.getOwner().getName());
        if (SharedPrefManager.getInstance(context).getSpes().size()>0){
            for (SpecializationObject s : SharedPrefManager.getInstance(context).getSpes()){
                if (s.getId() == info.getOwner().getSpecialization_id()){
                    profession.setText(s.getName());
                    break;
                }
            }
        }
        web_view.loadHtml(info.getDetails());


        loading.setVisibility(View.VISIBLE);
        BaseFunctions.setGlideImageWithEvents(context, image, info.getImage(), new ILoadImage() {
            @Override
            public void onLoaded() {
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailed() {
                loading.setVisibility(View.GONE);
            }
        });
    }
}
