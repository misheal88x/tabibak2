package com.scit.tabibak.Fragments;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.scit.tabibak.APIsClasses.HealthRecordAPIsClass;
import com.scit.tabibak.Activities.SplashActivity;
import com.scit.tabibak.Adapters.HealthNotesAdapter;
import com.scit.tabibak.Adapters.MedicalImagesAdapter;
import com.scit.tabibak.Adapters.MyHealthRecordTextsAdapter;
import com.scit.tabibak.Bases.BaseFragment;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Dialogs.ContactUsDialog;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Interfaces.OnItemClickListener;
import com.scit.tabibak.Models.Health_record_models.BasicHealthInfo;
import com.scit.tabibak.Models.Health_record_models.ChronicDiseaseObject;
import com.scit.tabibak.Models.Health_record_models.ContinueMedicinesObject;
import com.scit.tabibak.Models.Health_record_models.HealthMedicalImageObject;
import com.scit.tabibak.Models.Health_record_models.MyHealthRecordObject;
import com.scit.tabibak.Models.Health_record_models.PreviousDiseaseObject;
import com.scit.tabibak.Models.MedicalImageObject;
import com.scit.tabibak.Models.Medical_notes_models.MedicalNoteObject;
import com.scit.tabibak.Models.UserObject;
import com.scit.tabibak.R;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.app.Activity.RESULT_OK;

public class EditMedicalRecordFragment extends BaseFragment implements  BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.ImageLoaderDelegate {

    private RelativeLayout toolbar;
    private TextView title;
    private RelativeLayout call,help,logout;

    private EditText my_name,my_age,my_weight,my_height;
    private CheckBox chk_blood_pressure,chk_heart,chk_diabetes,chk_tumors,chk_malignant,with_alarm;
    private Button add_previous_disease,add_chronic_diseases,add_continue_medicine,add_medical_image;
    private LinearLayout previous_disease_layout,chronic_diseases_layout,continue_medicine_layout,medical_image_layout;
    private EditText previous_disease_name,chronic_diseases_name,continue_medicine_name;
    private TextView previous_disease_add,chronic_diseases_add,continue_medicine_add;
    private RecyclerView previous_disease_recycler,chronic_diseases_recycler,continue_medicine_recycler,medical_images_recycler;
    private List<String> previous_disease_list,chronic_diseases_list,continue_medicine_list;
    private MyHealthRecordTextsAdapter previous_disease_adapter,chronic_diseases_adapter,continue_medicine_adapter;
    private MedicalImagesAdapter images_adapter;
    private LinearLayoutManager previous_disease_layout_manager,chronic_diseases_layout_manager,continue_medicine_layout_manager
            ,images_layout_manager;
    private List<MedicalImageObject> images_list;
    private String TEMP_PHOTO_FILE = "temporary_holder.jpg";
    private int clicked_image_type = 0;

    private Animation fadeIn,fadeOut;
    private LinearLayout no_internet,error;

    private LinearLayout health_record_layout;
    private ProgressBar loading_base_data,loading_save_record;
    private TextView save_health_record;

    private TextView previous_diseases_title,medical_images_title,chronic_diseases_title,continue_medicine_title;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_edit_medical_record_fragment,container,false);
    }

    @Override
    public void init_views() {
        //Toolbar
        toolbar = base.findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        call = toolbar.findViewById(R.id.contact_us);
        help = toolbar.findViewById(R.id.help);
        logout = toolbar.findViewById(R.id.logout);
        if (!SharedPrefManager.getInstance(base).getLogged()){
            logout.setVisibility(View.GONE);
        }

        //EditText
        my_name = base.findViewById(R.id.name);
        my_age = base.findViewById(R.id.age);
        my_weight = base.findViewById(R.id.weight);
        my_height = base.findViewById(R.id.height);
        previous_disease_name = base.findViewById(R.id.previous_disease_name);
        chronic_diseases_name = base.findViewById(R.id.chronic_diseases_name);
        continue_medicine_name = base.findViewById(R.id.continue_medicine_name);
        //Checkbox
        chk_blood_pressure = base.findViewById(R.id.blood_pressure);
        chk_heart = base.findViewById(R.id.heart);
        chk_diabetes = base.findViewById(R.id.diabetes);
        chk_tumors = base.findViewById(R.id.tumors);
        chk_malignant = base.findViewById(R.id.malignant);
        with_alarm = base.findViewById(R.id.with_alarm);
        //Button
        add_previous_disease = base.findViewById(R.id.add_previous_disease);
        add_chronic_diseases = base.findViewById(R.id.add_chronic_diseases);
        add_continue_medicine = base.findViewById(R.id.add_continue_medicine);
        add_medical_image = base.findViewById(R.id.add_medical_image);
        //LinearLayout
        previous_disease_layout = base.findViewById(R.id.add_previous_disease_layout);
        chronic_diseases_layout  = base.findViewById(R.id.add_chronic_diseases_layout);
        continue_medicine_layout = base.findViewById(R.id.add_continue_medicine_layout);
        medical_image_layout = base.findViewById(R.id.add_medical_image_layout);
        no_internet = base.findViewById(R.id.no_internet_layout);
        error = base.findViewById(R.id.error_layout);
        health_record_layout = base.findViewById(R.id.health_record_layout);
        //TextView
        previous_disease_add = base.findViewById(R.id.previous_disease_add);
        chronic_diseases_add = base.findViewById(R.id.chronic_diseases_add);
        continue_medicine_add = base.findViewById(R.id.continue_medicine_add);
        save_health_record = base.findViewById(R.id.save_health_record);
        previous_diseases_title = base.findViewById(R.id.previous_diseases_title);
        medical_images_title = base.findViewById(R.id.previous_diseases_title);
        chronic_diseases_title = base.findViewById(R.id.previous_diseases_title);
        continue_medicine_title = base.findViewById(R.id.previous_diseases_title);
        //RecyclerView
        previous_disease_recycler = base.findViewById(R.id.previous_diseases_recycler);
        chronic_diseases_recycler = base.findViewById(R.id.chronic_diseases_recycler);
        continue_medicine_recycler = base.findViewById(R.id.continue_medicine_recycler);
        medical_images_recycler = base.findViewById(R.id.medical_images_recycler);
        //Animation
        fadeIn = AnimationUtils.loadAnimation(base,R.anim.fade_in);
        fadeOut = AnimationUtils.loadAnimation(base,R.anim.fade_out);
        //ProgressBars
        loading_base_data = base.findViewById(R.id.loading_base_data);
        loading_save_record = base.findViewById(R.id.loading_save_record);
    }

    @Override
    public void init_events() {
        add_previous_disease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_previous_disease.setVisibility(View.GONE);
                previous_disease_layout.setVisibility(View.VISIBLE);
                previous_disease_layout.startAnimation(fadeIn);
            }
        });
        add_continue_medicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_continue_medicine.setVisibility(View.GONE);
                continue_medicine_layout.setVisibility(View.VISIBLE);
                continue_medicine_layout.startAnimation(fadeIn);
            }
        });
        add_chronic_diseases.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_chronic_diseases.setVisibility(View.GONE);
                chronic_diseases_layout.setVisibility(View.VISIBLE);
                chronic_diseases_layout.startAnimation(fadeIn);
            }
        });
        add_medical_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicked_image_type = 1;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE},1000);
            }
        });
        previous_disease_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //add_previous_disease.setVisibility(View.VISIBLE);
                //previous_disease_layout.setVisibility(View.GONE);
                if (previous_disease_name.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.health_record_previous_disease_empty));
                    return;
                }
                previous_disease_list.add(previous_disease_name.getText().toString());
                previous_disease_adapter.notifyDataSetChanged();
                previous_disease_name.setText("");
            }
        });
        continue_medicine_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //add_continue_medicine.setVisibility(View.VISIBLE);
                //continue_medicine_layout.setVisibility(View.GONE);
                if (continue_medicine_name.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.health_record_continue_medicine_empty));
                    return;
                }
                continue_medicine_list.add(continue_medicine_name.getText().toString());
                continue_medicine_adapter.notifyDataSetChanged();
                continue_medicine_name.setText("");
            }
        });
        chronic_diseases_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //add_chronic_diseases.setVisibility(View.VISIBLE);
                //chronic_diseases_layout.setVisibility(View.GONE);
                if (chronic_diseases_name.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.health_record_chronic_disease_empty));
                    return;
                }
                chronic_diseases_list.add(chronic_diseases_name.getText().toString());
                chronic_diseases_adapter.notifyDataSetChanged();
                chronic_diseases_name.setText("");
            }
        });
        error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callHealthRecordAPI();
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactUsDialog dialog = new ContactUsDialog(base);
                dialog.show();
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base.open_fragment(new HelpFragment());
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(base);
                builder.setTitle(getResources().getString(R.string.logout_title));
                builder.setMessage(getResources().getString(R.string.logout_message));
                builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPrefManager.getInstance(base).setLogged(false);
                        SharedPrefManager.getInstance(base).setUser(new UserObject());
                        startActivity(new Intent(base, SplashActivity.class));
                        base.finish();
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
        save_health_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (my_name.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.health_record_no_name));
                    return;
                }
                if (my_age.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.health_record_no_age));
                    return;
                }
                if (my_weight.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.health_record_no_weight));
                    return;
                }
                if (my_height.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.health_record_no_height));
                    return;
                }
                callSaveRecordAPI();
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        title.setText(getResources().getString(R.string.record_update_title));
        init_recyclers();
        callHealthRecordAPI();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1000){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
                    ImagePicker.Companion.with(EditMedicalRecordFragment.this)
                            .galleryOnly()
                            .crop(16f,9f)	    			//Crop image(Optional), Check Customization for more option
                            .compress(1024)			//Final image size will be less than 1 MB(Optional)
                            .start();
                }else {
                    BSImagePicker singleSelectionPicker = new BSImagePicker.Builder("com.scit.tabibak.provider")
                            .hideCameraTile() //Default: show. Set this if you don't want user to take photo.
                            .hideGalleryTile() //Default: show. Set this if you don't want to further let user select from a gallery app. In such case, I suggest you to set maximum displaying images to Integer.MAX_VALUE.
                            .build();
                    singleSelectionPicker.show(getChildFragmentManager(),"Picker");
                }

            }else {
                BaseFunctions.showErrorToast(base,getResources().getString(R.string.no_permission));
            }
        }
    }

    private File getTempFile() {
        if (isSDCARDMounted()) {
            int oneTimeID = (int) SystemClock.uptimeMillis();
            TEMP_PHOTO_FILE = oneTimeID+"_"+"temporary_holder.jpg";
            File f = new File(Environment.getExternalStorageDirectory(),TEMP_PHOTO_FILE);
            try {
                f.createNewFile();
            } catch (IOException e) {

            }
            return f;
        } else {
            return null;
        }
    }

    private boolean isSDCARDMounted(){
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED))
            return true;
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Crop.REQUEST_CROP:{
                handle_crop(resultCode,data);
            }break;
            default:{
                if (resultCode == Activity.RESULT_OK) {
                    String filePath = ImagePicker.Companion.getFilePath(data);
                    if (clicked_image_type == 1){
                        MedicalImageObject o = new MedicalImageObject();
                        o.setImagePath(filePath);
                        Bitmap myBitmap = BitmapFactory.decodeFile(filePath);
                        o.setImageBitmap(myBitmap);
                        images_list.add(o);
                        images_adapter.notifyDataSetChanged();
                    }
                }
            }
        }
    }

    private void handle_crop(int code, final Intent data){
        if (code == RESULT_OK){
            base.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String filePath=  Environment.getExternalStorageDirectory()
                            + "/"+TEMP_PHOTO_FILE;
                    if (new File(filePath).exists()){
                        if (clicked_image_type == 1){
                            MedicalImageObject o = new MedicalImageObject();
                            o.setImagePath(filePath);
                            Bitmap myBitmap = BitmapFactory.decodeFile(filePath);
                            o.setImageBitmap(myBitmap);
                            images_list.add(o);
                            images_adapter.notifyDataSetChanged();
                        }
                    }

                }
            });
        }
    }
    private void init_recyclers(){
        images_list = new ArrayList<>();
        previous_disease_list = new ArrayList<>();
        chronic_diseases_list = new ArrayList<>();
        continue_medicine_list = new ArrayList<>();

        previous_disease_adapter = new MyHealthRecordTextsAdapter(base,previous_disease_list,true);
        chronic_diseases_adapter = new MyHealthRecordTextsAdapter(base,chronic_diseases_list,true);
        continue_medicine_adapter = new MyHealthRecordTextsAdapter(base,continue_medicine_list,true);
        images_adapter = new MedicalImagesAdapter(base,images_list);

        previous_disease_layout_manager = new LinearLayoutManager(base);
        chronic_diseases_layout_manager = new LinearLayoutManager(base);
        continue_medicine_layout_manager = new LinearLayoutManager(base);
        images_layout_manager = new LinearLayoutManager(base,LinearLayoutManager.HORIZONTAL,false);

        previous_disease_recycler.setLayoutManager(previous_disease_layout_manager);
        chronic_diseases_recycler.setLayoutManager(chronic_diseases_layout_manager);
        continue_medicine_recycler.setLayoutManager(continue_medicine_layout_manager);
        medical_images_recycler.setLayoutManager(images_layout_manager);

        previous_disease_recycler.setAdapter(previous_disease_adapter);
        chronic_diseases_recycler.setAdapter(chronic_diseases_adapter);
        continue_medicine_recycler.setAdapter(continue_medicine_adapter);
        medical_images_recycler.setAdapter(images_adapter);
    }

    private void callHealthRecordAPI(){
        health_record_layout.setVisibility(View.GONE);
        loading_base_data.setVisibility(View.VISIBLE);
        HealthRecordAPIsClass.get_my_record(base,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        health_record_layout.setVisibility(View.VISIBLE);
                        loading_base_data.setVisibility(View.GONE);
                        error.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        loading_base_data.setVisibility(View.GONE);
                        error.setVisibility(View.GONE);
                        no_internet.setVisibility(View.GONE);
                        String j = new Gson().toJson(json);
                        MyHealthRecordObject success = new Gson().fromJson(j,MyHealthRecordObject.class);
                        //Basic info
                        if (success.getMedical_reports()!=null){
                            if (success.getMedical_reports().size()>0){
                                BasicHealthInfo b = success.getMedical_reports().get(0);
                                my_name.setText(b.getName());
                                my_age.setText(b.getAge());
                                my_height.setText(b.getTall());
                                my_weight.setText(b.getWeight());
                                String[] arr = new Gson().fromJson(b.getDisease_id(),String[].class);
                                if (arr.length>0){
                                    for (String s : arr){
                                        switch (s){
                                            case "1":{
                                                chk_blood_pressure.setChecked(true);
                                            }break;
                                            case "2":{
                                                chk_heart.setChecked(true);
                                            }break;
                                            case "3":{
                                                chk_diabetes.setChecked(true);
                                            }break;
                                            case "4":{
                                                chk_tumors.setChecked(true);
                                            }break;
                                            case "5":{
                                                chk_malignant.setChecked(true);
                                            }break;
                                        }
                                    }
                                }
                            }else {
                                error.setVisibility(View.VISIBLE);
                            }
                        }else {
                            error.setVisibility(View.VISIBLE);
                        }
                        //Previous Diseases
                        if (success.getPrecedent()!=null){
                            if (success.getPrecedent().size()>0){
                                for (PreviousDiseaseObject p : success.getPrecedent()){
                                    previous_disease_list.add(p.getName());
                                    previous_disease_adapter.notifyDataSetChanged();
                                }
                            }else {
                                //previous_diseases_title.setVisibility(View.GONE);
                                //previous_disease_recycler.setVisibility(View.GONE);
                            }
                        }else {
                            //previous_diseases_title.setVisibility(View.GONE);
                            //previous_disease_recycler.setVisibility(View.GONE);
                        }
                        //Medical Images
                        if (success.getMedical_images()!=null){
                            if (success.getMedical_images().size()>0){
                                for (HealthMedicalImageObject h : success.getMedical_images()){
                                    MedicalImageObject m = new MedicalImageObject();
                                    m.setImageUrl(h.getImage());
                                    m.setId(h.getId());
                                    images_list.add(m);
                                    images_adapter.notifyDataSetChanged();
                                }
                            }else {
                                //medical_images_title.setVisibility(View.GONE);
                                //medical_images_recycler.setVisibility(View.GONE);
                            }
                        }else {
                            //medical_images_title.setVisibility(View.GONE);
                            //medical_images_recycler.setVisibility(View.GONE);
                        }
                        //Chronic Diseases
                        if (success.getGenetic()!=null){
                            if (success.getGenetic().size()>0){
                                for (ChronicDiseaseObject o : success.getGenetic()){
                                    chronic_diseases_list.add(o.getName());
                                    chronic_diseases_adapter.notifyDataSetChanged();
                                }
                            }else {
                                //chronic_diseases_title.setVisibility(View.GONE);
                                //chronic_diseases_recycler.setVisibility(View.GONE);
                            }
                        }else {
                            //chronic_diseases_title.setVisibility(View.GONE);
                            //chronic_diseases_recycler.setVisibility(View.GONE);
                        }
                        //Medicines
                        if (success.getMedicines()!=null){
                            if (success.getMedicines().size()>0){
                                for (ContinueMedicinesObject c : success.getMedicines()){
                                    continue_medicine_list.add(c.getName());
                                    continue_medicine_adapter.notifyDataSetChanged();
                                }
                            }else {
                                //continue_medicine_title.setVisibility(View.GONE);
                                //continue_medicine_recycler.setVisibility(View.GONE);
                            }
                        }else {
                            //continue_medicine_title.setVisibility(View.GONE);
                            //continue_medicine_recycler.setVisibility(View.GONE);
                        }
                        health_record_layout.setVisibility(View.VISIBLE);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        health_record_layout.setVisibility(View.VISIBLE);
                        loading_base_data.setVisibility(View.GONE);
                        no_internet.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void callSaveRecordAPI(){
        String name_text = my_name.getText().toString();
        String age_text = my_age.getText().toString();
        String weight_text = my_weight.getText().toString();
        String height_text = my_height.getText().toString();

        List<String> diseases_list = new ArrayList<>();
        if (chk_blood_pressure.isChecked()){
            diseases_list.add("1");
        }
        if (chk_diabetes.isChecked()){
            diseases_list.add("3");
        }
        if (chk_heart.isChecked()){
            diseases_list.add("2");
        }
        if (chk_malignant.isChecked()){
            diseases_list.add("5");
        }
        if (chk_tumors.isChecked()){
            diseases_list.add("4");
        }

        List<HealthRecordContents> previous_disease_content_list = new ArrayList<>();
        List<HealthRecordContents> chronic_diseases_content_list = new ArrayList<>();
        List<HealthRecordContents> continue_medicine_content_list = new ArrayList<>();


        if (previous_disease_list.size()>0){
            for (String s : previous_disease_list){
                previous_disease_content_list.add(new HealthRecordContents(s));
            }
        }
        if (chronic_diseases_list.size()>0){
            for (String s : chronic_diseases_list){
                chronic_diseases_content_list.add(new HealthRecordContents(s));
            }
        }
        if (continue_medicine_list.size()>0){
            for (String s : continue_medicine_list){
                continue_medicine_content_list.add(new HealthRecordContents(s));
            }
        }
        List<String> images_paths_string = new ArrayList<>();
        if (images_list.size()>0){
            for (MedicalImageObject o : images_list){
                if (!o.getImagePath().equals("")) {
                    images_paths_string.add(o.getImagePath());
                }
            }
        }

        save_health_record.setVisibility(View.GONE);
        loading_save_record.setVisibility(View.VISIBLE);
        HealthRecordAPIsClass.save_record(
                base,
                name_text,
                age_text,
                height_text,
                weight_text,
                new Gson().toJson(diseases_list),
                new Gson().toJson(previous_disease_content_list),
                new Gson().toJson(chronic_diseases_content_list),
                new Gson().toJson(continue_medicine_content_list),
                images_paths_string,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        save_health_record.setVisibility(View.VISIBLE);
                        loading_save_record.setVisibility(View.GONE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        BaseFunctions.showSuccessToast(base,getResources().getString(R.string.health_record_saved_success));
                        base.fragmentManager.popBackStack();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        save_health_record.setVisibility(View.VISIBLE);
                        loading_save_record.setVisibility(View.GONE);
                        BaseFunctions.showErrorToast(base,getResources().getString(R.string.no_internet));
                    }
                });
    }

    @Override
    public void loadImage(Uri imageUri, ImageView ivImage) {
        Glide.with(base).load(imageUri).into(ivImage);
    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        Uri source_uri = uri;
        Uri destination_uri = Uri.fromFile(getTempFile());
        Crop.of(source_uri,destination_uri).withAspect(16,9).start(base,EditMedicalRecordFragment.this);
    }

    class HealthRecordContents{
        @SerializedName("name") private String name = "";

        public HealthRecordContents(String name) {
            this.name = name;
        }
    }
}
