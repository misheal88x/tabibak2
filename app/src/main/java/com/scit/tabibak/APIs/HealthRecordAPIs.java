package com.scit.tabibak.APIs;

import com.scit.tabibak.Models.BaseResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface HealthRecordAPIs {

    @Multipart
    @POST("save-medical-report")
    Call<BaseResponse> save_report(
            @Header("Accept") String accept,
            @Part("name") RequestBody name,
            @Part("age") RequestBody age,
            @Part("tall") RequestBody tall,
            @Part("weight") RequestBody weight,
            @Part("disease_id") RequestBody disease_id,
            @Part("precedents") RequestBody precedents,
            @Part("genetic") RequestBody genetic,
            @Part("medicines") RequestBody medicines,
            @Part MultipartBody.Part[] images
    );

    @GET("has-medical-report")
    Call<BaseResponse> check_record(
            @Header("Accept") String accept
    );

    @GET("get-medical-report")
    Call<BaseResponse> get_my_record(
            @Header("Accept") String accept
    );

    @GET("medical-report/{id}")
    Call<BaseResponse> get_other_record(
            @Header("Accept") String accept,
            @Path("id") String id
    );

    @GET("delete-medical-image/{id}")
    Call<BaseResponse> remove_image(
            @Header("Accept") String accept,
            @Path("id") String id
    );
}
