package com.scit.tabibak.APIsClasses;

import android.content.Context;
import android.util.Log;

import com.scit.tabibak.APIs.UserAPIs;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.BaseRetrofit;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Models.BaseResponse;
import com.scit.tabibak.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Part;

public class UserAPIsClass extends BaseRetrofit {

    public static void register(
            final Context context,
            String name,
            String phone,
            String password,
            String role_id,
            String city_id,
            String specialization_id,
            String address,
            String identification_file,
            String email,
            String facebook,
            String facility_id,
            String image,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        RequestBody nameRequest = BaseFunctions.uploadFileStringConverter(name);
        RequestBody phoneRequest = BaseFunctions.uploadFileStringConverter(phone);
        RequestBody passwordRequest = BaseFunctions.uploadFileStringConverter(password);
        RequestBody role_idRequest = BaseFunctions.uploadFileStringConverter(role_id);
        RequestBody city_idRequest = BaseFunctions.uploadFileStringConverter(city_id);
        RequestBody specialization_idRequest = BaseFunctions.uploadFileStringConverter(specialization_id);
        RequestBody addressRequest = BaseFunctions.uploadFileStringConverter(address);
        RequestBody identification_fileRequest = BaseFunctions.uploadFileStringConverter(identification_file);
        RequestBody emailRequest = BaseFunctions.uploadFileStringConverter(email);
        RequestBody facebookRequest = BaseFunctions.uploadFileStringConverter(facebook);
        RequestBody facility_idRequest = BaseFunctions.uploadFileStringConverter(facility_id);
        MultipartBody.Part body=null;
        if (!image.equals("")){
            body = BaseFunctions.uploadFileImageConverter("image",image);
        }
        Retrofit retrofit = configureRetrofitWithoutBearer();
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.register("application/json",nameRequest,phoneRequest,passwordRequest,role_idRequest
        ,city_idRequest,specialization_idRequest,addressRequest,identification_fileRequest,emailRequest,facebookRequest,facility_idRequest,body);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()){
                    if (response.code() == 200){
                        if (response.body()!=null){
                            if (response.body().getStatus() == 0){
                                onResponse.onResponse();
                                BaseFunctions.showErrorToast(context,response.body().getMessage());
                            } else if (response.body().getStatus()==1){
                                onResponse.onResponse(response.body().getData());
                            }else {
                                onResponse.onResponse();
                                BaseFunctions.showErrorToast(context,response.body().getMessage());
                            }
                        }else {
                            onResponse.onResponse();
                        }
                    }else {
                        onResponse.onResponse();
                    }
                }else {
                    BufferedReader reader = null;
                    StringBuilder sb = new StringBuilder();
                    reader = new BufferedReader(new InputStreamReader(response.errorBody().byteStream()));
                    String line;
                    try {
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    String finallyError = sb.toString();
                    Log.i("dfdfrr", "onResponse: "+finallyError);
                    onResponse.onResponse();
                }
                //BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t)
            {
                Log.i("dfdfrr", "onResponse: "+t.getMessage());
                onFailure.onFailure();
            }
        });
    }

    public static void login(
            final Context context,
            String phone,
            String password,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.login("application/json",phone,password);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }


    public static void update_token(
            final Context context,
            String token,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.update_fcm("application/json",token);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
}
