package com.scit.tabibak.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scit.tabibak.APIsClasses.MedicalCosulationAPIsClass;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Dialogs.AddReplyDialog;
import com.scit.tabibak.Dialogs.RepliesDialog;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Interfaces.OnItemClickListener;
import com.scit.tabibak.Models.Medical_consulation_models.QestionObject;
import com.scit.tabibak.Models.Medical_consulation_models.ReplyObject;
import com.scit.tabibak.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class QuestionsAdapter extends RecyclerView.Adapter<QuestionsAdapter.ViewHolder>  {
    private Context context;
    private List<QestionObject> list;

    public QuestionsAdapter( Context context,List<QestionObject> list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView icon;
        private TextView name,date,text,see_replies,add_reply;
        private ProgressBar load_replies;
        private View divider;
        private LinearLayout reply_tools;

        public ViewHolder(View view) {
            super(view);
            icon = view.findViewById(R.id.icon);
            name = view.findViewById(R.id.name);
            date = view.findViewById(R.id.date);
            text = view.findViewById(R.id.text);
            load_replies = view.findViewById(R.id.load_replies);
            see_replies = view.findViewById(R.id.see_replies);
            add_reply = view.findViewById(R.id.add_reply);
            divider = view.findViewById(R.id.divider);
            reply_tools = view.findViewById(R.id.reply_tools);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_question, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        final QestionObject o = list.get(position);
        BaseFunctions.setGlideImage(context,holder.icon,o.getOwner().getAvatar());
        holder.name.setText(o.getOwner().getName());
        String new_date = BaseFunctions.dateToStringConverter(BaseFunctions.stringToDateConverter(o.getCreated_at()));
        holder.date.setText(new_date);
        holder.text.setText(o.getDetails());
        if (SharedPrefManager.getInstance(context).getUser().getCan_reply() == 1){
            holder.divider.setVisibility(View.VISIBLE);
            holder.add_reply.setVisibility(View.VISIBLE);
            holder.add_reply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AddReplyDialog dialog = new AddReplyDialog(context,o.getId());
                    dialog.show();
                }
            });
        }else {
            holder.divider.setVisibility(View.GONE);
            holder.add_reply.setVisibility(View.GONE);
        }

        holder.see_replies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callRepliesAPI(holder,o.getId());
            }
        });
    }

    private void callRepliesAPI(final ViewHolder holder, int id){
        holder.load_replies.setVisibility(View.VISIBLE);
        holder.reply_tools.setVisibility(View.GONE);
        MedicalCosulationAPIsClass.get_replies(context,
                id,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        holder.load_replies.setVisibility(View.GONE);
                        holder.reply_tools.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        holder.load_replies.setVisibility(View.GONE);
                        holder.reply_tools.setVisibility(View.VISIBLE);
                        String j = new Gson().toJson(json);
                        ReplyObject[] success = new Gson().fromJson(j,ReplyObject[].class);
                        if (success.length>0){
                            List<ReplyObject> lis = new ArrayList<>();
                            for (ReplyObject r : success){
                                lis.add(r);
                            }
                            RepliesDialog dialog = new RepliesDialog(context,lis,"");
                            dialog.show();
                        }else {
                            BaseFunctions.showErrorToast(context,context.getResources().getString(R.string.ask_no_replies));
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        holder.load_replies.setVisibility(View.GONE);
                        holder.reply_tools.setVisibility(View.VISIBLE);
                        BaseFunctions.showErrorToast(context,context.getResources().getString(R.string.no_internet));
                    }
                });
    }
}
