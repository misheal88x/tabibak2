package com.scit.tabibak.Interfaces;

public interface OnItemClickListener {
    void onItemClick(int position);
}
