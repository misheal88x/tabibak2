package com.scit.tabibak.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Models.Medical_consulation_models.ReplyObject;
import com.scit.tabibak.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class RepliesAdapter extends RecyclerView.Adapter<RepliesAdapter.ViewHolder> {
    private Context context;
    private List<ReplyObject> list;

    public RepliesAdapter(Context context,List<ReplyObject> list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView icon;
        private TextView name,date,text;

        public ViewHolder(View view) {
            super(view);
            icon = view.findViewById(R.id.icon);
            name = view.findViewById(R.id.name);
            date = view.findViewById(R.id.date);
            text = view.findViewById(R.id.text);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_reply, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        ReplyObject o = list.get(position);
        BaseFunctions.setGlideImage(context,holder.icon,o.getOwner().getAvatar());
        holder.name.setText(o.getOwner().getName());
        String new_date = BaseFunctions.dateToStringConverter(BaseFunctions.stringToDateConverter(o.getCreated_at()));
        holder.date.setText(new_date);
        holder.text.setText(o.getText());
    }
}
