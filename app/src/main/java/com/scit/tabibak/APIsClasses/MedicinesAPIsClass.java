package com.scit.tabibak.APIsClasses;

import android.content.Context;

import com.scit.tabibak.APIs.MedicinesAPIs;
import com.scit.tabibak.APIs.UserAPIs;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.BaseRetrofit;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MedicinesAPIsClass extends BaseRetrofit {
    public static void get_all(
            final Context context,
            int page,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        MedicinesAPIs api = retrofit.create(MedicinesAPIs.class);
        Call<BaseResponse> call = api.get_all("application/json",page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void get_med(
            final Context context,
            int id,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        MedicinesAPIs api = retrofit.create(MedicinesAPIs.class);
        Call<BaseResponse> call = api.get_med("application/json",String.valueOf(id));
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void search(
            final Context context,
            String name,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        MedicinesAPIs api = retrofit.create(MedicinesAPIs.class);
        Call<BaseResponse> call = api.search("application/json",name);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

}
