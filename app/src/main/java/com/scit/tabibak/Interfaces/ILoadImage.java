package com.scit.tabibak.Interfaces;

public interface ILoadImage {
    void onLoaded();
    void onFailed();
}
