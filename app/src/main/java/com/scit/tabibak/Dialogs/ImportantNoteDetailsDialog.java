package com.scit.tabibak.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Interfaces.ILoadImage;
import com.scit.tabibak.Models.Important_notes_models.ImportantNoteObject;
import com.scit.tabibak.R;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;

public class ImportantNoteDetailsDialog extends AlertDialog {
    private Context context;
    private TextView title,desc,date,time,date_title,time_title;
    private ImageView image;
    private ProgressBar loading_image;
    private ImportantNoteObject o;

    public ImportantNoteDetailsDialog(@NonNull Context context,ImportantNoteObject o) {
        super(context);
        this.context = context;
        this.o = o;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_important_note_details);

        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;

        init_views();
        init_events();
        init_dialog();
    }

    private void init_views() {
        title = findViewById(R.id.note_title);
        desc = findViewById(R.id.note_desc);
        image = findViewById(R.id.image);
        loading_image = findViewById(R.id.loading_image);
        date = findViewById(R.id.date);
        time = findViewById(R.id.time);
        date_title = findViewById(R.id.date_title);
        time_title = findViewById(R.id.time_title);
    }

    private void init_events() {
    }

    private void init_dialog() {
        title.setText(o.getTitle());
        desc.setText(o.getDetails());
        if (o.getImage()!=null&&!o.getImage().equals("")) {
            loading_image.setVisibility(View.VISIBLE);
            BaseFunctions.setGlideImageWithEvents(context, image, o.getImage(), new ILoadImage() {
                @Override
                public void onLoaded() {
                    loading_image.setVisibility(View.GONE);
                }

                @Override
                public void onFailed() {
                    loading_image.setVisibility(View.GONE);
                }
            });
        }else {
            image.setVisibility(View.GONE);
        }
        if (o.getAlarm_date()!=null&&!o.getAlarm_date().equals("")){
            date.setText(o.getAlarm_date());
        }else {
            date.setVisibility(View.GONE);
            date_title.setVisibility(View.GONE);
        }

        if (o.getAlarm_time()!=null&&!o.getAlarm_time().equals("")){
            time.setText(o.getAlarm_time());
        }else {
            time.setVisibility(View.GONE);
            time_title.setVisibility(View.GONE);
        }
    }
}
