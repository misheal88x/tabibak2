package com.scit.tabibak.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scit.tabibak.Dialogs.RepliesDialog;
import com.scit.tabibak.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class MyHealthRecordTextsAdapter  extends RecyclerView.Adapter<MyHealthRecordTextsAdapter.ViewHolder>{
    private Context context;
    private List<String> list;
    private boolean with_delete;

    public MyHealthRecordTextsAdapter( Context context,List<String> list,boolean with_delete) {
        this.context = context;
        this.list = list;
        this.with_delete = with_delete;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView text,delete;

        public ViewHolder(View view) {
            super(view);
            text = view.findViewById(R.id.text);
            delete = view.findViewById(R.id.delete);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_health_record_text, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.text.setText(list.get(position));
        if (with_delete){
            holder.delete.setVisibility(View.VISIBLE);
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage(context.getResources().getString(R.string.health_record_delete_message));
                    builder.setPositiveButton(context.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            list.remove(position);
                            notifyDataSetChanged();
                        }
                    });
                    builder.setNegativeButton(context.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.show();
                }
            });
        }else {
            holder.delete.setVisibility(View.GONE);
        }

    }
}
