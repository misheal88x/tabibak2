package com.scit.tabibak.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.scit.tabibak.APIsClasses.StartupAPIsClass;
import com.scit.tabibak.Bases.BaseActivity;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Models.Startup_models.StartupResponse;
import com.scit.tabibak.R;

public class SplashActivity extends BaseActivity {

    private ProgressBar loading;
    private RelativeLayout layout;
    @Override
    public void set_layout() {
            setContentView(R.layout.activity_splash);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        if (BaseFunctions.isOnline(SplashActivity.this)){
            callStartAPI();
        }else {
            startActivity(new Intent(SplashActivity.this,NoInternetActivity.class));
            finish();
        }
    }

    @Override
    public void init_views() {
        loading = findViewById(R.id.loading);
        layout = findViewById(R.id.layout);
    }

    @Override
    public void init_events() {

    }

    @Override
    public void set_fragment_place() {

    }

    private void callStartAPI(){
        loading.setVisibility(View.VISIBLE);
        StartupAPIsClass.get_all(SplashActivity.this,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        loading.setVisibility(View.GONE);
                        Snackbar.make(layout, getResources().getString(R.string.error_occured_2), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callStartAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        StartupResponse success = new Gson().fromJson(j,StartupResponse.class);
                        if (success.getCities()!=null){
                            SharedPrefManager.getInstance(SplashActivity.this).setCities(success.getCities());
                        }
                        if (success.getFacilities()!=null){
                            SharedPrefManager.getInstance(SplashActivity.this).setFacilities(success.getFacilities());
                        }
                        if (success.getSlides()!=null){
                            SharedPrefManager.getInstance(SplashActivity.this).setSlides(success.getSlides());
                        }
                        if (success.getSpecializations()!=null){
                            SharedPrefManager.getInstance(SplashActivity.this).setSpes(success.getSpecializations());
                        }
                        if (success.getContact()!=null){
                            if (success.getContact().size()>0){
                                SharedPrefManager.getInstance(SplashActivity.this).setContact(success.getContact().get(0).getContact_info());
                            }
                        }
                        startActivity(new Intent(SplashActivity.this,MainActivity.class));
                        finish();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        loading.setVisibility(View.GONE);
                        Snackbar.make(layout, getResources().getString(R.string.error_occured_2), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callStartAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
