package com.scit.tabibak.Models;

import com.google.gson.annotations.SerializedName;

public class UserObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("name") private String name = "";
    @SerializedName("phone") private String phone = "";
    @SerializedName("address") private String address = "";
    @SerializedName("facebook") private String facebook = "";
    @SerializedName("city_id") private int city_id = 0;
    @SerializedName("specialization_id") private int specialization_id = 0;
    @SerializedName("facility_id") private int facility_id = 0;
    @SerializedName("email") private String email = "";
    @SerializedName("avatar") private String avatar = "";
    @SerializedName("api_token") private String api_token = "";
    @SerializedName("role_id") private int role_id = 0;
    @SerializedName("can_reply") private int can_reply = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public int getSpecialization_id() {
        return specialization_id;
    }

    public void setSpecialization_id(int specialization_id) {
        this.specialization_id = specialization_id;
    }

    public int getFacility_id() {
        return facility_id;
    }

    public void setFacility_id(int facility_id) {
        this.facility_id = facility_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getApi_token() {
        return api_token;
    }

    public void setApi_token(String api_token) {
        this.api_token = api_token;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public int getCan_reply() {
        return can_reply;
    }

    public void setCan_reply(int can_reply) {
        this.can_reply = can_reply;
    }
}
