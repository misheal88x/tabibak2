package com.scit.tabibak.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.scit.tabibak.APIsClasses.MedicalDatesAPIsClass;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Dialogs.AddDoctorDateDialog;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Interfaces.OnItemClickListener;
import com.scit.tabibak.Models.Dates_requests.DateRequestObject;
import com.scit.tabibak.Models.Medical_dates_models.MedicalDateObject;
import com.scit.tabibak.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class DatesRequestsAdapter extends RecyclerView.Adapter<DatesRequestsAdapter.ViewHolder>{
    private Context context;
    private List<DateRequestObject> list;

    public DatesRequestsAdapter(Context context,List<DateRequestObject> list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView name,complaint,times,add_date,delete;
        private ProgressBar loading;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            complaint = view.findViewById(R.id.complaint);
            times = view.findViewById(R.id.times);
            add_date = view.findViewById(R.id.add_date);
            delete = view.findViewById(R.id.delete);
            loading = view.findViewById(R.id.loading);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_date_request, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final DateRequestObject o = list.get(position);
        holder.name.setText(o.getPatient().getName());
        holder.complaint.setText(o.getComplaint());
        if (o.getNote()!=null && !o.getNote().equals("")){
            holder.times.setText(o.getNote());
        }else {
            holder.times.setVisibility(View.GONE);
        }
        holder.add_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddDoctorDateDialog dialog = new AddDoctorDateDialog(context,o.getPatient().getId());
                dialog.show();
            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(context.getResources().getString(R.string.request_date_delete_message));
                builder.setPositiveButton(context.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callDeleteAPI(holder,o.getId(),position);
                    }
                });
                builder.setNegativeButton(context.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
    }

    private void callDeleteAPI(final ViewHolder holder, int id, final int position){
        holder.delete.setVisibility(View.GONE);
        holder.loading.setVisibility(View.VISIBLE);
        MedicalDatesAPIsClass.delete_date_request(context, id, new IResponse() {
            @Override
            public void onResponse() {
                holder.delete.setVisibility(View.VISIBLE);
                holder.loading.setVisibility(View.GONE);
            }

            @Override
            public void onResponse(Object json) {
                holder.delete.setVisibility(View.VISIBLE);
                holder.loading.setVisibility(View.GONE);
                BaseFunctions.showSuccessToast(context,context.getResources().getString(R.string.request_date_delete_success));
                list.remove(position);
                notifyDataSetChanged();
            }
        }, new IFailure() {
            @Override
            public void onFailure() {
                holder.delete.setVisibility(View.VISIBLE);
                holder.loading.setVisibility(View.GONE);
                BaseFunctions.showErrorToast(context,context.getResources().getString(R.string.no_internet));
            }
        });
    }
}
