package com.scit.tabibak.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scit.tabibak.APIs.MedicalCunsulationAPIs;
import com.scit.tabibak.APIsClasses.MedicalCosulationAPIsClass;
import com.scit.tabibak.APIsClasses.MedicalDatesAPIsClass;
import com.scit.tabibak.APIsClasses.MedicinesAPIsClass;
import com.scit.tabibak.Activities.SplashActivity;
import com.scit.tabibak.Adapters.QuestionsAdapter;
import com.scit.tabibak.Bases.BaseFragment;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Dialogs.ContactUsDialog;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Models.Medical_consulation_models.QestionObject;
import com.scit.tabibak.Models.Medical_consulation_models.QuestionsResponse;
import com.scit.tabibak.Models.Medicine_models.MedicineObject;
import com.scit.tabibak.Models.Medicine_models.MedicinesResponse;
import com.scit.tabibak.Models.UserObject;
import com.scit.tabibak.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MedicalConsulationFragment extends BaseFragment {

    private RelativeLayout toolbar;
    private TextView title;
    private RelativeLayout call,help,logout;

    private EditText question_edt;
    private TextView ask_btn;

    private TextView public_title;
    private RecyclerView questions_recycler;
    private List<QestionObject> list;
    private QuestionsAdapter questions_adapter;
    private LinearLayoutManager questions_layout_manager;

    private Animation fadeIn,fadeOut;
    private ProgressBar loading,loading_add,load_more;
    private LinearLayout no_data,no_internet,error;
    private NestedScrollView scrollView;

    private int currentPage = 1;
    private boolean continue_paginate = true;
    private int per_page = 20;

    private boolean is_ask_visible = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_medical_consoulation,container,false);
    }

    @Override
    public void init_views() {
        toolbar = base.findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        call = toolbar.findViewById(R.id.contact_us);
        help = toolbar.findViewById(R.id.help);
        logout = toolbar.findViewById(R.id.logout);
        if (!SharedPrefManager.getInstance(base).getLogged()){
            logout.setVisibility(View.GONE);
        }

        question_edt = base.findViewById(R.id.question_text);
        ask_btn = base.findViewById(R.id.ask);

        public_title = base.findViewById(R.id.public_questions_title);
        questions_recycler = base.findViewById(R.id.questions_recycler);

        fadeIn = AnimationUtils.loadAnimation(base,R.anim.fade_in);
        fadeOut = AnimationUtils.loadAnimation(base,R.anim.fade_out);
        loading = base.findViewById(R.id.loading);
        loading_add = base.findViewById(R.id.loading_add);
        load_more = base.findViewById(R.id.load_more);
        no_data = base.findViewById(R.id.no_data_layout);
        no_internet = base.findViewById(R.id.no_internet_layout);
        error = base.findViewById(R.id.error_layout);
        scrollView = base.findViewById(R.id.layout);
    }

    @Override
    public void init_events() {
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(base);
                builder.setTitle(getResources().getString(R.string.logout_title));
                builder.setMessage(getResources().getString(R.string.logout_message));
                builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPrefManager.getInstance(base).setLogged(false);
                        SharedPrefManager.getInstance(base).setUser(new UserObject());
                        startActivity(new Intent(base, SplashActivity.class));
                        base.finish();
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactUsDialog dialog = new ContactUsDialog(base);
                dialog.show();
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base.open_fragment(new HelpFragment());
            }
        });
        question_edt.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length()>0){
                    if (!is_ask_visible) {
                        ask_btn.setVisibility(View.VISIBLE);
                        ask_btn.startAnimation(fadeIn);
                        is_ask_visible = true;
                    }
                }else {
                    ask_btn.setVisibility(View.GONE);
                    ask_btn.startAnimation(fadeOut);
                    is_ask_visible = false;
                }
            }
        });
        ask_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (question_edt.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.ask_no_text));
                    return;
                }
                callAddQuestionAPI(question_edt.getText().toString());
            }
        });
        error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callGetQuestionsAPI(currentPage,0);
            }
        });

        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (list.size()>=per_page){
                            if (continue_paginate){
                                currentPage++;
                                callGetQuestionsAPI(currentPage,1);
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        title.setText(getResources().getString(R.string.home_medical_question));
        init_recycler();
    }

    private void init_recycler(){
        list = new ArrayList<>();
        questions_adapter = new QuestionsAdapter(base,list);
        questions_layout_manager = new LinearLayoutManager(base);
        questions_recycler.setLayoutManager(questions_layout_manager);
        questions_recycler.setAdapter(questions_adapter);
        callGetQuestionsAPI(currentPage,0);
    }

    private void callGetQuestionsAPI(final int page, final int type){
        if (type == 1){
            load_more.setVisibility(View.VISIBLE);
        }else {
            loading.setVisibility(View.VISIBLE);
            list.clear();
            questions_adapter = new QuestionsAdapter(base,list);
            questions_recycler.setAdapter(questions_adapter);
        }
        MedicalCosulationAPIsClass.get_all(
                base,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        try {
                            QuestionsResponse success = new Gson().fromJson(j, QuestionsResponse.class);
                            if (success.getData() != null) {
                                if (success.getData().size() > 0) {
                                    process_data(type);
                                    per_page = success.getPer_page();
                                    for (QestionObject m : success.getData()) {
                                        list.add(m);
                                        questions_adapter.notifyDataSetChanged();
                                    }
                                } else {
                                    no_data(type);
                                }
                            } else {
                                error_happend(type);
                            }
                        } catch (Exception e){
                            if (type == 0){
                                callGetQuestionsAPI(page, type);
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        no_internet(type);
                    }
                });
    }

    private void error_happend(int type){
        if (type == 1){
            load_more.setVisibility(View.GONE);
        }else {
            loading.setVisibility(View.GONE);
            error.setVisibility(View.VISIBLE);
            no_data.setVisibility(View.GONE);
            no_internet.setVisibility(View.GONE);
        }
    }
    private void process_data(int type){
        if (type == 0){
            loading.setVisibility(View.GONE);
            error.setVisibility(View.GONE);
            no_data.setVisibility(View.GONE);
            no_internet.setVisibility(View.GONE);
            public_title.setVisibility(View.VISIBLE);
        }else {
            load_more.setVisibility(View.GONE);
        }
    }
    private void no_data(int type){
        if (type == 0){
            loading.setVisibility(View.GONE);
            no_data.setVisibility(View.VISIBLE);
            public_title.setVisibility(View.GONE);
        }else {
            load_more.setVisibility(View.GONE);
            BaseFunctions.showWarningToast(base,getResources().getString(R.string.no_more));
            continue_paginate = false;
        }
    }

    private void no_internet(int type){
        if (type == 0){
            loading.setVisibility(View.GONE);
            no_internet.setVisibility(View.VISIBLE);
        }else {
            load_more.setVisibility(View.GONE);
            BaseFunctions.showErrorToast(base,getResources().getString(R.string.no_internet));
        }
    }

    private void callAddQuestionAPI(String details){
        showAddLoading(true);
        MedicalCosulationAPIsClass.add_question(
                base,
                details,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        showAddLoading(false);
                    }

                    @Override
                    public void onResponse(Object json) {
                        showAddLoading(false);
                        BaseFunctions.showSuccessToast(base, getResources().getString(R.string.ask_success));
                        ask_btn.setVisibility(View.GONE);
                        ask_btn.startAnimation(fadeOut);
                        is_ask_visible = false;
                        question_edt.setText("");
                        BaseFunctions.playMusic(base, R.raw.added);
                        currentPage = 1;
                        public_title.setVisibility(View.GONE);
                        callGetQuestionsAPI(currentPage,0);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        showAddLoading(false);
                        BaseFunctions.showErrorToast(base,getResources().getString(R.string.no_internet));
                    }
                });
    }

    private void showAddLoading(boolean state){
        if (state){
            loading_add.setVisibility(View.VISIBLE);
            ask_btn.setVisibility(View.GONE);
            question_edt.setEnabled(false);
        }else {
            loading_add.setVisibility(View.GONE);
            ask_btn.setVisibility(View.VISIBLE);
            question_edt.setEnabled(true);
        }
    }
}
