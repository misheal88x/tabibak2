package com.scit.tabibak.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.TextView;

import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Models.Medical_dates_models.MedicalDateObject;
import com.scit.tabibak.R;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;
import im.delight.android.webview.AdvancedWebView;

public class ContactUsDialog extends AlertDialog {
    private Context context;
    //private WebView webView;
    private AdvancedWebView mWebView;

    public ContactUsDialog(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_contact_us);

        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;

        init_views();
        init_events();
        init_dialog();
    }

    private void init_views() {
        //webView = findViewById(R.id.web_view);
        mWebView = findViewById(R.id.web_view);
    }

    private void init_events() {
    }

    private void init_dialog() {
        mWebView.loadHtml("<html dir=\"rtl\" lang=\"\"><body>" +SharedPrefManager.getInstance(context).getContact()+"</body></html>");
        //BaseFunctions.setWebView(webView, SharedPrefManager.getInstance(context).getContact());
    }
}
