package com.scit.tabibak.Models.Startup_models;

import com.google.gson.annotations.SerializedName;

public class SliderObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("image") private String image = "";
    @SerializedName("url") private String url = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
