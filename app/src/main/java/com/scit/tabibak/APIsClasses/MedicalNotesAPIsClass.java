package com.scit.tabibak.APIsClasses;

import android.content.Context;

import com.scit.tabibak.APIs.ImportantNotesAPIs;
import com.scit.tabibak.APIs.MedicalNotesAPIs;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.BaseRetrofit;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Models.BaseResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MedicalNotesAPIsClass extends BaseRetrofit {
    public static void get_all(
            final Context context,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicalNotesAPIs api = retrofit.create(MedicalNotesAPIs.class);
        Call<BaseResponse> call = api.get_all("application/json");
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void add_note(
            final Context context,
            String title,
            String details,
            String date,
            String time,
            String image,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        RequestBody titleRequest = BaseFunctions.uploadFileStringConverter(title);
        RequestBody detailsRequest = BaseFunctions.uploadFileStringConverter(details);
        RequestBody timeRequest = BaseFunctions.uploadFileStringConverter(time);
        RequestBody dateRequest = BaseFunctions.uploadFileStringConverter(date);
        MultipartBody.Part body = BaseFunctions.uploadFileImageConverter("image",image);

        Retrofit retrofit = configureRetrofitWithBearer(context);
        MedicalNotesAPIs api = retrofit.create(MedicalNotesAPIs.class);
        Call<BaseResponse> call = api.add_note("application/json",titleRequest,
                detailsRequest,dateRequest,timeRequest,body);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
}
