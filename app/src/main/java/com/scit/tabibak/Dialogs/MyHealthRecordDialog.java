package com.scit.tabibak.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scit.tabibak.APIsClasses.HealthRecordAPIsClass;
import com.scit.tabibak.Adapters.DiagnosisAdapter;
import com.scit.tabibak.Adapters.HealthNotesAdapter;
import com.scit.tabibak.Adapters.MedicalImagesAdapter;
import com.scit.tabibak.Adapters.MyHealthRecordTextsAdapter;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Models.Diagnosis_models.DiagnosisObject;
import com.scit.tabibak.Models.Health_record_models.BasicHealthInfo;
import com.scit.tabibak.Models.Health_record_models.ChronicDiseaseObject;
import com.scit.tabibak.Models.Health_record_models.ContinueMedicinesObject;
import com.scit.tabibak.Models.Health_record_models.HealthMedicalImageObject;
import com.scit.tabibak.Models.Health_record_models.MyHealthRecordObject;
import com.scit.tabibak.Models.Health_record_models.PreviousDiseaseObject;
import com.scit.tabibak.Models.MedicalImageObject;
import com.scit.tabibak.Models.Medical_notes_models.MedicalNoteObject;
import com.scit.tabibak.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MyHealthRecordDialog extends AlertDialog {
    private Context context;
    private TextView dialog_title,name,age,height,weight,diseases_in_family_title,previous_diseases_title,chronic_diseases_title,continue_medicine_title,medical_images_title,medical_diagnosis_title;
    private CheckBox blood_pressure,heart,diabetes,tumors,malignant;
    private RecyclerView previous_diseases_recycler,chronic_diseases_recycler,continue_medicine_recycler,medical_images_recycler,medical_diagnosis_recycler;
    private List<String> previous_disease_list,chronic_diseases_list,continue_medicine_list;
    private List<DiagnosisObject> medical_diagnosis_list;
    private List<MedicalImageObject> images_list;
    private MyHealthRecordTextsAdapter previous_disease_adapter,chronic_diseases_adapter,continue_medicine_adapter;
    private DiagnosisAdapter medical_diagnosis_adapter;
    private MedicalImagesAdapter images_adapter;
    private LinearLayoutManager previous_disease_layout_manager,chronic_diseases_layout_manager,continue_medicine_layout_manager,
            images_layout_manager,medical_diagnosis_layout_manager;
    private LinearLayout content_layout;
    private ProgressBar loading;
    private String type;
    private int id;

    public MyHealthRecordDialog(@NonNull Context context,String type,int id) {
        super(context);
        this.context = context;
        this.type = type;
        this.id = id;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_my_health_record);

        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;

        init_views();
        init_events();
        init_dialog();
    }

    private void init_views() {
        //TextView
        dialog_title = findViewById(R.id.dialog_title);
        name = findViewById(R.id.name);
        age = findViewById(R.id.age);
        height = findViewById(R.id.height);
        weight = findViewById(R.id.weight);
        diseases_in_family_title = findViewById(R.id.diseases_in_family_title);
        previous_diseases_title = findViewById(R.id.previous_diseases_title);
        chronic_diseases_title = findViewById(R.id.chronic_diseases_title);
        continue_medicine_title = findViewById(R.id.continue_medicine_title);
        medical_images_title = findViewById(R.id.medical_images_title);
        medical_diagnosis_title = findViewById(R.id.medical_diagnosis_title);
        //Checkbox
        blood_pressure = findViewById(R.id.blood_pressure);
        heart = findViewById(R.id.heart);
        diabetes = findViewById(R.id.diabetes);
        tumors = findViewById(R.id.tumors);
        malignant = findViewById(R.id.malignant);
        blood_pressure.setEnabled(false);
        heart.setEnabled(false);
        tumors.setEnabled(false);
        diabetes.setEnabled(false);
        malignant.setEnabled(false);
        //RecyclerView
        previous_diseases_recycler = findViewById(R.id.previous_diseases_recycler);
        chronic_diseases_recycler = findViewById(R.id.chronic_diseases_recycler);
        continue_medicine_recycler = findViewById(R.id.continue_medicine_recycler);
        medical_images_recycler = findViewById(R.id.medical_images_recycler);
        medical_diagnosis_recycler = findViewById(R.id.medical_diagnosis_recycler);
        //LinearLayout
        content_layout = findViewById(R.id.content_layout);
        //Progressbar
        loading = findViewById(R.id.loading);
    }

    private void init_events() {
    }

    private void init_dialog() {
        init_recyclers();
        if (type.equals("mine")){
            callHealthRecordAPI();
        }else {
            dialog_title.setText(context.getResources().getString(R.string.my_health_record_other));
            callOtherHealthRecordAPI();
        }
    }

    private void init_recyclers(){
        images_list = new ArrayList<>();
        previous_disease_list = new ArrayList<>();
        chronic_diseases_list = new ArrayList<>();
        continue_medicine_list = new ArrayList<>();
        medical_diagnosis_list = new ArrayList<>();

        previous_disease_adapter = new MyHealthRecordTextsAdapter(context,previous_disease_list,false);
        chronic_diseases_adapter = new MyHealthRecordTextsAdapter(context,chronic_diseases_list,false);
        continue_medicine_adapter = new MyHealthRecordTextsAdapter(context,continue_medicine_list,false);
        images_adapter = new MedicalImagesAdapter(context,images_list);
        medical_diagnosis_adapter = new DiagnosisAdapter(context,medical_diagnosis_list);

        previous_disease_layout_manager = new LinearLayoutManager(context);
        chronic_diseases_layout_manager = new LinearLayoutManager(context);
        continue_medicine_layout_manager = new LinearLayoutManager(context);
        medical_diagnosis_layout_manager = new LinearLayoutManager(context);
        images_layout_manager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);

        previous_diseases_recycler.setLayoutManager(previous_disease_layout_manager);
        chronic_diseases_recycler.setLayoutManager(chronic_diseases_layout_manager);
        continue_medicine_recycler.setLayoutManager(continue_medicine_layout_manager);
        medical_images_recycler.setLayoutManager(images_layout_manager);
        medical_diagnosis_recycler.setLayoutManager(medical_diagnosis_layout_manager);

        previous_diseases_recycler.setAdapter(previous_disease_adapter);
        chronic_diseases_recycler.setAdapter(chronic_diseases_adapter);
        continue_medicine_recycler.setAdapter(continue_medicine_adapter);
        medical_images_recycler.setAdapter(images_adapter);
        medical_diagnosis_recycler.setAdapter(medical_diagnosis_adapter);
    }

    private void callHealthRecordAPI(){
        content_layout.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);
        HealthRecordAPIsClass.get_my_record(context,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        content_layout.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        content_layout.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                        String j = new Gson().toJson(json);
                        MyHealthRecordObject success = new Gson().fromJson(j,MyHealthRecordObject.class);
                        //Basic info
                        if (success.getMedical_reports()!=null){
                            if (success.getMedical_reports().size()>0){
                                BasicHealthInfo b = success.getMedical_reports().get(0);
                                name.setText(b.getName());
                                age.setText(b.getAge());
                                height.setText(b.getTall());
                                weight.setText(b.getWeight());
                                String[] arr = new Gson().fromJson(b.getDisease_id(),String[].class);
                                if (arr.length>0){
                                    for (String s : arr){
                                        switch (s){
                                            case "1":{
                                                blood_pressure.setChecked(true);
                                            }break;
                                            case "2":{
                                                heart.setChecked(true);
                                            }break;
                                            case "3":{
                                                diabetes.setChecked(true);
                                            }break;
                                            case "4":{
                                                tumors.setChecked(true);
                                            }break;
                                            case "5":{
                                                malignant.setChecked(true);
                                            }break;
                                        }
                                    }
                                }
                            }else {
                                name.setVisibility(View.GONE);
                                age.setVisibility(View.GONE);
                                weight.setVisibility(View.GONE);
                                height.setVisibility(View.GONE);
                                blood_pressure.setChecked(false);
                                heart.setChecked(false);
                                diabetes.setChecked(false);
                                tumors.setChecked(false);
                                malignant.setChecked(false);
                            }
                        }else {
                            name.setVisibility(View.GONE);
                            age.setVisibility(View.GONE);
                            weight.setVisibility(View.GONE);
                            height.setVisibility(View.GONE);
                            blood_pressure.setChecked(false);
                            heart.setChecked(false);
                            diabetes.setChecked(false);
                            tumors.setChecked(false);
                            malignant.setChecked(false);
                        }
                        //Previous Diseases
                        if (success.getPrecedent()!=null){
                            if (success.getPrecedent().size()>0){
                                for (PreviousDiseaseObject p : success.getPrecedent()){
                                    previous_disease_list.add(p.getName());
                                    previous_disease_adapter.notifyDataSetChanged();
                                }
                            }else {
                                previous_diseases_title.setVisibility(View.GONE);
                                previous_diseases_recycler.setVisibility(View.GONE);
                            }
                        }else {
                            previous_diseases_title.setVisibility(View.GONE);
                            previous_diseases_recycler.setVisibility(View.GONE);
                        }
                        //Medical Images
                        if (success.getMedical_images()!=null){
                            if (success.getMedical_images().size()>0){
                                for (HealthMedicalImageObject h : success.getMedical_images()){
                                    MedicalImageObject m = new MedicalImageObject();
                                    m.setImageUrl(h.getImage());
                                    images_list.add(m);
                                    images_adapter.notifyDataSetChanged();
                                }
                            }else {
                                medical_images_title.setVisibility(View.GONE);
                                medical_images_recycler.setVisibility(View.GONE);
                            }
                        }else {
                            medical_images_title.setVisibility(View.GONE);
                            medical_images_recycler.setVisibility(View.GONE);
                        }
                        //Chronic Diseases
                        if (success.getGenetic()!=null){
                            if (success.getGenetic().size()>0){
                                for (ChronicDiseaseObject o : success.getGenetic()){
                                    chronic_diseases_list.add(o.getName());
                                    chronic_diseases_adapter.notifyDataSetChanged();
                                }
                            }else {
                                chronic_diseases_title.setVisibility(View.GONE);
                                chronic_diseases_recycler.setVisibility(View.GONE);
                            }
                        }else {
                            chronic_diseases_title.setVisibility(View.GONE);
                            chronic_diseases_recycler.setVisibility(View.GONE);
                        }
                        //Medicines
                        if (success.getMedicines()!=null){
                            if (success.getMedicines().size()>0){
                                for (ContinueMedicinesObject c : success.getMedicines()){
                                    continue_medicine_list.add(c.getName());
                                    continue_medicine_adapter.notifyDataSetChanged();
                                }
                            }else {
                                continue_medicine_title.setVisibility(View.GONE);
                                continue_medicine_recycler.setVisibility(View.GONE);
                            }
                        }else {
                            continue_medicine_title.setVisibility(View.GONE);
                            continue_medicine_recycler.setVisibility(View.GONE);
                        }
                        //Diagnosis
                        if (success.getDiagnoses()!=null){
                            if (success.getDiagnoses().size()>0){
                                for (DiagnosisObject d : success.getDiagnoses()){
                                    medical_diagnosis_list.add(d);
                                    medical_diagnosis_adapter.notifyDataSetChanged();
                                }
                            }else {
                                medical_diagnosis_title.setVisibility(View.GONE);
                                medical_diagnosis_recycler.setVisibility(View.GONE);
                            }
                        }else {
                            medical_diagnosis_title.setVisibility(View.GONE);
                            medical_diagnosis_recycler.setVisibility(View.GONE);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        content_layout.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                        BaseFunctions.showErrorToast(context,context.getResources().getString(R.string.no_internet));
                    }
                });
    }

    private void callOtherHealthRecordAPI(){
        content_layout.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);
        HealthRecordAPIsClass.get_other_record(context,
                id,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        content_layout.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        content_layout.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                        String j = new Gson().toJson(json);
                        MyHealthRecordObject success = new Gson().fromJson(j,MyHealthRecordObject.class);
                        //Basic info
                        if (success.getMedical_reports()!=null){
                            if (success.getMedical_reports().size()>0){
                                BasicHealthInfo b = success.getMedical_reports().get(0);
                                name.setText(b.getName());
                                age.setText(b.getAge());
                                height.setText(b.getTall());
                                weight.setText(b.getWeight());
                                String[] arr = new Gson().fromJson(b.getDisease_id(),String[].class);
                                if (arr.length>0){
                                    for (String s : arr){
                                        switch (s){
                                            case "1":{
                                                blood_pressure.setChecked(true);
                                            }break;
                                            case "2":{
                                                heart.setChecked(true);
                                            }break;
                                            case "3":{
                                                diabetes.setChecked(true);
                                            }break;
                                            case "4":{
                                                tumors.setChecked(true);
                                            }break;
                                            case "5":{
                                                malignant.setChecked(true);
                                            }break;
                                        }
                                    }
                                }
                            }else {
                                name.setVisibility(View.GONE);
                                age.setVisibility(View.GONE);
                                weight.setVisibility(View.GONE);
                                height.setVisibility(View.GONE);
                                blood_pressure.setChecked(false);
                                heart.setChecked(false);
                                diabetes.setChecked(false);
                                tumors.setChecked(false);
                                malignant.setChecked(false);
                            }
                        }else {
                            name.setVisibility(View.GONE);
                            age.setVisibility(View.GONE);
                            weight.setVisibility(View.GONE);
                            height.setVisibility(View.GONE);
                            blood_pressure.setChecked(false);
                            heart.setChecked(false);
                            diabetes.setChecked(false);
                            tumors.setChecked(false);
                            malignant.setChecked(false);
                        }
                        //Previous Diseases
                        if (success.getPrecedent()!=null){
                            if (success.getPrecedent().size()>0){
                                for (PreviousDiseaseObject p : success.getPrecedent()){
                                    previous_disease_list.add(p.getName());
                                    previous_disease_adapter.notifyDataSetChanged();
                                }
                            }else {
                                previous_diseases_title.setVisibility(View.GONE);
                                previous_diseases_recycler.setVisibility(View.GONE);
                            }
                        }else {
                            previous_diseases_title.setVisibility(View.GONE);
                            previous_diseases_recycler.setVisibility(View.GONE);
                        }
                        //Medical Images
                        if (success.getMedical_images()!=null){
                            if (success.getMedical_images().size()>0){
                                for (HealthMedicalImageObject h : success.getMedical_images()){
                                    MedicalImageObject m = new MedicalImageObject();
                                    m.setImageUrl(h.getImage());
                                    images_list.add(m);
                                    images_adapter.notifyDataSetChanged();
                                }
                            }else {
                                medical_images_title.setVisibility(View.GONE);
                                medical_images_recycler.setVisibility(View.GONE);
                            }
                        }else {
                            medical_images_title.setVisibility(View.GONE);
                            medical_images_recycler.setVisibility(View.GONE);
                        }
                        //Chronic Diseases
                        if (success.getGenetic()!=null){
                            if (success.getGenetic().size()>0){
                                for (ChronicDiseaseObject o : success.getGenetic()){
                                    chronic_diseases_list.add(o.getName());
                                    chronic_diseases_adapter.notifyDataSetChanged();
                                }
                            }else {
                                chronic_diseases_title.setVisibility(View.GONE);
                                chronic_diseases_recycler.setVisibility(View.GONE);
                            }
                        }else {
                            chronic_diseases_title.setVisibility(View.GONE);
                            chronic_diseases_recycler.setVisibility(View.GONE);
                        }
                        //Medicines
                        if (success.getMedicines()!=null){
                            if (success.getMedicines().size()>0){
                                for (ContinueMedicinesObject c : success.getMedicines()){
                                    continue_medicine_list.add(c.getName());
                                    continue_medicine_adapter.notifyDataSetChanged();
                                }
                            }else {
                                continue_medicine_title.setVisibility(View.GONE);
                                continue_medicine_recycler.setVisibility(View.GONE);
                            }
                        }else {
                            continue_medicine_title.setVisibility(View.GONE);
                            continue_medicine_recycler.setVisibility(View.GONE);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        content_layout.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                        BaseFunctions.showErrorToast(context,context.getResources().getString(R.string.no_internet));
                    }
                });
    }
}
