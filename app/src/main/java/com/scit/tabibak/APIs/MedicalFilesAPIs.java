package com.scit.tabibak.APIs;

import com.scit.tabibak.Models.BaseResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface MedicalFilesAPIs {
    @GET("medical-files")
    Call<BaseResponse> get_all(
            @Header("Accept") String accept
    );
    @Multipart
    @POST("add-medical-file")
    Call<BaseResponse> add_file(
            @Header("Accept") String accept,
            @Part("title") RequestBody title,
            @Part("details") RequestBody details,
            @Part MultipartBody.Part image
    );

    @GET("delete-medical-file/{id}")
    Call<BaseResponse> delete_file(
            @Header("Accept") String accept,
            @Path("id") String id
    );
}
