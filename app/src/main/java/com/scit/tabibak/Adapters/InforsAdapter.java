package com.scit.tabibak.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Interfaces.ILoadImage;
import com.scit.tabibak.Interfaces.OnItemClickListener;
import com.scit.tabibak.Models.Medical_info_models.MedicalInfoObject;
import com.scit.tabibak.Models.Startup_models.SpecializationObject;
import com.scit.tabibak.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class InforsAdapter extends RecyclerView.Adapter<InforsAdapter.ViewHolder>{
    private Context context;
    private List<MedicalInfoObject> list;

    private OnItemClickListener onItemClickListener;

    public InforsAdapter(Context context,List<MedicalInfoObject> list,OnItemClickListener onItemClickListener) {
        this.context = context;
        this.list = list;
        this.onItemClickListener = onItemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView icon;
        private TextView name,profession,title,text;
        private SimpleDraweeView image;
        private ProgressBar loading;
        private CardView layout;


        public ViewHolder(View view) {
            super(view);
            icon = view.findViewById(R.id.icon);
            name = view.findViewById(R.id.name);
            profession = view.findViewById(R.id.profession);
            image = view.findViewById(R.id.image);
            loading = view.findViewById(R.id.loading);
            title = view.findViewById(R.id.title);
            text = view.findViewById(R.id.text);
            layout = view.findViewById(R.id.layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_info, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        MedicalInfoObject o = list.get(position);
        BaseFunctions.setFrescoImage(holder.icon,o.getOwner().getAvatar());
        BaseFunctions.setFrescoImage(holder.image,o.getImage());
        holder.name.setText(o.getOwner().getName());
        if (SharedPrefManager.getInstance(context).getSpes().size()>0){
            for (SpecializationObject s : SharedPrefManager.getInstance(context).getSpes()){
                if (s.getId() == o.getOwner().getSpecialization_id()){
                    holder.profession.setText(s.getName());
                    break;
                }
            }
        }
        holder.title.setText(o.getName());
        if (o.getSub_details().length()>30){
            holder.text.setText(o.getSub_details().substring(0,29)+"...");
        }else {
            holder.text.setText(o.getSub_details());
        }
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(position);
            }
        });
    }
}
