package com.scit.tabibak.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.scit.tabibak.Adapters.RepliesAdapter;
import com.scit.tabibak.Models.Medical_dates_models.MedicalDateObject;
import com.scit.tabibak.R;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class DateDetailsDialog extends AlertDialog {
    private Context context;
    private TextView title,desc,date,time;
    private MedicalDateObject o;

    public DateDetailsDialog(@NonNull Context context,MedicalDateObject o) {
        super(context);
        this.context = context;
        this.o = o;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_date_details);

        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;

        init_views();
        init_events();
        init_dialog();
    }

    private void init_views() {
        title = findViewById(R.id.date_title);
        desc = findViewById(R.id.desc);
        date = findViewById(R.id.date);
        time = findViewById(R.id.time);
    }

    private void init_events() {
    }

    private void init_dialog() {
        title.setText(o.getTitle());
        desc.setText(o.getDetails());
        date.setText(o.getDate());
        time.setText(o.getTime());
    }
}
