package com.scit.tabibak.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scit.tabibak.APIs.DiagnosisAPIs;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Interfaces.OnItemClickListener;
import com.scit.tabibak.Models.Diagnosis_models.DiagnosisObject;
import com.scit.tabibak.Models.Medical_notes_models.MedicalNoteObject;
import com.scit.tabibak.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class DiagnosisAdapter extends RecyclerView.Adapter<DiagnosisAdapter.ViewHolder>{
    private Context context;
    private List<DiagnosisObject> list;

    public DiagnosisAdapter(Context context,List<DiagnosisObject> list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView icon;
        private TextView name,status,text,medicines;
        private LinearLayout pills_layout;

        public ViewHolder(View view) {
            super(view);
            icon = view.findViewById(R.id.icon);
            name = view.findViewById(R.id.name);
            status = view.findViewById(R.id.status);
            text = view.findViewById(R.id.text);
            medicines = view.findViewById(R.id.medicines);
            pills_layout = view.findViewById(R.id.pills_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_diagnosis, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        DiagnosisObject o = list.get(position);

        holder.status.setText(o.getMedical_status());
        holder.text.setText(o.getMedical_diagnosis());
        if (o.getDoctor()!=null){
            holder.name.setText(o.getDoctor().getName());
            BaseFunctions.setGlideImage(context,holder.icon,o.getDoctor().getAvatar());
        }
        if (o.getPrescription()!=null&&!o.getPrescription().equals("")){
            holder.medicines.setText(o.getPrescription());
        }else {
            holder.pills_layout.setVisibility(View.GONE);
        }
    }
}
