package com.scit.tabibak.Fragments;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.gson.Gson;
import com.scit.tabibak.APIs.MedicalFilesAPIs;
import com.scit.tabibak.APIsClasses.MedicalFilesAPIsClass;
import com.scit.tabibak.Activities.LoginActivity;
import com.scit.tabibak.Activities.SplashActivity;
import com.scit.tabibak.Adapters.MedicalFilesAdapter;
import com.scit.tabibak.Bases.BaseFragment;
import com.scit.tabibak.Bases.BaseFunctions;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Dialogs.ContactUsDialog;
import com.scit.tabibak.Dialogs.FileDetailsDialog;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Interfaces.OnItemClickListener;
import com.scit.tabibak.Models.MedicalFileObject;
import com.scit.tabibak.Models.UserObject;
import com.scit.tabibak.R;
import com.scit.tabibak.Utils.FileUtils;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.graphics.PathUtils;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.app.Activity.RESULT_OK;

public class MedicalFilesFragment extends BaseFragment implements OnItemClickListener , BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.ImageLoaderDelegate{

    private RelativeLayout toolbar;
    private TextView title;
    private RelativeLayout call,help,logout;

    private Button add_file,add_image;
    private LinearLayout add_file_layout;
    private EditText file_title,desc;
    private ImageView image;
    private TextView add_file_btn;
    private ImageView minimize;
    private String imagePath = "";
    private static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";

    private TextView added_files;
    private RecyclerView files_recycler;
    private List<MedicalFileObject> list;
    private MedicalFilesAdapter files_adapter;
    private LinearLayoutManager files_layout_manager;

    private ProgressBar loading,loading_add;
    private LinearLayout no_data,no_internet,error;

    private Animation fadeIn,fadeOut;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_medical_files,container,false);
    }

    @Override
    public void init_views() {
        toolbar = base.findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        call = toolbar.findViewById(R.id.contact_us);
        help = toolbar.findViewById(R.id.help);
        logout = toolbar.findViewById(R.id.logout);
        if (!SharedPrefManager.getInstance(base).getLogged()){
            logout.setVisibility(View.GONE);
        }

        add_file = base.findViewById(R.id.add_file);
        add_image = base.findViewById(R.id.pick_image);
        add_file_layout = base.findViewById(R.id.add_file_layout);
        file_title = base.findViewById(R.id.file_title);
        desc = base.findViewById(R.id.desc);
        image = base.findViewById(R.id.image);
        add_file_btn = base.findViewById(R.id.add_file_btn);
        added_files = base.findViewById(R.id.added_files_title);
        files_recycler = base.findViewById(R.id.files_recycler);
        loading = base.findViewById(R.id.loading);
        loading_add = base.findViewById(R.id.loading_add);
        no_data = base.findViewById(R.id.no_data_layout);
        no_internet = base.findViewById(R.id.no_internet_layout);
        error = base.findViewById(R.id.error_layout);
        minimize = base.findViewById(R.id.minimize);

        fadeIn = AnimationUtils.loadAnimation(base,R.anim.fade_in);
        fadeOut = AnimationUtils.loadAnimation(base,R.anim.fade_out);
    }

    @Override
    public void init_events() {
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(base);
                builder.setTitle(getResources().getString(R.string.logout_title));
                builder.setMessage(getResources().getString(R.string.logout_message));
                builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPrefManager.getInstance(base).setLogged(false);
                        SharedPrefManager.getInstance(base).setUser(new UserObject());
                        startActivity(new Intent(base, SplashActivity.class));
                        base.finish();
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactUsDialog dialog = new ContactUsDialog(base);
                dialog.show();
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base.open_fragment(new HelpFragment());
            }
        });
        add_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_file.setVisibility(View.GONE);
                add_file_layout.setVisibility(View.VISIBLE);
                add_file_layout.startAnimation(fadeIn);
                minimize.setVisibility(View.VISIBLE);
            }
        });
        minimize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                file_title.setText("");
                desc.setText("");
                imagePath = "";
                image.setVisibility(View.GONE);
                add_file_layout.setVisibility(View.GONE);
                add_file_layout.startAnimation(fadeOut);
                add_file.setVisibility(View.VISIBLE);
                minimize.setVisibility(View.GONE);
            }
        });

        add_file_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (file_title.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.file_no_title));
                    return;
                }
                if (desc.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.file_no_notes));
                    return;
                }
                if (imagePath.equals("")){
                    BaseFunctions.showWarningToast(base,getResources().getString(R.string.file_no_image));
                    return;
                }
                callAddFileAPI(file_title.getText().toString(),desc.getText().toString());
            }
        });
        add_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE},1000);
            }
        });
        error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callGetFilesAPI();
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        title.setText(getResources().getString(R.string.record_medical_files));
        init_recycler();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1000){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
                    ImagePicker.Companion.with(MedicalFilesFragment.this)
                            .galleryOnly()
                            //.crop(16f,9f)	    			//Crop image(Optional), Check Customization for more option
                            .compress(1024)			//Final image size will be less than 1 MB(Optional)
                            .start();
                }else {
                    BSImagePicker singleSelectionPicker = new BSImagePicker.Builder("com.scit.tabibak.provider")
                     .hideCameraTile() //Default: show. Set this if you don't want user to take photo.
                     .hideGalleryTile() //Default: show. Set this if you don't want to further let user select from a gallery app. In such case, I suggest you to set maximum displaying images to Integer.MAX_VALUE.
                     .build();
                     singleSelectionPicker.show(getChildFragmentManager(),"Picker");
                }



            }else {
                BaseFunctions.showErrorToast(base,getResources().getString(R.string.no_permission));
            }
        }
    }

    private void init_recycler(){
        list = new ArrayList<>();
        files_adapter = new MedicalFilesAdapter(base,list,this);
        files_layout_manager = new LinearLayoutManager(base);
        files_recycler.setLayoutManager(files_layout_manager);
        files_recycler.setAdapter(files_adapter);
        callGetFilesAPI();
    }

    @Override
    public void onItemClick(int position) {
        FileDetailsDialog dialog = new FileDetailsDialog(base,list.get(position));
        dialog.show();
    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        FileUtils utils = new FileUtils(base);
        imagePath = utils.getPath(uri);
        Bitmap myBitmap = BitmapFactory.decodeFile(imagePath);
        image.setImageBitmap(myBitmap);
        image.setVisibility(View.VISIBLE);
        image.startAnimation(fadeIn);
        //Uri source_uri = uri;
        //Uri destination_uri = Uri.fromFile(getTempFile());
        //Crop.of(source_uri,destination_uri).withAspect(16,9).start(base,MedicalFilesFragment.this);
    }

    @Override
    public void loadImage(Uri imageUri, ImageView ivImage) {
        Glide.with(base).load(imageUri).into(ivImage);
    }

    private File getTempFile() {
        if (isSDCARDMounted()) {

            File f = new File(Environment.getExternalStorageDirectory(),TEMP_PHOTO_FILE);
            try {
                f.createNewFile();
            } catch (IOException e) {

            }
            return f;
        } else {
            return null;
        }
    }

    private boolean isSDCARDMounted(){
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED))
            return true;
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case Crop.REQUEST_CROP:{
                handle_crop(resultCode,data);
            }break;
            default:{
                if (resultCode == Activity.RESULT_OK) {
                    imagePath = ImagePicker.Companion.getFilePath(data);
                    Bitmap myBitmap = BitmapFactory.decodeFile(imagePath);
                    image.setImageBitmap(myBitmap);
                    image.setVisibility(View.VISIBLE);
                    image.startAnimation(fadeIn);
                }
            }
        }
    }

    private void handle_crop(int code, final Intent data){
        if (code == RESULT_OK){
            base.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String filePath=  Environment.getExternalStorageDirectory()
                            + "/"+"temporary_holder.jpg";
                    if (new File(filePath).exists()){
                        imagePath = filePath;
                        Bitmap myBitmap = BitmapFactory.decodeFile(imagePath);
                        image.setImageBitmap(myBitmap);
                        image.setVisibility(View.VISIBLE);
                        image.startAnimation(fadeIn);
                    }

                }
            });
        }
    }

    private void callGetFilesAPI(){
        loading.setVisibility(View.VISIBLE);
        added_files.setVisibility(View.GONE);
        list.clear();
        files_adapter = new MedicalFilesAdapter(base,list,this);
        files_recycler.setAdapter(files_adapter);
        no_data.setVisibility(View.GONE);
        error.setVisibility(View.GONE);
        no_internet.setVisibility(View.GONE);
        MedicalFilesAPIsClass.get_all(base,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        loading.setVisibility(View.GONE);
                        error.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        loading.setVisibility(View.GONE);
                        String j = new Gson().toJson(json);
                        try {
                            MedicalFileObject[] success = new Gson().fromJson(j,MedicalFileObject[].class);
                            if (success.length>0){
                                added_files.setVisibility(View.VISIBLE);
                                for (MedicalFileObject o : success){
                                    list.add(o);
                                    files_adapter.notifyDataSetChanged();
                                }
                            }else {
                                added_files.setVisibility(View.GONE);
                                no_data.setVisibility(View.VISIBLE);
                            }
                        }catch (Exception e){
                            callGetFilesAPI();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        loading.setVisibility(View.GONE);
                        no_internet.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void callAddFileAPI(String title,String details){
        showAddLoading(true);
        MedicalFilesAPIsClass.add_file(
                base,
                title,
                details,
                imagePath,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        showAddLoading(false);
                    }

                    @Override
                    public void onResponse(Object json) {
                        showAddLoading(false);
                        BaseFunctions.showSuccessToast(base,getResources().getString(R.string.file_add_success));
                        file_title.setText("");
                        desc.setText("");
                        imagePath = "";
                        image.setVisibility(View.GONE);
                        add_file_layout.setVisibility(View.GONE);
                        add_file_layout.startAnimation(fadeOut);
                        add_file.setVisibility(View.VISIBLE);
                        minimize.setVisibility(View.GONE);
                        BaseFunctions.playMusic(base,R.raw.added);
                        callGetFilesAPI();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        showAddLoading(false);
                        BaseFunctions.showErrorToast(base,getResources().getString(R.string.no_internet));
                    }
                });
    }

    private void showAddLoading(boolean state){
        if (state){
            loading_add.setVisibility(View.VISIBLE);
            add_file_btn.setVisibility(View.GONE);
            title.setEnabled(false);
            desc.setEnabled(false);
            add_image.setEnabled(false);
        }else {
            loading_add.setVisibility(View.GONE);
            add_file_btn.setVisibility(View.VISIBLE);
            title.setEnabled(true);
            desc.setEnabled(true);
            add_image.setEnabled(true);
        }
    }
}
