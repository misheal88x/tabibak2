package com.scit.tabibak.Models.Medical_dates_models;

import com.google.gson.annotations.SerializedName;

public class MedicalDateObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("title") private String title = "";
    @SerializedName("details") private String details = "";
    @SerializedName("date") private String date = "";
    @SerializedName("time") private String time = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
