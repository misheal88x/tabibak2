package com.scit.tabibak.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scit.tabibak.APIsClasses.MedicalDatesAPIsClass;
import com.scit.tabibak.Activities.SplashActivity;
import com.scit.tabibak.Adapters.DatesRequestsAdapter;
import com.scit.tabibak.Adapters.PatientsDatesAdapter;
import com.scit.tabibak.Bases.BaseFragment;
import com.scit.tabibak.Bases.SharedPrefManager;
import com.scit.tabibak.Dialogs.ContactUsDialog;
import com.scit.tabibak.Dialogs.DateDetailsDialog;
import com.scit.tabibak.Interfaces.IFailure;
import com.scit.tabibak.Interfaces.IOnOperationClick;
import com.scit.tabibak.Interfaces.IResponse;
import com.scit.tabibak.Interfaces.OnItemClickListener;
import com.scit.tabibak.Models.Dates_requests.DateRequestObject;
import com.scit.tabibak.Models.Medical_dates_models.MedicalDateObject;
import com.scit.tabibak.Models.Medical_dates_models.PatientDateObject;
import com.scit.tabibak.Models.UserObject;
import com.scit.tabibak.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PatientsDatesFragment extends BaseFragment implements OnItemClickListener, IOnOperationClick {

    private RelativeLayout toolbar;
    private TextView title;
    private RelativeLayout call,help,logout;

    private RecyclerView dates_recycler;
    private List<PatientDateObject> list;
    private PatientsDatesAdapter dates_adapter;
    private LinearLayoutManager dates_layout_manager;

    private ProgressBar loading;
    private LinearLayout no_data,no_internet,error;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_patients_dates,container,false);
    }

    @Override
    public void init_views() {
        toolbar = base.findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        call = toolbar.findViewById(R.id.contact_us);
        help = toolbar.findViewById(R.id.help);
        logout = toolbar.findViewById(R.id.logout);
        if (!SharedPrefManager.getInstance(base).getLogged()){
            logout.setVisibility(View.GONE);
        }

        loading = base.findViewById(R.id.loading);
        no_data = base.findViewById(R.id.no_data_layout);
        no_internet = base.findViewById(R.id.no_internet_layout);
        error = base.findViewById(R.id.error_layout);

        dates_recycler = base.findViewById(R.id.dates_recycler);
    }

    @Override
    public void init_events() {
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactUsDialog dialog = new ContactUsDialog(base);
                dialog.show();
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base.open_fragment(new HelpFragment());
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(base);
                builder.setTitle(getResources().getString(R.string.logout_title));
                builder.setMessage(getResources().getString(R.string.logout_message));
                builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPrefManager.getInstance(base).setLogged(false);
                        SharedPrefManager.getInstance(base).setUser(new UserObject());
                        startActivity(new Intent(base, SplashActivity.class));
                        base.finish();
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
        error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callGetDatesAPI();
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        title.setText(getResources().getString(R.string.record_patients_dates));
        init_recycler();
    }
    private void init_recycler(){
        list = new ArrayList<>();
        dates_adapter = new PatientsDatesAdapter(base,list,this,this);
        dates_layout_manager = new LinearLayoutManager(base);
        dates_recycler.setLayoutManager(dates_layout_manager);
        dates_recycler.setAdapter(dates_adapter);
        callGetDatesAPI();
    }

    private void callGetDatesAPI(){
        loading.setVisibility(View.VISIBLE);
        list.clear();
        dates_adapter = new PatientsDatesAdapter(base,list,this,this);
        dates_recycler.setAdapter(dates_adapter);
        no_data.setVisibility(View.GONE);
        error.setVisibility(View.GONE);
        no_internet.setVisibility(View.GONE);
        MedicalDatesAPIsClass.get_doctor_dates(base,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        loading.setVisibility(View.GONE);
                        error.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onResponse(Object json) {
                        loading.setVisibility(View.GONE);
                        String j = new Gson().toJson(json);
                        try {
                            PatientDateObject[] success = new Gson().fromJson(j,PatientDateObject[].class);
                            if (success.length>0){
                                for (PatientDateObject o : success){
                                    list.add(o);
                                    dates_adapter.notifyDataSetChanged();
                                }
                            }else {
                                no_data.setVisibility(View.VISIBLE);
                            }
                        }catch (Exception e){
                            callGetDatesAPI();
                        }

                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        loading.setVisibility(View.GONE);
                        no_internet.setVisibility(View.VISIBLE);
                    }
                });
    }

    @Override
    public void onClick() {
        callGetDatesAPI();
    }

    @Override
    public void onItemClick(int position) {
        MedicalDateObject o = new MedicalDateObject();
        PatientDateObject oo = list.get(position);
        o.setTitle(oo.getTitle());
        o.setDetails(oo.getDetails());
        o.setDate(oo.getDate());
        o.setTime(oo.getTime());
        DateDetailsDialog dialog = new DateDetailsDialog(base,o);
        dialog.show();
    }
}
