package com.scit.tabibak.Models.Important_notes_models;

import com.google.gson.annotations.SerializedName;

public class ImportantNoteObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("title") private String title = "";
    @SerializedName("details") private String details = "";
    @SerializedName("alarm_time") private String alarm_time = "";
    @SerializedName("alarm_date") private String alarm_date = "";
    @SerializedName("user_id") private int user_id = 0;
    @SerializedName("type") private int type = 0;
    @SerializedName("image") private String image = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getAlarm_time() {
        return alarm_time;
    }

    public void setAlarm_time(String alarm_time) {
        this.alarm_time = alarm_time;
    }

    public String getAlarm_date() {
        return alarm_date;
    }

    public void setAlarm_date(String alarm_date) {
        this.alarm_date = alarm_date;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
