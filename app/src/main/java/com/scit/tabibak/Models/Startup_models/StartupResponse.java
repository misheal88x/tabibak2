package com.scit.tabibak.Models.Startup_models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class StartupResponse {
    @SerializedName("cities") private List<CityObject> cities = new ArrayList<>();
    @SerializedName("facilities") private List<FacilityObject> facilities = new ArrayList<>();
    @SerializedName("slides") private List<SliderObject> slides = new ArrayList<>();
    @SerializedName("specializations") private List<SpecializationObject> specializations = new ArrayList<>();
    @SerializedName("contact") private List<ContactObject> contact = new ArrayList<>();

    public List<CityObject> getCities() {
        return cities;
    }

    public void setCities(List<CityObject> cities) {
        this.cities = cities;
    }

    public List<FacilityObject> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<FacilityObject> facilities) {
        this.facilities = facilities;
    }

    public List<SliderObject> getSlides() {
        return slides;
    }

    public void setSlides(List<SliderObject> slides) {
        this.slides = slides;
    }

    public List<SpecializationObject> getSpecializations() {
        return specializations;
    }

    public void setSpecializations(List<SpecializationObject> specializations) {
        this.specializations = specializations;
    }

    public List<ContactObject> getContact() {
        return contact;
    }

    public void setContact(List<ContactObject> contact) {
        this.contact = contact;
    }
}
