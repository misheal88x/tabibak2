package com.scit.tabibak.Models.Medicine_models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MedicineObject implements Serializable {
    @SerializedName("id") private int id = 0;
    @SerializedName("name") private String name = "";
    @SerializedName("image") private String image = "";
    @SerializedName("details") private String details = "";
    @SerializedName("sub_details") private String sub_details = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getSub_details() {
        return sub_details;
    }

    public void setSub_details(String sub_details) {
        this.sub_details = sub_details;
    }
}
