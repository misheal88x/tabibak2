package com.scit.tabibak.Models.Medical_dates_models;

import com.google.gson.annotations.SerializedName;

public class PatientDateObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("details") private String details = "";
    @SerializedName("date") private String date = "";
    @SerializedName("time") private String time = "";
    @SerializedName("user_id") private int user_id = 0;
    @SerializedName("title") private String title = "";
    @SerializedName("doctor_id") private int doctor_id = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(int doctor_id) {
        this.doctor_id = doctor_id;
    }
}
