package com.scit.tabibak.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scit.tabibak.Interfaces.OnItemClickListener;
import com.scit.tabibak.Models.Important_notes_models.ImportantNoteObject;
import com.scit.tabibak.Models.Medical_notes_models.MedicalNoteObject;
import com.scit.tabibak.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class HealthNotesAdapter extends RecyclerView.Adapter<HealthNotesAdapter.ViewHolder> {
    private Context context;
    private List<MedicalNoteObject> list;
    private OnItemClickListener onItemClickListener;

    public HealthNotesAdapter(Context context,List<MedicalNoteObject> list,OnItemClickListener onItemClickListener) {
        this.context = context;
        this.list = list;
        this.onItemClickListener = onItemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView title,date,time;
        private LinearLayout date_layout,time_layout;
        private CardView layout;

        public ViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            date = view.findViewById(R.id.date);
            time = view.findViewById(R.id.time);
            date_layout = view.findViewById(R.id.date_layout);
            time_layout = view.findViewById(R.id.time_layout);
            layout = view.findViewById(R.id.layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_health_note, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        MedicalNoteObject o = list.get(position);

        holder.title.setText(o.getTitle());
        if (o.getAlarm_date()!=null&&!o.getAlarm_date().equals("")){
            holder.date.setText(o.getAlarm_date());
        }else {
            holder.date_layout.setVisibility(View.GONE);
        }
        if (o.getAlarm_time()!=null&&!o.getAlarm_time().equals("")){
            holder.time.setText(o.getAlarm_time());
        }else {
            holder.time_layout.setVisibility(View.GONE);
        }
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(position);
            }
        });
    }
}
