package com.scit.tabibak.APIs;

import com.scit.tabibak.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface DiagnosisAPIs {
    @FormUrlEncoded
    @POST("add-diagnosis")
    Call<BaseResponse> add(
            @Header("Accept") String accept,
            @Field("user_id") int user_id,
            @Field("medical_status") String medical_status,
            @Field("medical_diagnosis") String medical_diagnosis,
            @Field("prescription") String prescription
    );
}
